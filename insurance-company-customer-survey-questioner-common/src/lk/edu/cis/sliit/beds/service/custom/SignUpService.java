package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.SignupDto;
import lk.edu.cis.sliit.beds.service.SuperService;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular User SignUp Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface SignUpService extends SuperService {

    /**
     * @param signupDto : lk.edu.cis.sliit.beds.dto.SignupDto
     * @return boolean
     * @throws Exception
     */
    boolean signUpCustomer(SignupDto signupDto) throws Exception;
}
