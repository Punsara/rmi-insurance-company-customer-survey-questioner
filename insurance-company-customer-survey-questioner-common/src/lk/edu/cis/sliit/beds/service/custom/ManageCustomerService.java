package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.service.SuperService;

import java.util.List;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular Customer Managing Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface ManageCustomerService extends SuperService {

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.CustomerDto>
     * @throws Exception
     */
    List<CustomerDto> findAllCustomers() throws Exception;

    /**
     * @param customerDto : lk.edu.cis.sliit.beds.dto.CustomerDto
     * @return boolean
     * @throws Exception
     */
    boolean saveCustomer(CustomerDto customerDto) throws Exception;
    
    /**
     * @param customerId : java.lang.Integer
     * @return boolean
     * @throws Exception
     */
    boolean deleteCustomer(Integer customerId) throws Exception;
    
    /**
     * @param customerId : java.lang.Integer
     * @param customerDto : lk.edu.cis.sliit.beds.dto.CustomerDto
     * @return boolean
     * @throws Exception
     */
    boolean updateCustomer(Integer customerId, CustomerDto customerDto) throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     */
    Integer findNextCustomerId() throws Exception;
    
    /**
     * @return java.lang.Integer
     * @throws Exception
     */
    Integer findNextIncrementUserId() throws Exception;
}
