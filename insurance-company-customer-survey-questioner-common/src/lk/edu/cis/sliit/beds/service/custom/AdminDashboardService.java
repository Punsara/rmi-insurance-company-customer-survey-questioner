package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.FeedbackDto;
import lk.edu.cis.sliit.beds.service.SuperService;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular Admin Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface AdminDashboardService extends SuperService {
	
	/**
	 * @return byte[]
	 * @throws Exception
	 * returns customer recommendation against time bar chart image output stream as a base64 encoded byte array
	 */
	byte[] getCustomerRecomendationAgaintTimeBarChart() throws Exception;
	
	/**
	 * @return byte[]
	 * @throws Exception
	 * returns quality of the service against time line chart image output stream as a base64 encoded byte array
	 */
	byte[] getQualityOfServiceAgaintTimeLineChart() throws Exception;
	
	/**
	 * @return byte[]
	 * @throws Exception
	 * returns customer against their attitude pie chart image output stream as a base64 encoded byte array
	 */
	byte[] getCustomerAgainstAttitudePieChart() throws Exception;
	
	/**
	 * @return FeedbackDto
	 * @throws Exception
	 * returns the total Number of feedbacks and the progress (Positive, Neutral or Negative) 
	 * of the customer feedbacks launched in a particular day
	 */
	FeedbackDto getTodayCustomerFeedback() throws Exception;
}
