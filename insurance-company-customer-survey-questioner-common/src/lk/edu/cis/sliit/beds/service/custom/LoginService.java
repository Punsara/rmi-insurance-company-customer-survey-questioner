package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.dto.LoginDto;
import lk.edu.cis.sliit.beds.service.SuperService;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular User Login Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface LoginService extends SuperService {

	/**
	 * @param loginDto : LoginDto
	 * @return lk.edu.cis.sliit.beds.dto.CookieDto
	 * @throws Exception
	 */
	CookieDto authenticateUser(LoginDto loginDto) throws Exception;
}
