package lk.edu.cis.sliit.beds.service;

import java.rmi.Remote;

/**
 * Defines a SuperService to Maintain the Class Hierarchy Type which inherited the 
 * Remote features from the java.rmi.Remote Interface and This feature will implicitly 
 * inherit to all Classes of which implements from this Interface
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface SuperService extends Remote {
}
