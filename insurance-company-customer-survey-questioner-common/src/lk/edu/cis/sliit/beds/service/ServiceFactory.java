package lk.edu.cis.sliit.beds.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular Service Implementation Type from the Server Side Using the Defined enum Service Types
 * 
 * This Interface inherited the Remote features from the java.rmi.Remote Interface as in the 
 * Server StartUp Class Remote Object is type of This Interface
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface ServiceFactory extends Remote {
	
	/**
     * provides enum types for each service type which may refer from the client side
     *
     */
	enum ServiceTypes {LOGIN_SERVICE, SIGNUP_SERVICE, UMM_SERVICE, CUSTOMER_SURVEY, ADMIN_DASHBOARD, MANAGE_CUSTOMER, MANAGE_QUESTIONER}
	
    /**
     * @param serviceType : lk.edu.cis.sliit.beds.service.ServiceTypes
     * @return lk.edu.cis.sliit.beds.service.SuperService
     * @throws RemoteException
     * 
     * returns the implementation class type of the requested service type
     * based on the requested enum type
     * 
     */
    SuperService getService(ServiceTypes serviceType) throws RemoteException;
}
