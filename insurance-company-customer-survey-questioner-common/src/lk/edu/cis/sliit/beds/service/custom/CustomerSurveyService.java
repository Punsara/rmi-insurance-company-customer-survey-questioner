package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto;
import lk.edu.cis.sliit.beds.dto.CustomerServiceDto;
import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.service.SuperService;

import java.util.List;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular Customer Survey Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface CustomerSurveyService extends SuperService {

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.CustomerServiceDto>
     * @throws Exception
     */
    List<CustomerServiceDto> getAllCustomerServices() throws Exception;

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.QuestionerDto>
     * @throws Exception
     */
    List<QuestionerDto> getQuestionsMappedWithAnswers() throws Exception;

    /**
     * @param feedbackDtoList : java.util.List<lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto>
     * @return boolean
     * @throws Exception
     */
    boolean saveCustomerFeedback(List<CustomerFeedbackDto> feedbackDtoList) throws Exception;
}
