package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.service.SuperService;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular UserManagement Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface UserManagementService  extends SuperService {

	/**
	 * @param cookieDto : CookieDto
	 * @return boolean
	 */
	boolean isValidCookie(CookieDto cookieDto);
	
	/**
	 * @param key : String
	 * @return boolean
	 */
	boolean logOutUser(String key);
}
