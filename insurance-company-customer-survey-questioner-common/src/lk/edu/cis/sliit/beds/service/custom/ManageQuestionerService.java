package lk.edu.cis.sliit.beds.service.custom;

import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.dto.ValueTextDto;
import lk.edu.cis.sliit.beds.service.SuperService;

import java.util.List;

/**
 * Provides a Common Interface for Client side (ProxyHandler) to Interact and Request a 
 * Particular Questioner Managing Related Service which Serves Form this Service
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface ManageQuestionerService extends SuperService {

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.ValueTextDto>
     * @throws Exception
     */
    List<ValueTextDto> findAllQuestions() throws Exception;

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.ValueTextDto>
     * @throws Exception
     */
    List<ValueTextDto> findAllAnswers() throws Exception;

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.QuestionerDto>
     * @throws Exception
     */
    List<QuestionerDto> findAllQuestionAnswerMappings() throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     */
    Integer findNextAnswerId() throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     */
    Integer findNextQuestionId() throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     */
    Integer findNextMappingId() throws Exception;

    /**
     * @param valueTextDto : lk.edu.cis.sliit.beds.dto.ValueTextDto
     * @return boolean
     * @throws Exception
     */
    boolean saveQuestion(ValueTextDto valueTextDto) throws Exception;

    /**
     * @param valueTextDto : lk.edu.cis.sliit.beds.dto.ValueTextDto
     * @return boolean
     * @throws Exception
     */
    boolean saveAnswer(ValueTextDto valueTextDto) throws Exception;

    /**
     * @param questionerDto : lk.edu.cis.sliit.beds.dto.QuestionerDto
     * @return boolean
     * @throws Exception
     */
    boolean saveQuestionAnswerMapping(QuestionerDto questionerDto) throws Exception;
}
