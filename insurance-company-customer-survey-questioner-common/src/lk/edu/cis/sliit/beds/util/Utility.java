package lk.edu.cis.sliit.beds.util;

/**
 * Defines all the Common Constants At One Place which Uses Throughout in this Application
 * 
 * @author Punsara Prathibha - 2010866
 */
public class Utility {
    /**
     * Defines a constant value for admin user flag which contains in the database 
     * for the ease of maintenance
     */
    public static final int ADMIN_USER_ROLE_FLAG = 1;
    /**
     * Defines a constant value for customer user flag which contains in the database 
     * for the ease of maintenance
     */
    public static final int CUSTOMER_USER_ROLE_FLAG = 2;
    /**
     * Defines a constant value for empty string
     */
    public static final String EMPTY_STRING = "";

}
