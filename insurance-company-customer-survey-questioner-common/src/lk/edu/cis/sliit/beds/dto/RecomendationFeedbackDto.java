package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the Questioner Recommendation Feedback Details Communication
 * Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class RecomendationFeedbackDto implements Serializable {

	/**
	 * serialVersionUID of RecomendationFeedbackDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * java.util.List<java.lang.String>
	 * Creates an Object reference to keep 10 previous months list for recommendations and feedbacks
	 */
	private List<String> month = new ArrayList<>();
	/**
	 * java.util.List<java.lang.String>
	 * Creates an Object reference to keep recommendation progress for each month
	 */
	private List<String> recomendation = new ArrayList<>();
	/**
	 * java.util.List<java.lang.String>
	 * Creates an Object reference to keep feedback progress for each month
	 */
	private List<String> feedback = new ArrayList<>();
	
	/**
	 * Default Constructor
	 */
	public RecomendationFeedbackDto() {
	}

	/**
	 * @param month : java.util.List<java.lang.String>
	 * @param recomendation : java.util.List<java.lang.String>
	 * @param feedback : java.util.List<java.lang.String>
	 */
	public RecomendationFeedbackDto(List<String> month, List<String> recomendation, List<String> feedback) {
		super();
		this.month = month;
		this.recomendation = recomendation;
		this.feedback = feedback;
	}

	/**
	 * @return java.util.List<java.lang.String>
	 */
	public List<String> getMonth() {
		return month;
	}

	/**
	 * @param month : java.util.List<java.lang.String>
	 */
	public void setMonth(List<String> month) {
		this.month = month;
	}

	/**
	 * @return java.util.List<java.lang.String>
	 */
	public List<String> getRecomendation() {
		return recomendation;
	}

	/**
	 * @param recomendation : java.util.List<java.lang.String>
	 */
	public void setRecomendation(List<String> recomendation) {
		this.recomendation = recomendation;
	}

	/**
	 * @return java.util.List<java.lang.String>
	 */
	public List<String> getFeedback() {
		return feedback;
	}

	/**
	 * @param feedback : java.util.List<java.lang.String>
	 */
	public void setFeedback(List<String> feedback) {
		this.feedback = feedback;
	}

	
	
}
