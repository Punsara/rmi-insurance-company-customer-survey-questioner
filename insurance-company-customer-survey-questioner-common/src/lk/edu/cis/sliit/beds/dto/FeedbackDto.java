package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable 
 * to Manage the Customer Daily Feedback Communication Between 
 * Client Side And Server Side to represent in AdminDashboard 
 * 
 * @author Punsara Prathibha - 2010866
 */
public class FeedbackDto implements Serializable {
	/**
	 * serialVersionUID of FeedbackDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the total number of feedbacks provided 
	 * in current day
	 */
	private Integer totalNoOfFeedbacks;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the number of positive feedbacks provided 
	 * in current day
	 */
	private Integer noOfPositiveFeedbacks;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the number of neutral feedbacks provided 
	 * in current day
	 */
	private Integer noOfNeutralFeedbacks;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the number of negetive feedbacks provided 
	 * in current day
	 */
	private Integer noOfNegetiveFeedbacks;
	
	/**
	 * Default Constructor
	 */
	public FeedbackDto() {
		
	}
	
	/**
	 * @param totalNoOfFeedbacks : java.lang.Integer
	 * @param noOfPositiveFeedbacks : java.lang.Integer
	 * @param noOfNeutralFeedbacks : java.lang.Integer
	 * @param noOfNegetiveFeedbacks : java.lang.Integer
	 */
	public FeedbackDto(Integer totalNoOfFeedbacks, Integer noOfPositiveFeedbacks, 
			Integer noOfNeutralFeedbacks, Integer noOfNegetiveFeedbacks) {
		super();
		this.totalNoOfFeedbacks = totalNoOfFeedbacks;
		this.noOfPositiveFeedbacks = noOfPositiveFeedbacks;
		this.noOfNeutralFeedbacks = noOfNeutralFeedbacks;
		this.noOfNegetiveFeedbacks = noOfNegetiveFeedbacks;
	}

	/**
	 * @return java.lang.Integer
	 */
	public Integer getTotalNoOfFeedbacks() {
		return totalNoOfFeedbacks;
	}
	
	/**
	 * @param totalNoOfFeedbacks : java.lang.Integer
	 */
	public void setTotalNoOfFeedbacks(Integer totalNoOfFeedbacks) {
		this.totalNoOfFeedbacks = totalNoOfFeedbacks;
	}
	
	/**
	 * @return java.lang.Integer
	 */
	public Integer getNoOfPositiveFeedbacks() {
		return noOfPositiveFeedbacks;
	}
	
	/**
	 * @param noOfPositiveFeedbacks : java.lang.Integer
	 */
	public void setNoOfPositiveFeedbacks(Integer noOfPositiveFeedbacks) {
		this.noOfPositiveFeedbacks = noOfPositiveFeedbacks;
	}
	
	/**
	 * @return java.lang.Integer
	 */
	public Integer getNoOfNeutralFeedbacks() {
		return noOfNeutralFeedbacks;
	}
	
	/**
	 * @param noOfNeutralFeedbacks : java.lang.Integer
	 */
	public void setNoOfNeutralFeedbacks(Integer noOfNeutralFeedbacks) {
		this.noOfNeutralFeedbacks = noOfNeutralFeedbacks;
	}
	
	/**
	 * @return java.lang.Integer
	 */
	public Integer getNoOfNegetiveFeedbacks() {
		return noOfNegetiveFeedbacks;
	}
	
	/**
	 * @param noOfNegetiveFeedbacks : java.lang.Integer
	 */
	public void setNoOfNegetiveFeedbacks(Integer noOfNegetiveFeedbacks) {
		this.noOfNegetiveFeedbacks = noOfNegetiveFeedbacks;
	}
}
