package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the User Sign Up Details Communication Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class SignupDto implements Serializable {
    /**
	 * serialVersionUID of SignupDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.String
	 * Creates an Object reference for customer NIC number
	 */
	private String nicNo;
	/**
	 * java.lang.String
	 * Creates an Object reference for customer user name
	 */
    private String userName;
    /**
	 * java.lang.String
	 * Creates an Object reference for customer password
	 */
    private String password;

    /**
     * Default Constructor
     */
    public SignupDto() {
    }

    /**
     * @param nicNo : java.lang.String
     * @param userName : java.lang.String
     * @param password : java.lang.String
     */
    public SignupDto(String nicNo, String userName, String password) {
        this.nicNo = nicNo;
        this.userName = userName;
        this.password = password;
    }

    /**
     * @return java.lang.String
     */
    public String getNicNo() {
        return nicNo;
    }

    /**
     * @param nicNo : java.lang.String
     */
    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    /**
     * @return java.lang.String
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName : java.lang.String
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return java.lang.String
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password : java.lang.String
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
