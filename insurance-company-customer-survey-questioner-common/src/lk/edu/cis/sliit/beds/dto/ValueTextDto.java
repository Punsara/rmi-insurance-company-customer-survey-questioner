package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the any Value-Text pair data Communication Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ValueTextDto implements Serializable {
    /**
	 * serialVersionUID of ValueTextDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates an Object reference for any value which bind with another text 
	 */
	private Integer value;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for any text which bind with another value 
	 */
    private String text;
    /**
	 * java.sql.Timestamp
	 * Creates an Object reference for last updated date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public ValueTextDto() {
    }

    /**
     * @param value : java.lang.Integer
     * @param text : java.lang.String
     */
    public ValueTextDto(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @param value : java.lang.Integer
     * @param text : java.lang.String
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public ValueTextDto(Integer value, String text, Timestamp lastUpdatedDate) {
        this.value = value;
        this.text = text;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getValue() {
        return value;
    }

    /**
     * @param value : java.lang.Integer
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * @return java.lang.String
     */
    public String getText() {
        return text;
    }

    /**
     * @param text : java.lang.String
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
