package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the New Customers Interaction Against Time Line Chart Detail 
 * Communication Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class QualityOfServiceDto implements Serializable {
	
	/**
	 * serialVersionUID of QualityOfServiceDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.util.List<java.lang.String>
	 * Creates an Object reference to keep previous 10 months list
	 */
	private List<String> month = new ArrayList<>();
	/**
	 * java.util.List<java.lang.String>
	 * Creates an Object reference to keep the quality of the service for each month
	 */
	private List<String> qualityOfService = new ArrayList<>();
	
	
	/**
	 * @return java.util.List<java.lang.String>
	 */
	public List<String> getMonth() {
		return month;
	}
	
	/**
	 * @param month : java.util.List<java.lang.String>
	 */
	public void setMonth(List<String> month) {
		this.month = month;
	}

	/**
	 * @return java.util.List<java.lang.String>
	 */
	public List<String> getQualityOfService() {
		return qualityOfService;
	}

	/**
	 * @param qualityOfService : java.util.List<java.lang.String>
	 */
	public void setQualityOfService(List<String> qualityOfService) {
		this.qualityOfService = qualityOfService;
	}
	
}
