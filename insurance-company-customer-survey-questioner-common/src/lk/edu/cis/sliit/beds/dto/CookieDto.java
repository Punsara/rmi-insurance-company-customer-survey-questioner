package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable 
 * to Manage the Cookies Communication Between Client Side And Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CookieDto implements Serializable {
	/**
	 * serialVersionUID of CookieDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.String
	 * Creates an Object reference to keep the unique key of a particular cookie
	 */
	private String key;
	/**
	 * java.lang.String
	 * Creates an Object reference to keep the cookie which bind with the key
	 */
	private String value;
	
	/**
	 * Default Constructor
	 */
	public CookieDto() {
		super();
	}

	/**
	 * @param key : java.lang.String
	 * @param value : java.lang.String
	 * All Args Constructor
	 */
	public CookieDto(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	
	/**
	 * @return java.lang.String
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * @param key : java.lang.String
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * @return java.lang.String
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * @param value : java.lang.String
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
