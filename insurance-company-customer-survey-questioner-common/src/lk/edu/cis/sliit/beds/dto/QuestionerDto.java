package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the Questioner Feedback Details Communication Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class QuestionerDto implements Serializable {
    /**
	 * serialVersionUID of QuestionerDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Object
	 * Creates an Object reference to keep customer feedback provided as javaFX input types 
	 * such as RadioButtons, TextAreas etc..
	 */
	Object feedback;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for question number
	 */
    private Integer questionNo;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for customer feedback id
	 */
    private Integer customerFeedbackId;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for question id
	 */
    private Integer questionId;
    /**
	 * java.lang.String
	 * Creates an Object reference for question
	 */
    private String question;
    /**
	 * java.lang.String
	 * Creates an Object reference for answer
	 */
    private String answer;
    /**
	 * java.sql.Timestamp
	 * Creates an Object reference for last updated date time of these data
	 */
    private Timestamp lastUpdatedTime;

    /**
     * Default Constructor
     */
    public QuestionerDto() {
    }

    /**
     * @param questionNo : java.lang.Integer
     * @param question : java.lang.String
     * @param answer : java.lang.String
     */
    public QuestionerDto(Integer questionNo, String question, String answer) {
        this.questionNo = questionNo;
        this.question = question;
        this.answer = answer;
    }

    /**
     * @param questionNo : java.lang.Integer
     * @param question : java.lang.String
     * @param answer : java.lang.String
     * @param lastUpdatedTime : java.sql.Timestamp
     */
    public QuestionerDto(Integer questionNo, String question, String answer, Timestamp lastUpdatedTime) {
        this.questionNo = questionNo;
        this.question = question;
        this.answer = answer;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    /**
     * @param questionNo : java.lang.Integer
     * @param customerFeedbackId : java.lang.Integer
     * @param questionId : java.lang.Integer
     * @param question : java.lang.String
     * @param answer : java.lang.String
     * @param lastUpdatedTime : java.sql.Timestamp
     */
    public QuestionerDto(Integer questionNo, Integer customerFeedbackId, Integer questionId, String question,
                         String answer, Timestamp lastUpdatedTime) {
        this.questionNo = questionNo;
        this.customerFeedbackId = customerFeedbackId;
        this.questionId = questionId;
        this.question = question;
        this.answer = answer;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    /**
     * @return java.lang.Object
     */
    public Object getFeedback() {
        return feedback;
    }

    /**
     * @param feedback : java.lang.Object
     */
    public void setFeedback(Object feedback) {
        this.feedback = feedback;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getQuestionNo() {
        return questionNo;
    }

    /**
     * @param questionNo : java.lang.Integer
     */
    public void setQuestionNo(Integer questionNo) {
        this.questionNo = questionNo;
    }

    /**
     * @return java.lang.String
     */
    public String getQuestion() {
        return question;
    }

    /**
     * @param question : java.lang.String
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * @return java.lang.String
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * @param answer : java.lang.String
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    /**
     * @param lastUpdatedTime : java.sql.Timestamp
     */
    public void setLastUpdatedTime(Timestamp lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getMappingId() {
        return customerFeedbackId;
    }

    /**
     * @param mappingId : java.lang.Integer
     */
    public void setMappingId(Integer mappingId) {
        this.customerFeedbackId = mappingId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId : java.lang.Integer
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }
}
