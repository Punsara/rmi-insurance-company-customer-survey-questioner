package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable
 * to Manage the User Login Credentials Communication Between Client Side and Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class LoginDto implements Serializable {
    /**
	 * serialVersionUID of LoginDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.String
	 * Creates an Object reference for the user's user name
	 */
	private String userName;
	/**
	 * java.lang.String
	 * Creates an Object reference for the user's password
	 */
    private String password;
    /**
	 * java.lang.String
	 * Creates an Object reference for the user's user role type
	 */
    private String userRole;

    /**
     * Default Constructor
     */
    public LoginDto() {
    }

    /**
     * @return java.lang.String
     */
    public String getUserRole() {
        return userRole.toUpperCase();
    }

    /**
     * @param userRole : java.lang.String
     */
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    /**
     * @return java.lang.String
     */
    public String getUserName() {
        return userName;
    }

    /** 
     * @param userName : java.lang.String
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return java.lang.String
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password : java.lang.String
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
