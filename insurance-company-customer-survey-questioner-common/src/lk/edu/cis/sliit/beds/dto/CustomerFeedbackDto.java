package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable 
 * to Manage the Customer Feedback Detail Communication Between Client Side And Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerFeedbackDto implements Serializable {
    /**
	 * serialVersionUID of CustomerFeedbackDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the customer feedback id
	 */
	private Integer customerFeedbackId;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the customer id
	 */
    private Integer customerId;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for the service id
	 */
    private Integer serviceId;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for the question id
	 */
    private Integer questionId;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer user name
	 */
    private String customerUserName;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer feedback
	 */
    private String customerFeedback;
    /**
	 * java.sql.Timestamp
	 * Creates an Object reference for the customer provided feedback date time of these data
	 */
    private Timestamp feedbackDateTime;

    /**
     * Default Constructor
     */
    public CustomerFeedbackDto() {
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerFeedbackId() {
        return customerFeedbackId;
    }

    /**
     * @param customerFeedbackId : java.lang.Integer
     */
    public void setCustomerFeedbackId(Integer customerFeedbackId) {
        this.customerFeedbackId = customerFeedbackId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId : java.lang.Integer
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId : java.lang.Integer
     */
    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId : java.lang.Integer
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    /**
     * @return java.lang.String
     */
    public String getCustomerFeedback() {
        return customerFeedback;
    }

    /**
     * @param customerFeedback : java.lang.String
     */
    public void setCustomerFeedback(String customerFeedback) {
        this.customerFeedback = customerFeedback;
    }

    /**
     * @return java.lang.String
     */
    public String getCustomerUserName() {
        return customerUserName;
    }

    /**
     * @param customerUserName : java.lang.String
     */
    public void setCustomerUserName(String customerUserName) {
        this.customerUserName = customerUserName;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getFeedbackDateTime() {
        return feedbackDateTime;
    }

    /**
     * @param feedbackDateTime : java.sql.Timestamp
     */
    public void setFeedbackDateTime(Timestamp feedbackDateTime) {
        this.feedbackDateTime = feedbackDateTime;
    }
}
