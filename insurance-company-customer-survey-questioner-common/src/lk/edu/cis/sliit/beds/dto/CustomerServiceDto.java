package lk.edu.cis.sliit.beds.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable 
 * to Manage the Customer Services Detail Communication Between Client Side And Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerServiceDto implements Serializable {
    /**
	 * serialVersionUID of CustomerServiceDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the service id
	 */
	private Integer serviceId;
	/**
	 * java.lang.String
	 * Creates an Object reference for the service name
	 */
    private String serviceName;
    /**
	 * java.sql.Timestamp
	 * Creates an Object reference for the last updated date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public CustomerServiceDto() {
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId : java.lang.Integer
     */
    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return java.lang.String
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName : java.lang.String
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
