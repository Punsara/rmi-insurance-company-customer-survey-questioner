package lk.edu.cis.sliit.beds.dto;

import javafx.scene.control.Button;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Data Transfer Object which implements from java.io.Serializable to Manage 
 * the Customer Details Communication Between Client Side And Server Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerDto implements Serializable {
    /**
	 * serialVersionUID of CustomerDto
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the customer id
	 */
	private Integer customerId;
	/**
	 * java.lang.Integer
	 * Creates an Object reference for the customer user id
	 */
    private Integer userId;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer first name
	 */
    private String firstName;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer last name
	 */
    private String lastName;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for the customer mobile number
	 */
    private Integer mobileNo;
    /**
	 * java.lang.Double
	 * Creates an Object reference for the customer salary
	 */
    private Double salary;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer gender
	 */
    private String gender;
    /**
	 * java.lang.Integer
	 * Creates an Object reference for the customer age
	 */
    private Integer age;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer NIC number
	 */
    private String nicNo;
    /**
	 * java.lang.String
	 * Creates an Object reference for the customer address
	 */
    private String address;
    /**
	 * javafx.scene.control.Button
	 * Creates an Object reference for customer update button
	 */
    private Button updateButton;
    /**
	 * javafx.scene.control.Button
	 * Creates an Object reference for customer delete button
	 */
    private Button deleteButton;
    /**
	 * java.sql.Timestamp
	 * Creates an Object reference for last update date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public CustomerDto() {
    }
    
    /**
     * @param firstName : java.lang.String
     * @param lastName : java.lang.String
     * @param mobileNo : java.lang.Integer
     * @param salary : java.lang.Double
     * @param gender : java.lang.String
     * @param age : java.lang.Integer
     * @param nicNo : java.lang.String
     * @param address : java.lang.String
     * All Args Constructor
     */
    public CustomerDto(String firstName, String lastName,
            Integer mobileNo, Double salary, String gender, Integer age, String nicNo,
            String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.salary = salary;
		this.gender = gender;
		this.age = age;
		this.nicNo = nicNo;
		this.address = address;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId : java.lang.Integer
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId : java.lang.Integer
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return java.lang.String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName : java.lang.String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return java.lang.String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName : java.lang.String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo : java.lang.Integer
     */
    public void setMobileNo(Integer mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return java.lang.Double
     */
    public Double getSalary() {
        return salary;
    }

    /**
     * @param salary : java.lang.Double
     */
    public void setSalary(Double salary) {
        this.salary = salary;
    }

    /**
     * @return java.lang.String
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender : java.lang.String
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age : java.lang.Integer
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return java.lang.String
     */
    public String getNicNo() {
        return nicNo;
    }

    /**
     * @param nicNo : java.lang.String
     */
    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    /**
     * @return java.lang.String
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address : java.lang.String
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return javafx.scene.control.Button
     */
    public Button getUpdateButton() {
        return updateButton;
    }

    /**
     * @param updateButton : javafx.scene.control.Button
     */
    public void setUpdateButton(Button updateButton) {
        this.updateButton = updateButton;
    }

    /**
     * @return javafx.scene.control.Button
     */
    public Button getDeleteButton() {
        return deleteButton;
    }

    /**
     * @param deleteButton : javafx.scene.control.Button
     */
    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }
}
