package lk.edu.cis.sliit.beds.client;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


/**
 * Client StartUp for the Customer Survey Questioner Application
 * This Class Starts the Client Side Application and provides the user login GUI
 * to the user as the first GUI to interact with the system
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ClientStartup extends Application {

	/**
	 * @param primaryStage : javafx.stage.Stage
	 * initiates and displays the login form of client side application
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = FXMLLoader.load(getClass().getResource("/lk/edu/cis/sliit/beds/views/Login.fxml"));
			Scene scene = new Scene(root,372,522);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param args : java.lang.String[]
	 * Main Method which start up the Client Side Application of the 
	 * Customer Survey Questioner Application
	 */
	public static void main(String[] args) {
		// Launch a stand alone application using the list of arguments provided by the main method
		launch(args);
	}
}
