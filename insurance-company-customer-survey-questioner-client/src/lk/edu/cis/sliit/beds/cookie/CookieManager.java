package lk.edu.cis.sliit.beds.cookie;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory.ServiceTypes;
import lk.edu.cis.sliit.beds.service.custom.UserManagementService;

/**
 * Locally Stores And Maintains the Unique User Specific Generated 
 * Cookie at the Client Side
 * 
 * Used Singleton Design Pattern by declaring a private default constructor of the class to restrict and
 * prevent creating new objects in the heap again and again of the class type from outside of this class. 
 * The public methods allows to manipulate or modify the attributes of the class by 
 * referring to one time generated object of class type. By using Singleton Design Pattern, the Cookie data
 * can be keep as a local storage by storing in those attributes by preventing resetting those attributes values
 * as again and again object creation is prevented using this design pattern.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CookieManager {

	/**
	 * lk.edu.cis.sliit.beds.cookie.CookieManager
	 * Creates an private static object reference for CookieManager
	 * 
	 * Encapsulation concept is used here by declaring attribute as a private access modifier,
	 * It does not allows someone to directly modify/manipulate the attribute values from out side
	 * of this class and hide those attribute data from the user.
	 */
	private static CookieManager cookieManagementHandler;
	/**
	 * lk.edu.cis.sliit.beds.dto.CookieDto
	 * Creates an private static object reference for CookieDto
	 */
	private static CookieDto keyBindedCookie;
	
	/**
	 * defines private constructor of CookieManager class for disabling creating 
	 * new Objects from out side of CookieManager for keep preventing resetting the keyBindedCookie
	 * when call CookieManager each time
	 */
	private CookieManager() {
		
	}
	
	
	/**
	 * @return lk.edu.cis.sliit.beds.cookie.CookieManager
	 * disabling creating new objects of CookieManager again and again, instead provides the existing 
	 * CookieManager object and creates a new Object if currently not any Object exists
	 */
	public static CookieManager getInstance() {
		if(null == cookieManagementHandler) {
			cookieManagementHandler = new CookieManager();
		}
		return cookieManagementHandler;
	}
	
	/**
	 * @param cookieDto : lk.edu.cis.sliit.beds.dto.CookieDto
	 * performs adding/registering a new cookie provided by mapping with a given key
	 */
	public static void addCookie(CookieDto cookieDto) {
		keyBindedCookie = cookieDto;
	}
	
	/**
	 * @return lk.edu.cis.sliit.beds.dto.CookieDto
	 * returns the current cookie with the key mapped
	 */
	public static CookieDto getCookie() {
		return keyBindedCookie;
	}
	
	/**
	 * @return boolean
	 * returns if the provided cookie is a valid by comparing with the current locally stored cookie
	 */
	public static boolean isValidCookie() {
		boolean isValidCookie = false;
    	UserManagementService ummService = (UserManagementService) ProxyHandler.getInstance().getService(ServiceTypes.UMM_SERVICE);
    	isValidCookie = ummService.isValidCookie(keyBindedCookie);
    	if(!isValidCookie) {
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setContentText("Invalid/Corrrupted Cookie Found! Please Login to Proceed");
    		alert.show();
    	}
    	return isValidCookie;
    }
	
	/**
	 * @return boolean
	 * remove the cookie from the local storage, logout user and remove cookie from the server side
	 */
	public boolean logoutUser() {
		String key = keyBindedCookie.getKey();
		keyBindedCookie = null;
		UserManagementService ummService = (UserManagementService) ProxyHandler.getInstance().getService(ServiceTypes.UMM_SERVICE);
		return ummService.logOutUser(key);
	}
}
