package lk.edu.cis.sliit.beds.proxy;

import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.SuperService;
import lk.edu.cis.sliit.beds.service.custom.*;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Provides a Common Interface for Client side to Interact and Request Resources from the Server Side
 * by Registering all the Back End Services in One Place by Look Up/Listening to the 
 * Remote Server Object and Provides the Requested Service Type from the Client Side
 * 
 * Used Singleton Design Pattern by declaring a private default constructor of the class to restrict and
 * prevent creating new objects in the heap again and again of the class type from outside of this class. 
 * The public methods allows to manipulate or modify the attributes of the class by 
 * referring to one time generated object of class type. By using Singleton Design Pattern, the data can be stored 
 * in those attributes by preventing resetting those attributes values as again and again object creation is 
 * prevented using this design pattern.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ProxyHandler implements ServiceFactory, Serializable {

    /**
     * serialVersionUID of ProxyHandler
     * 
     * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
     */
    private static final long serialVersionUID = 2906642554793891381L;

    /**
	 * lk.edu.cis.sliit.beds.proxy.ProxyHandler
	 * Creates a private static object reference for ProxyHandler
	 */
    private static ProxyHandler proxyHandler;
    /**
	 * lk.edu.cis.sliit.beds.service.ServiceFactory
	 * Creates a private static object reference for ServiceFactory
	 */
    private static ServiceFactory serviceFactory;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.LoginService
	 * Creates a private object reference for LoginService
	 */
    private LoginService loginService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService
	 * Creates a private object reference for CustomerSurveyService
	 */
    private CustomerSurveyService customerService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.ManageCustomerService
	 * Creates a private object reference for ManageCustomerService
	 */
    private ManageCustomerService manageCustomerService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.SignUpService
	 * Creates a private object reference for SignUpService
	 */
    private SignUpService signUpService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.ManageQuestionerService
	 * Creates a private object reference for ManageQuestionerService
	 */
    private ManageQuestionerService manageQuestionerService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.AdminDashboardService
	 * Creates a private object reference for AdminDashboardService
	 */
    private AdminDashboardService adminDashboardService;
    /**
	 * lk.edu.cis.sliit.beds.service.custom.UserManagementService
	 * Creates a private object reference for UserManagementService
	 */
    private UserManagementService ummService;


    /**
     * defines private constructor of ProxyHandler class for disabling creating 
	 * new Objects from out side of ProxyHandler for keep preventing resetting the service connections
     */
    private ProxyHandler() {
        try {
            this.loginService = (LoginService) serviceFactory.getService(ServiceFactory.ServiceTypes.LOGIN_SERVICE);
            this.signUpService = (SignUpService) serviceFactory.getService(ServiceFactory.ServiceTypes.SIGNUP_SERVICE);
            this.customerService = (CustomerSurveyService) serviceFactory.getService(ServiceFactory.ServiceTypes.CUSTOMER_SURVEY);
            this.manageCustomerService = (ManageCustomerService) serviceFactory.getService(ServiceTypes.MANAGE_CUSTOMER);
            this.manageQuestionerService = (ManageQuestionerService) serviceFactory.getService(ServiceTypes.MANAGE_QUESTIONER);
            this.adminDashboardService = (AdminDashboardService) serviceFactory.getService(ServiceTypes.ADMIN_DASHBOARD);
            this.ummService = (UserManagementService) serviceFactory.getService(ServiceTypes.UMM_SERVICE);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return lk.edu.cis.sliit.beds.proxy.ProxyHandler
     * communicate with the RMI registry and store it as a private variable and disabling creating 
     * new objects of ProxyHandler type again and again, instead provides the existing 
	 * ProxyHandler object and creates a new Object if currently not any Object exists
     */
    public static ProxyHandler getInstance() {
        try {
            serviceFactory = (ServiceFactory) Naming.lookup("rmi://localhost/InsuranceCustomerSurveyRegistry");
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (null == proxyHandler) {
            proxyHandler = new ProxyHandler();
        }
        return proxyHandler;
    }

    /**
     * returns the requested service type by communicating with the RMI server to the controller
     */
    @Override
    public SuperService getService(ServiceTypes serviceType) {
        switch (serviceType) {
            case LOGIN_SERVICE:
                return this.loginService;
            case SIGNUP_SERVICE:
                return this.signUpService;
            case UMM_SERVICE:
            	return this.ummService;
            case CUSTOMER_SURVEY:
                return this.customerService;
            case MANAGE_CUSTOMER:
                return this.manageCustomerService;
            case MANAGE_QUESTIONER:
                return this.manageQuestionerService;
            case ADMIN_DASHBOARD:
            	return this.adminDashboardService;
            default:
                return null;
        }
    }
}
