package lk.edu.cis.sliit.beds.controller;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.dto.LoginDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.LoginService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in UserLogin GUI
 * This Class Consists of Following Main Features:
 *  1) Allows the UserLogin GUI to Authenticate using User Credentials to Access the Application
 *  2) Provides the Feature to Switch to User Sign Up Form for Register and Create an 
 *     Account to Interact with the system if a Particular Customer is New to the System
 *  
 * @author Punsara Prathibha - 2010866
 */
public class LoginController implements Initializable {
    
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Root Pane for the Login Form
	 */
    @FXML
    public AnchorPane rootPane;
    /**
	 * javafx.scene.control.RadioButton
	 * Creates an object reference for RadioButton for Customer User Role
	 */
    @FXML
    public RadioButton rbtnCustomer;
    /**
	 * javafx.scene.control.RadioButton
	 * Creates an object reference for RadioButton for Admin User Role
	 */
    @FXML
    public RadioButton rbtnAdmin;
    /**
	 * javafx.scene.control.TextField
	 * Creates an object reference for TextField for user name input field
	 */
    @FXML
    public TextField txtUserName;
    /**
	 * javafx.scene.control.PasswordField
	 * Creates an object reference for PasswordField for password input field
	 */
    @FXML
    public PasswordField txtPassword;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for User Name Validation Message Label
	 */
    @FXML
    public Label lblUserNameValidationMsg;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for Password Validation Message Label
	 */
    @FXML
    public Label lblPasswordValidationMsg;
    /**
	 * javafx.scene.control.Button
	 * Creates an object reference for Sign In Button
	 */
    @FXML 
    public Button btnSignIn;
    /**
     * javafx.scene.control.ToggleGroup
     * Creates an object reference for ToggleGroup for User Role RadioButtons
     */
    @FXML
    private final ToggleGroup toggleGroup = new ToggleGroup();
    /**
     * lk.edu.cis.sliit.beds.service.custom.LoginService
     * Creates an object reference for LoginService
     */
    private LoginService loginService;

    /**
     *  @param url : java.net.URL
     *  @param rb : java.util.ResourceBundle
     *  initializes lk.edu.cis.sliit.beds.controller.LoginController
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loginService = (LoginService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.LOGIN_SERVICE);
        setToggleGroup();
    }

    /**
     * sets the toggle group for customer and admin user radio inputs
     */
    private void setToggleGroup() {
        rbtnCustomer.setToggleGroup(toggleGroup);
        rbtnAdmin.setToggleGroup(toggleGroup);
    }

    /**
     * @return java.lang.String
     * returns user role based on selected radio buttons of customer and admin user
     */
    private String getUserRole() {
        String userRole = null;
        if (rbtnCustomer.isSelected()) {
            userRole = rbtnCustomer.getText();
        } else if (rbtnAdmin.isSelected()) {
            userRole = rbtnAdmin.getText();
        }
        return userRole;
    }

    /**
     * @return boolean
     * returns boolean value based on userName and password validation criteria
     */
    private boolean isValidateCredentials() {
    	String userName = this.txtUserName.getText();
    	String password = this.txtPassword.getText();
        return (isUserNameValidates(userName) && isPasswordValidates(password));
    }
    
    /**
     * @param value : java.lang.String
     * @return boolean
     * returns boolean value based on null or empty criteria for given String value
     */
    private boolean isNotNullAndNotEmpty(String value) {
    	return null != value && !value.isEmpty();
    }

    /**
     * @return boolean
     * returns boolean value indicating whether user role is selected
     */
    private boolean isUserRoleSelected() {
        return rbtnCustomer.isSelected() || rbtnAdmin.isSelected();
    }

    /**
     * @throws IOException
     * redirects customer users to the customer portal
     */
    @FXML
    private void redirectToCustomerPortal() throws IOException {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/CustomerPortal.fxml"));
        Scene scene = new Scene(loader.load());
        Stage stage = (Stage) this.rootPane.getScene().getWindow();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        CustomerPortalController customerPortalController = loader.getController();
        customerPortalController.initData(txtUserName.getText());
        stage.show();

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
        translateTransition.setFromX(-scene.getWidth());
        translateTransition.setToX(0);
        translateTransition.play();
    }

    /**
     * @throws IOException
     * redirects admin users to the admin portal
     */
    @FXML
    private void redirectToAdminPortal() throws IOException {
        Parent root = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/AdminPanel.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) this.rootPane.getScene().getWindow();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
        translateTransition.setFromX(-scene.getWidth());
        translateTransition.setToX(0);
        translateTransition.play();
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for userName and request focus to next input 
     * based on results when ActionEvent occurs on txtUserName 
     */
    @FXML
    public void txtUserNameOnAction(ActionEvent actionEvent) {
    	String userName = this.txtUserName.getText();
    	if(isUserNameValidates(userName)) {
    		txtPassword.requestFocus();
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs relevant validation process for password and auto performs authentication process
     * based on results when ActionEvent occurs on txPassword 
     */
    @FXML
    public void txtPasswordOnAction(ActionEvent actionEvent) {
    	if(isValidateCredentials()) {
    		authenticateUser();
    	}
    }
    
    /**
     * @param userName : String
     * @return boolean
     * returns boolean value based on userName validation criteria
     */
    private boolean isUserNameValidates(String userName) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(userName);
    	if(isNotNullAndNotEmpty) {
    		lblUserNameValidationMsg.setVisible(false);
    	} else {
    		lblUserNameValidationMsg.setText("User Name is Required!");
    		lblUserNameValidationMsg.setAlignment(Pos.CENTER);
    		lblUserNameValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    /**
     * @param password : String
     * @return boolean
     * returns boolean value based on password validation criteria
     */
    private boolean isPasswordValidates(String password) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(password);
    	if(isNotNullAndNotEmpty) {
    		lblPasswordValidationMsg.setVisible(false);
    	} else {
    		lblPasswordValidationMsg.setText("Password is Required!");
    		lblPasswordValidationMsg.setAlignment(Pos.CENTER);
    		lblPasswordValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }

    /**
     * @param actionEvent : ActionEvent
     * requests to perform user authentication process based on ActionEvent occurs on btnSignIn
     */
    @FXML
    public void btnSignInOnAction(ActionEvent actionEvent) {
    	authenticateUser();
    }
    
    /**
     * perform user authentication process based on user role selection validation 
     * and credentials validation process
     */
    private void authenticateUser() {
    	if (isUserRoleSelected()) {
            if (isValidateCredentials()) {
                Alert alert = new Alert(AlertType.INFORMATION);
                try {
                    LoginDto loginDto = new LoginDto();
                    loginDto.setUserName(this.txtUserName.getText());
                    loginDto.setPassword(this.txtPassword.getText());
                    loginDto.setUserRole(getUserRole());
                    CookieDto cookie = loginService.authenticateUser(loginDto);
                    String response;
                    if (null != cookie) {
                    	CookieManager.getInstance().addCookie(cookie);
                        if ("CUSTOMER".equals(loginDto.getUserRole().trim())) {
                            response = "You have Successfully Logged In!";
                            redirectToCustomerPortal();
                        } else if ("ADMIN".equals(loginDto.getUserRole().trim())) {
                            response = "You have Successfully Logged In!";
                            redirectToAdminPortal();
                        } else {
                            alert.setAlertType(AlertType.ERROR);
                            response = "Invalid User Role";
                        }
                    } else {
                        alert.setAlertType(AlertType.ERROR);
                        response = "Your Credentials are Wrong. Please Check Again.";
                    }
                    alert.setContentText(response);
                    alert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setContentText("UserName and Password can't be Empty");
                alert.show();
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("Please Select User Role!");
            alert.show();
        }
    }

    /**
     * @param mouseEvent : MouseEvent
     * redirects to SignUp Form when MouseEvent occurs on SignUp button
     */
    @FXML
    public void lblSignUpOnClick(MouseEvent mouseEvent) {
        try {
            Parent root = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/Signup.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage) this.rootPane.getScene().getWindow();
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.setResizable(false);
            stage.show();

            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
            translateTransition.setFromX(-scene.getWidth());
            translateTransition.setToX(0);
            translateTransition.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}