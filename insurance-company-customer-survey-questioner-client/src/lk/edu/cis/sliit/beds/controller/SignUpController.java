package lk.edu.cis.sliit.beds.controller;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.edu.cis.sliit.beds.dto.SignupDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.SignUpService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in CustomerSurveyQuestioner GUI
 * This Class Consists of Following Main Features:
 *  1) Allows the New Customers to Create an Account to Interact with the System
 *  2) Allows the Customer to Redirect Back to the User Login Form
 * 
 * @author Punsara Prathibha - 2010866
 */
public class SignUpController implements Initializable {
	
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Root Pane for sign up Form
	 */
	@FXML
    public AnchorPane rootPane;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for NIC number input field
	 */
	@FXML
    public TextField txtNicNo;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for user name input field
	 */
	@FXML
    public TextField txtUserName;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for password input field
	 */
	@FXML
    public PasswordField txtPassword;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for sign up Button
	 */
	@FXML
    public Button btnSignUp;
	/**
	 * javafx.scene.image.ImageView
	 * Creates an object reference for back button ImageView
	 */
	@FXML
    public ImageView lblBack;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for NIC number validation message
	 */
	@FXML
	public Label lblNicValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for user name validation message
	 */
	@FXML
	public Label lblUserNameValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for password validation message
	 */
	@FXML
	public Label lblPasswordValidationMsg;

	/**
     * lk.edu.cis.sliit.beds.service.custom.SignUpService
     * Creates a Object reference for SignUpService
     */
    private SignUpService signUpService;

    /**
     * @param location : java.net.URL
     * @param resources : java.util.ResourceBundle
     * initializes lk.edu.cis.sliit.beds.controller.SignUpController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.signUpService = (SignUpService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.SIGNUP_SERVICE);
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * request to perform user sign up when ActionEvent trigger on sign up button
     */
    @FXML
    public void btnSignUpOnAction(ActionEvent actionEvent) {
        try {
            performUserSignUp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @throws Exception
     * performs user sign up process by validating data provided by the user
     */
    private void performUserSignUp() throws Exception {
    	if(isValidateAll()) {
    		boolean isSuccessfullySaved = signUpService.signUpCustomer(getSignupDetails());
            Alert alert;
            if (isSuccessfullySaved) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Welcome to customers survey! \n Please Login to Continue");
                redirectToLogin();
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("OOPS.. Something went wrong!\n Please check your Details");
            }
            alert.show();
    	}
        
    }

    /**
     * @return lk.edu.cis.sliit.beds.dto.SignupDto
     * returns a SignupDto according to the current sign up user details provided by the user
     */
    private SignupDto getSignupDetails() {
        SignupDto signupDto = new SignupDto();
        signupDto.setNicNo(txtNicNo.getText());
        signupDto.setUserName(txtUserName.getText());
        signupDto.setPassword(txtPassword.getText());
        return signupDto;
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for NIC no validation and request focus to next input based on validation criteria 
     */
    @FXML
    public void txtNicNoOnAction(ActionEvent actionEvent) {
    	String nicNo = txtNicNo.getText();
    	if(isNicNoValidate(nicNo)) {
        	txtUserName.requestFocus();
    	}
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for userName validation and request focus to next input based on validation criteria 
     */
    @FXML
    public void txtUserNameOnAction(ActionEvent actionEvent) {
    	String userName = txtUserName.getText();
    	if(isUserNameValidate(userName)) {
    		txtPassword.requestFocus();
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for password validation and request focus to next input based on validation criteria 
     */
    @FXML
    public void txtPasswordOnAction(ActionEvent actionEvent) {
    	String password = txtPassword.getText();
    	if(isPasswordValidate(password)) {
    		try {
				performUserSignUp();
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }
    
    /**
     * @param nicNo : java.lang.String
     * @return boolean
     * returns boolean value based on validation criteria of given NIC no
     */
    private boolean isNicNoValidate(String nicNo) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(nicNo);
    	boolean isNicNoValid = nicNo.matches("^([0-9]{9}[x|X|v|V]|[0-9]{12})$");
    	
    	if(isNotNullAndNotEmpty && isNicNoValid) {
    		lblNicValidationMsg.setVisible(false);
    	} else {
    		if(!isNotNullAndNotEmpty) {
    			lblNicValidationMsg.setText("NIC is Required!");
        		lblNicValidationMsg.setAlignment(Pos.CENTER);
        		lblNicValidationMsg.setVisible(true);
    		} else {
    			lblNicValidationMsg.setText("Invalid NIC Number!");
        		lblNicValidationMsg.setAlignment(Pos.CENTER);
        		lblNicValidationMsg.setVisible(true);
    		}
    	}
    	return isNotNullAndNotEmpty && isNicNoValid;
    }
    
    /**
     * @param userName : java.lang.String
     * @return boolean
     * returns boolean value based on validation criteria of given userName
     */
    private boolean isUserNameValidate(String userName) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(userName);
    	if(isNotNullAndNotEmpty) {
    		lblUserNameValidationMsg.setVisible(false);
    	} else {
    		lblUserNameValidationMsg.setText("User Name is Required!");
    		lblUserNameValidationMsg.setAlignment(Pos.CENTER);
    		lblUserNameValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    /**
     * @param password : java.lang.String
     * @return boolean
     * returns boolean value based on validation criteria of given password
     */
    private boolean isPasswordValidate(String password) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(password);
    	if(isNotNullAndNotEmpty) {
    		lblPasswordValidationMsg.setVisible(false);
    	} else {
    		lblPasswordValidationMsg.setText("Password is Required!");
    		lblPasswordValidationMsg.setAlignment(Pos.CENTER);
    		lblPasswordValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    /**
     * @param value : java.lang.String
     * @return boolean
     * returns boolean value based on provided String not null and not empty criteria
     */
    private boolean isNotNullAndNotEmpty(String value) {
    	return null != value && !value.isEmpty();
    }
    
    /**
     * @param value : java.lang.String
     * @return boolean
     * returns boolean value based on whether all fields are validated
     */
    private boolean isValidateAll() {
    	return isNicNoValidate(txtNicNo.getText()) 
    			&& isUserNameValidate(txtUserName.getText()) 
    			&& isPasswordValidate(txtPassword.getText());
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * request for redirect back to the user login form
     */
    @FXML
    public void lblBackOnClick(MouseEvent mouseEvent) {
        try {
            redirectToLogin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    /**
     * @throws IOException
     * performs operations to redirect back to the user login form
     */
    private void redirectToLogin() throws IOException {
        Parent root = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/Login.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) this.rootPane.getScene().getWindow();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
        translateTransition.setFromX(-scene.getWidth());
        translateTransition.setToX(0);
        translateTransition.play();
    }
}
