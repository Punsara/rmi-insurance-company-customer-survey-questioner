package lk.edu.cis.sliit.beds.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.ManageCustomerService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in AdminManageCustomer GUI
 * This Class Consists of Following Main Features:
 *  1) Allows to Save New Customer Details
 *  2) Displays all Customer Details Currently Available in the System 
 *  3) Allows to Update Customer Details
 *  4) Allows to Delete a Particular Customer from the System
 * 
 * @author Punsara Pathibha - 2010866
 */
public class ManageCustomerController implements Initializable {

	/**
	 * javafx.scene.control.TableView<lk.edu.cis.sliit.beds.dto.CustomerDto>
	 * Creates an object reference for customer details table
	 */
	@FXML
    public TableView<CustomerDto> tblCustomerDetail;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer id disabled field
	 */
	@FXML
    public TextField txtCustomerId;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer first name input field
	 */
	@FXML
    public TextField txtFirstName;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer last name input field
	 */
	@FXML
    public TextField txtLastName;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer user id disabled field
	 */
	@FXML
	public TextField txtUserId;
	/**
	 * javafx.scene.control.TextArea
	 * Creates an object reference for customer address hidden field
	 */
	@FXML
    public TextArea txtAddress;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer mobile number input field
	 */
	@FXML
    public TextField txtMobileNo;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer NIC number input field
	 */
	@FXML
    public TextField txtNicNo;
	/**
	 * javafx.scene.control.ComboBox<java.lang.String>
	 * Creates an object reference for customer gender combo box
	 */
	@FXML
    public ComboBox<String> cmbGender;
	
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for save customer Button
	 */
	@FXML
    public Button btnSaveCustomer;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for reset customer Button
	 */
	@FXML
    public Button btnResetCustomer;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer salary input field
	 */
	@FXML
    public TextField txtSalary;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for customer age input field
	 */
	@FXML
    public TextField txtAge;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for first name validation message
	 */
	@FXML
	public Label lblFirstNameValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for last name validation message
	 */
	@FXML
	public Label lblLastNameValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for mobile number validation message
	 */
	@FXML
	public Label lblMobilNoValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for customer salary validation message
	 */
	@FXML
	public Label lblSalaryValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for customer gender validation message
	 */
	@FXML
	public Label lblGenderValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for customer age validation message
	 */
	@FXML
	public Label lblAgeValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for customer NIC number validation message
	 */
	@FXML
	public Label lblNicNoValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for customer address validation message
	 */
	@FXML
	public Label lblAddressValidationMsg;
	
    /**
     * lk.edu.cis.sliit.beds.service.custom.ManageCustomerService
     * Creates an object reference for ManageCustomerService
     */
    private ManageCustomerService manageCustomerService;

    /**
     *  @param location : java.net.URL
     *  @param resources : java.util.ResourceBundle
     *  initializes lk.edu.cis.sliit.beds.controller.ManageCustomerController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
        	if(CookieManager.isValidCookie()) {
        		manageCustomerService = (ManageCustomerService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.MANAGE_CUSTOMER);
        		loadAllCustomers();
        		loadNextCustomerId();
        		loadNextUserId();
        		loadGender();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    /**
     * @throws Exception
     * loads next incremented customer id
     */
    private void loadNextCustomerId() throws Exception {
        Integer nextCustomerId = manageCustomerService.findNextCustomerId();
        txtCustomerId.setText(nextCustomerId.toString());
    }
    
    /**
     * @throws Exception
     * loads next incremented user id
     */
    private void loadNextUserId() throws Exception {
    	Integer nextUserId = manageCustomerService.findNextIncrementUserId();
        txtUserId.setText(nextUserId.toString());
    }

    /**
     * loads gender values
     */
    private void loadGender() {
        cmbGender.getItems().add("Male");
        cmbGender.getItems().add("Female");
        cmbGender.getItems().add("Other");
    }

    /**
     * @throws Exception
     * load and view all customers in a table by retrieving data from the server side
     */
    private void loadAllCustomers() throws Exception {
        List<CustomerDto> allCustomers = manageCustomerService.findAllCustomers();

        tblCustomerDetail.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("customerId"));
        tblCustomerDetail.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tblCustomerDetail.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tblCustomerDetail.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblCustomerDetail.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("mobileNo"));
        tblCustomerDetail.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("nicNo"));
        tblCustomerDetail.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("salary"));
        tblCustomerDetail.getColumns().get(7).setCellValueFactory(new PropertyValueFactory<>("gender"));
        tblCustomerDetail.getColumns().get(8).setCellValueFactory(new PropertyValueFactory<>("age"));
        tblCustomerDetail.getColumns().get(9).setCellValueFactory(new PropertyValueFactory<>("updateButton"));
        tblCustomerDetail.getColumns().get(10).setCellValueFactory(new PropertyValueFactory<>("deleteButton"));
        
        tblCustomerDetail.getItems().clear();
        tblCustomerDetail.setItems(FXCollections.observableArrayList(allCustomers));
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for first name and request focus to next input 
     * based on results when ActionEvent occurs on txtFirstName 
     */
    @FXML
    public void txtFirstNameOnAction(ActionEvent actionEvent) {
    	String firstName = this.txtFirstName.getText();
    	if(isFirstNameValidate(firstName)) {
    		txtLastName.requestFocus();
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for last name and request focus to next input 
     * based on results when ActionEvent occurs on txtLastName 
     */
    @FXML
    public void txtLastNameOnAction(ActionEvent actionEvent) {
    	String lastName = this.txtLastName.getText();
    	if(isLastNameValidate(lastName)) {
    		txtMobileNo.requestFocus();
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for mobile no and request focus to next input 
     * based on results when ActionEvent occurs on txtMobileNo 
     */
    @FXML
    public void txtMobileNoOnAction(ActionEvent actionEvent) {
    	String mobileNo = this.txtMobileNo.getText();
    	if(isMobileNoValidate(mobileNo)) {
    		txtSalary.requestFocus();
    	}
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for salary and request focus to next input 
     * based on results when ActionEvent occurs on txtSalary 
     */
    @FXML
    public void txtSalaryOnAction(ActionEvent actionEvent) {
    	String salary = this.txtSalary.getText();
    	if(isSalaryValidate(salary)) {
    		cmbGender.requestFocus();
    	}
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for gender and request focus to next input 
     * based on results when ActionEvent occurs on cmbGender 
     */
    @FXML
    public void cmbGenderOnAction(ActionEvent actionEvent) {
    	if(isGenderValidate()) {
    		txtAge.requestFocus();
    	}
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for age and request focus to next input 
     * based on results when ActionEvent occurs on txtAge 
     */
    @FXML
    public void txtAgeOnAction(ActionEvent actionEvent) {
    	String age = this.txtAge.getText();
    	if(isAgeValidate(age)) {
    		txtNicNo.requestFocus();
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * perform relevant validation process for NIC no and request focus to next input 
     * based on results when ActionEvent occurs on txtNicNo 
     */
    @FXML
    public void txtNicNoOnAction(ActionEvent actionEvent) {
    	String nicNo = this.txtNicNo.getText();
    	if(isNicNoValidate(nicNo)) {
    		txtAddress.requestFocus();
    	}
    }
    
    /**
     * @param keyEvent : javafx.event.KeyEvent
     * perform relevant validation process for address no and request focus to next input 
     * based on results when KeyEvent occurs on txtAddress 
     */
    @FXML
    public void txtAddressOnKeyRelease(KeyEvent keyEvent) {
    	String address = this.txtAddress.getText();
    	isAddressValidate(address);
    }
    
    
    /**
     * @param firstName : java.lang.String
     * @return boolean
     * returns boolean value based on the first name validation criteria
     */
    private boolean isFirstNameValidate(String firstName) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(firstName);
    	if(isNotNullAndNotEmpty) {
    		lblFirstNameValidationMsg.setVisible(false);
    	} else {
    		lblFirstNameValidationMsg.setText("First Name is Required!");
    		lblFirstNameValidationMsg.setAlignment(Pos.CENTER);
    		lblFirstNameValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    /**
     * @param lastName : java.lang.String
     * @return boolean
     * returns boolean value based on the last name validation criteria
     */
    private boolean isLastNameValidate(String lastName) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(lastName);
    	if(isNotNullAndNotEmpty) {
    		lblLastNameValidationMsg.setVisible(false);
    	} else {
    		lblLastNameValidationMsg.setText("Last Name is Required!");
    		lblLastNameValidationMsg.setAlignment(Pos.CENTER);
    		lblLastNameValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    /**
     * @param mobileNo : java.lang.String
     * @return boolean
     * returns boolean value based on the mobile no validation criteria
     */
    private boolean isMobileNoValidate(String mobileNo) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(mobileNo);
    	boolean isMobileNoValid = mobileNo.matches("^(?:0|94|\\+94|0094)?(7(0|1|2|5|6|7|8)\\d)\\d{6}$");
    	if(isNotNullAndNotEmpty && isMobileNoValid) {
    		lblMobilNoValidationMsg.setVisible(false);
    	} else {
    	    if(!isNotNullAndNotEmpty) {
    	    	lblMobilNoValidationMsg.setText("Mobile Number is Required!");
        		lblMobilNoValidationMsg.setAlignment(Pos.CENTER);
        		lblMobilNoValidationMsg.setVisible(true);
    	    } else {
    	    	lblMobilNoValidationMsg.setText("Invalid Mobile Number!");
        		lblMobilNoValidationMsg.setAlignment(Pos.CENTER);
        		lblMobilNoValidationMsg.setVisible(true);
    	    }
    	}
    	return isNotNullAndNotEmpty && isMobileNoValid;
    }
    
    
    /**
     * @param salary : java.lang.String
     * @return boolean
     * returns boolean value based on the salary validation criteria
     */
    private boolean isSalaryValidate(String salary) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(salary);
    	boolean isSalaryValidates = salary.matches("^[0-9]{3,7}(\\.){0,1}[0-9]{0,2}$");
    	if(isNotNullAndNotEmpty && isSalaryValidates) {
    		lblSalaryValidationMsg.setVisible(false);
    	} else {
    		if(!isNotNullAndNotEmpty) {
    			lblSalaryValidationMsg.setText("Salary is Required!");
        		lblSalaryValidationMsg.setAlignment(Pos.CENTER);
        		lblSalaryValidationMsg.setVisible(true);
    		} else {
    			lblSalaryValidationMsg.setText("Invalid Format for Salary!");
        		lblSalaryValidationMsg.setAlignment(Pos.CENTER);
        		lblSalaryValidationMsg.setVisible(true);
    		}
    	}
    	return isNotNullAndNotEmpty && isSalaryValidates;
    }
    
    /**
     * @return boolean
     * returns boolean value based on the gender validation criteria
     */
    private boolean isGenderValidate() {
    	boolean isGenderNotSelected = cmbGender.getSelectionModel().isEmpty();
    	if(isGenderNotSelected) {
    		lblGenderValidationMsg.setText("Gender is Required!");
    		lblGenderValidationMsg.setAlignment(Pos.CENTER);
    		lblGenderValidationMsg.setVisible(true);
    	} else {
    		lblGenderValidationMsg.setVisible(false);
    	}
    	return isGenderNotSelected;
    }
    
    
    /**
     * @param age : java.lang.String
     * @return boolean
     * returns boolean value based on the age validation criteria
     */
    private boolean isAgeValidate(String age) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(age);
    	boolean isAgeValidates = age.matches("^[0-9]{1}[0-9]{1,2}$");
    	if(isNotNullAndNotEmpty && isAgeValidates) {
    		lblAgeValidationMsg.setVisible(false);
    	} else {
    	    if(!isNotNullAndNotEmpty) {
    	    	lblAgeValidationMsg.setText("Age is Required!");
        		lblAgeValidationMsg.setAlignment(Pos.CENTER);
        		lblAgeValidationMsg.setVisible(true);
    	    } else {
    	    	lblAgeValidationMsg.setText("Invalid Format for Age!");
        		lblAgeValidationMsg.setAlignment(Pos.CENTER);
        		lblAgeValidationMsg.setVisible(true);
    	    }
    	}
    	return isNotNullAndNotEmpty && isAgeValidates;
    }
    
    /**
     * @param nicNo : java.lang.String
     * @return boolean
     * returns boolean value based on the NIC no validation criteria
     */
    private boolean isNicNoValidate(String nicNo) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(nicNo);
    	boolean isNicNoValid = nicNo.matches("^([0-9]{9}[x|X|v|V]|[0-9]{12})$");
    	
    	if(isNotNullAndNotEmpty && isNicNoValid) {
    		lblNicNoValidationMsg.setVisible(false);
    	} else {
    		if(!isNotNullAndNotEmpty) {
    			lblNicNoValidationMsg.setText("NIC is Required!");
    			lblNicNoValidationMsg.setAlignment(Pos.CENTER);
    			lblNicNoValidationMsg.setVisible(true);
    		} else {
    			lblNicNoValidationMsg.setText("Invalid NIC Number!");
    			lblNicNoValidationMsg.setAlignment(Pos.CENTER);
    			lblNicNoValidationMsg.setVisible(true);
    		}
    	}
    	return isNotNullAndNotEmpty && isNicNoValid;
    }
    
    /**
     * @param address : java.lang.String
     * @return boolean
     * returns boolean value based on the address no validation criteria
     */
    private boolean isAddressValidate(String address) {
    	boolean isNotNullAndNotEmpty = isNotNullAndNotEmpty(address);
    	if(isNotNullAndNotEmpty) {
    		lblAddressValidationMsg.setVisible(false);
    	} else {
    		lblAddressValidationMsg.setText("Address is Required!");
    		lblAddressValidationMsg.setAlignment(Pos.CENTER);
    		lblAddressValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }
    
    
    /**
     * @param value : java.lang.String
     * @return boolean
     * returns boolean value based on checking the given string null or empty criteria
     */
    private boolean isNotNullAndNotEmpty(String value) {
    	return null != value && !value.isEmpty();
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs reset all details when ActionEvent performs on reset button
     */
    @FXML
    public void btnResetCustomerOnAction(ActionEvent actionEvent) {
    	resetCustomerDetails();
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs cookie validation, data validation and finally save customer process 
     * by communicating with the server side
     */
    @FXML
    public void btnSaveCustomerOnAction(ActionEvent actionEvent) {
    	if(CookieManager.isValidCookie()) {
    		if(isValidate()) {
		        try {
		            boolean isCustomerSaved = manageCustomerService.saveCustomer(getCustomer());
		            Alert alert;
		            if (isCustomerSaved) {
		                 alert = new Alert(Alert.AlertType.INFORMATION);
		                 alert.setContentText("Customer Saved Successfully!");
		                 loadNextCustomerId();
		                 loadNextUserId();
		                 loadAllCustomers();
		                 resetCustomerDetails();
		            } else {
		                 alert = new Alert(Alert.AlertType.ERROR);
		                 alert.setContentText("OOPS.. Something Happened!");
		            }
		            alert.show();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
    	   } else {
    		   Alert alert = new Alert(AlertType.WARNING);
    		   alert.setContentText("All Fields are Required!");
    		   alert.show();
    	   }
	   }
    }

    /**
     * @return lk.edu.cis.sliit.beds.dto.CustomerDto
     * provides a customerDto object based on the current data provided in the customer form
     */
    private CustomerDto getCustomer() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerId(Integer.parseInt(txtCustomerId.getText()));
        customerDto.setUserId(Integer.parseInt(txtUserId.getText()));
        customerDto.setFirstName(txtFirstName.getText());
        customerDto.setLastName(txtLastName.getText());
        customerDto.setMobileNo(Integer.parseInt(txtMobileNo.getText()));
        customerDto.setSalary(Double.parseDouble(txtSalary.getText()));
        customerDto.setGender(cmbGender.getSelectionModel().getSelectedItem());
        customerDto.setAge(Integer.parseInt(txtAge.getText()));
        customerDto.setNicNo(txtNicNo.getText());
        customerDto.setAddress(txtAddress.getText());
        return customerDto;
    }
    
    /**
     * resets all the customer form values to its default
     */
    private void resetCustomerDetails() {
        txtFirstName.setText("");
        txtLastName.setText("");
        txtMobileNo.setText("");
        txtSalary.setText("");
        txtAge.setText("");
        txtNicNo.setText("");
        txtAddress.setText("");
    }
    
    /**
     * @return boolean
     * returns a boolean values based on validation criteria for all fields
     */
    private boolean isValidate() {
        return isFirstNameValidate(txtFirstName.getText())
                && isLastNameValidate(txtLastName.getText())
                && isMobileNoValidate(txtMobileNo.getText())
                && isSalaryValidate(txtSalary.getText())
                && isAgeValidate(txtAge.getText())
                && isNicNoValidate(txtNicNo.getText())
                && isAddressValidate(txtAddress.getText());
    }
}
