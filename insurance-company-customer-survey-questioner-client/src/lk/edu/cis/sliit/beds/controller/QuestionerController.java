package lk.edu.cis.sliit.beds.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto;
import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in CustomerSurveyQuestioner GUI
 * This Class Consists of Following Main Features:
 *  1) Displays the Questioner Questions and Relevant Answers to the Customer
 *  2) Allows to Answer all the Answers and Manages the Answering the Questions
 *  3) Indicates the Progress of the Customer's Questioner Answering Progress using
 *     Progress Bars and Progress Indicators
 * 
 * @author Punsara Prathibha - 2010866
 */
public class QuestionerController implements Initializable {
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Root Pane for Questioner Panel 
	 */
    @FXML
    public AnchorPane rootPane;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for send feedback button
	 */
    @FXML
    public Button btnSendFeedback;
    /**
	 * javafx.scene.control.TextField
	 * Creates an object reference for service id hidden field
	 */
    @FXML
    public TextField txtServiceId;
    /**
	 * javafx.scene.control.ComboBox<java.lang.String>
	 * Creates an object reference for logout combo box
	 */
    @FXML
    public ComboBox<String> cBoxLogOut;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display current time
	 */
    @FXML
    public Label lblTime;
    /**
	 * javafx.scene.image.ImageView
	 * Creates an object reference for back button ImageView
	 */
    @FXML
    public ImageView backButton;
    /**
	 * javafx.scene.control.ProgressBar
	 * Creates an object reference for questioner progress bar
	 */
    @FXML
    public ProgressBar questionerProgressBar;
    /**
	 * javafx.scene.control.ProgressIndicator
	 * Creates an object reference for questioner progress indicator
	 */
    @FXML
    public ProgressIndicator questionerProgressIndicator;

    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display first question of given group of questions
	 */
    @FXML
    public Label lblQuestion1;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display second question of given group of questions
	 */
    @FXML
    public Label lblQuestion2;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display third question of given group of questions
	 */
    @FXML
    public Label lblQuestion3;

    /**
	 * javafx.scene.layout.StackPane
	 * Creates an object reference for display first answer of given group of questions
	 */
    @FXML
    public StackPane lblAnswer1;
    /**
	 * javafx.scene.layout.StackPane
	 * Creates an object reference for display second answer of given group of questions
	 */
    @FXML
    public StackPane lblAnswer2;
    /**
	 * javafx.scene.layout.StackPane
	 * Creates an object reference for display third answer of given group of questions
	 */
    @FXML
    public StackPane lblAnswer3;

    
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display first question number of given group of questions
	 */
    @FXML
    public Label lblQuestionNo1;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display second question number of given group of questions
	 */
    @FXML
    public Label lblQuestionNo2;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display third question number of given group of questions
	 */
    @FXML
    public Label lblQuestionNo3;

    
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display first question id of given group of questions
	 */
    @FXML
    public Label lblQuestionId1;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display second question id of given group of questions
	 */
    @FXML
    public Label lblQuestionId2;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display third question id of given group of questions
	 */
    @FXML
    public Label lblQuestionId3;

    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display first mapping id of given group of questions
	 */
    @FXML
    public Label lblMappingId1;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display second mapping id of given group of questions
	 */
    @FXML
    public Label lblMappingId2;
    /**
	 * javafx.scene.control.Label
	 * Creates an object reference for display third mapping id of given group of questions
	 */
    @FXML
    public Label lblMappingId3;
    
    /**
     * Creates a boolean reference variable to manage and validate the empty answers
     * provided scenario
     */
    private static boolean is_empty_answers_found = false;
    /**
     * Creates a boolean reference variable to manage and validate the is multiple questions
     * available scenario
     */
    private static boolean is_multiple_questions_available = false;
    /**
     * Creates a boolean reference variable to manage and validate the is 
     * customer survey end scenario
     */
    private static boolean isCustomerSurveyEnd = false;
    /**
     * java.util.List<QuestionerDto>
     * Creates a Object reference to keep all questions and answers mapped with them
     */
    private static List<QuestionerDto> questionsMappedWithAnswers;
    /**
     * Creates a boolean reference variable to keep questioner progress to manage 
     * progress bar and progress indicator along with the progress of survey
     */
    private static double queationer_progress = 0.0;

    /**
     * lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService
     * Creates a Object reference for CustomerSurveyService
     */
    private CustomerSurveyService customerSurveyService;

    /**
     * @param location : java.net.URL
     * @param resources : java.util.ResourceBundle
     * initializes lk.edu.cis.sliit.beds.controller.QuestionerController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
        	if(CookieManager.isValidCookie()) {
	            customerSurveyService = (CustomerSurveyService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.CUSTOMER_SURVEY);
	            questionsMappedWithAnswers = customerSurveyService.getQuestionsMappedWithAnswers();
	            loadQuestioner();
	            loadTime();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * perform current digital time updated on the GUI for each second
     */
    private void loadTime() {
    	Timeline newTimeLine=new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblTime.setText(new SimpleDateFormat("hh:mm:ss a" ).format(new Date()));
                lblTime.setFocusTraversable(false);
                lblTime.setDisable(true);
                lblTime.setAlignment(Pos.CENTER);
            }

        }),new KeyFrame(Duration.seconds(1)));
        newTimeLine.setCycleCount(Animation.INDEFINITE);
        newTimeLine.play();
    }

    
    /**
     * @param serviceId : String
     * @param userName : String
     * retrieve passing data from other controllers after controller initialization process finished
     */
    public void initData(String serviceId, String userName) {
        txtServiceId.setText(serviceId);
        cBoxLogOut.getItems().clear();
        cBoxLogOut.getItems().add(userName);
        cBoxLogOut.getItems().add("LogOut");
        cBoxLogOut.getSelectionModel().select(0);
    }

    /**
     * @throws Exception
     * load questioner questions and feedback answers retrieved from the server
     */
    private void loadQuestioner() throws Exception {
    	questionerProgressBar.setProgress(queationer_progress);
    	questionerProgressIndicator.setProgress(queationer_progress);
    	if(questionsMappedWithAnswers.size() >= 3) {
    		is_multiple_questions_available = true;
    		
    		int questionCount = 1;
    		for(int index = 0; index < 3; index++) {
    			insertIntoComponents(questionsMappedWithAnswers.get(index), questionCount++);
    		}
    		
    		for(int index = 0; index < 3; index++) {
    			questionsMappedWithAnswers.remove(0);
    		}
    	} else if (questionsMappedWithAnswers.size() > 0) {
    		is_multiple_questions_available = false;
    		
    		insertIntoComponents(questionsMappedWithAnswers.get(0), 0);
    		isCustomerSurveyEnd = true;
    		questionsMappedWithAnswers.remove(0);
    	} else {
        	lblAnswer1.getChildren().clear();
    	}
    }
    
    /**
     * @param questionerDto : QuestionerDto
     * @param index : int
     * performs inserting data into input components based on the question type 
     */
    private void insertIntoComponents(QuestionerDto questionerDto, int index) {
    	String answers = questionerDto.getAnswer();
    	TilePane tilePane = new TilePane();
        ToggleGroup toggleGroup = new ToggleGroup();
        if ("TEXT".equals(answers)) {
        	TextArea textAreaInput =new TextArea();
        	textAreaInput.setStyle("-fx-font-family: Cambria; -fx-font-style: Italic; -fx-font-size: 12.0;");
        	textAreaInput.setPrefRowCount(2);
        	textAreaInput.setPrefColumnCount(16);
            tilePane.getChildren().add(textAreaInput);
        } else {
            for (String answer : answers.split("[\\,]")) {
                RadioButton radioButton = new RadioButton(answer);
                radioButton.setToggleGroup(toggleGroup);
                radioButton.setStyle("-fx-padding: 5px; -fx-font-family: Cambria; -fx-font-style: Italic; -fx-font-size: 12.0;");
                tilePane.getChildren().add(radioButton);
            }
        }
        questionerDto.setFeedback(tilePane);
        
        if(index == 1) {
        	lblQuestionNo1.setText("Q" + questionerDto.getQuestionNo().toString());
        	lblQuestionId1.setText(questionerDto.getQuestionId().toString());
        	lblQuestion1.setText(questionerDto.getQuestion());
        	lblQuestion1.setWrapText(true);
        	lblAnswer1.getChildren().clear();
        	lblAnswer1.getChildren().add((Node) questionerDto.getFeedback());
        	lblMappingId1.setText(questionerDto.getMappingId().toString());
        } else if(index == 2) {
        	lblQuestionNo2.setText("Q" + questionerDto.getQuestionNo().toString());
        	lblQuestionId2.setText(questionerDto.getQuestionId().toString());
        	lblQuestion2.setText(questionerDto.getQuestion());
        	lblQuestion2.setWrapText(true);
        	lblAnswer2.getChildren().clear();
        	lblAnswer2.getChildren().add((Node) questionerDto.getFeedback());
        	lblMappingId2.setText(questionerDto.getMappingId().toString());
        } else if(index == 3) {
        	lblQuestionNo3.setText("Q" + questionerDto.getQuestionNo().toString());
        	lblQuestionId3.setText(questionerDto.getQuestionId().toString());
        	lblQuestion3.setText(questionerDto.getQuestion());
        	lblQuestion3.setWrapText(true);
        	lblAnswer3.getChildren().clear();
        	lblAnswer3.getChildren().add((Node) questionerDto.getFeedback());
        	lblMappingId3.setText(questionerDto.getMappingId().toString());
        } else if(index == 0) {
        	lblQuestionNo1.setText("Q" + questionerDto.getQuestionNo().toString());
        	lblQuestionId1.setText(questionerDto.getQuestionId().toString());
        	lblQuestion1.setText(questionerDto.getQuestion());
        	lblQuestion1.setWrapText(true);
        	lblAnswer1.getChildren().clear();
        	lblAnswer1.getChildren().add((Node) questionerDto.getFeedback());
        	lblAnswer2.getChildren().clear();
        	lblAnswer3.getChildren().clear();
        	lblQuestion2.setText("");
        	lblQuestion3.setText("");
        	lblQuestionNo2.setText("");
        	lblQuestionNo3.setText("");
        	lblMappingId1.setText(questionerDto.getMappingId().toString());
        }
    }

    /**
     * @param actionEvent : ActionEvent
     * perform validating the cookie of the current user and validating whether given all questions are
     * answered and finally sends relevant data to the server to save in the database
     */
    @FXML
    public void btnSendFeedbackOnAction(ActionEvent actionEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
        		is_empty_answers_found = false;
        		List<CustomerFeedbackDto> customerFeedbacks = getCustomerFeedback();
        		if(!is_empty_answers_found && !customerFeedbacks.isEmpty()) {
        			boolean isFeedbackSaved = customerSurveyService.saveCustomerFeedback(customerFeedbacks);
    	            if (isFeedbackSaved) {
    	               if(isCustomerSurveyEnd) {
    	            	   Alert alert = new Alert(Alert.AlertType.INFORMATION);
       	                   alert.setContentText("Thanks for your feedback!\n It Means a Lot to Us");
       	                   alert.show();
    	               }
    	               if(queationer_progress != 0.9) {
    	            	   queationer_progress += 0.3;
    	               } else {
    	            	   queationer_progress = 1.0;
    	               }
    	               loadQuestioner();
    	            } else {
    	            	Alert alert = new Alert(Alert.AlertType.ERROR);
    	                alert.setContentText("OOPS.. Something went wrong!");
    	                alert.show();
    	            }
        		} else {
        			Alert alert = new Alert(AlertType.WARNING);
        			alert.setContentText("Please Answer All Questions");
        			alert.show();
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return java.util.List<CustomerFeedbackDto>
     * returns list of customer feedbacks based on no of questions available
     */
    private List<CustomerFeedbackDto> getCustomerFeedback() {
        List<CustomerFeedbackDto> questionerFeedbacks = new ArrayList<>();
    	if(is_multiple_questions_available) {
    		
    		CustomerFeedbackDto feedbackDto1 = getFeedbackDto(Integer.parseInt(lblMappingId1.getText()), 
    				Integer.parseInt(lblQuestionId1.getText()), getFeedback(((TilePane) lblAnswer1.getChildren().get(0))));
    		questionerFeedbacks.add(feedbackDto1);
            
    		CustomerFeedbackDto feedbackDto2 = getFeedbackDto(Integer.parseInt(lblMappingId2.getText()), 
    				Integer.parseInt(lblQuestionId2.getText()), getFeedback((TilePane) lblAnswer2.getChildren().get(0)));
    		questionerFeedbacks.add(feedbackDto2);
            
    		CustomerFeedbackDto feedbackDto3 =  getFeedbackDto(Integer.parseInt(lblMappingId3.getText()), 
    				Integer.parseInt(lblQuestionId3.getText()), getFeedback((TilePane) lblAnswer3.getChildren().get(0)));
    		questionerFeedbacks.add(feedbackDto3);
    	} else {
    		CustomerFeedbackDto feedbackDto = getFeedbackDto(Integer.parseInt(lblMappingId1.getText()), 
    				Integer.parseInt(lblQuestionId1.getText()), getFeedback((TilePane) lblAnswer1.getChildren().get(0)));
    		questionerFeedbacks.add(feedbackDto);
    	}
    	return questionerFeedbacks;
    }
    
    /**
     * @param mappingId : java.lang.Integer
     * @param questionId : java.lang.Integer
     * @param feedback : java.lang.String
     * @return lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto
     * returns unit wise customer feedback details submitted in the questioner
     */
    private CustomerFeedbackDto getFeedbackDto(Integer mappingId, Integer questionId, String feedback) {
    	 CustomerFeedbackDto customerFeedbackDto = new CustomerFeedbackDto();
         customerFeedbackDto.setCustomerFeedbackId(mappingId);
         customerFeedbackDto.setServiceId(Integer.parseInt(txtServiceId.getText()));
         customerFeedbackDto.setQuestionId(questionId);
         customerFeedbackDto.setCustomerUserName(cBoxLogOut.getItems().get(0));
         customerFeedbackDto.setCustomerFeedback(feedback);
         return customerFeedbackDto;
    }

    /**
     * @param tilePane : javafx.scene.layout.TilePane
     * @return java.lang.String
     * returns the user feedbacks based on the inputs in the given TilePane
     */
    private String getFeedback(TilePane tilePane) {
        Node node = (Node) tilePane.getChildren().get(0);
        String feedback = null;
        if (node instanceof TextArea) {
            feedback = ((TextArea) node).getText();
        } else if (node instanceof RadioButton) {
        	RadioButton selectedRadioBtn = (RadioButton) ((RadioButton) node).getToggleGroup().getSelectedToggle();
            if(null != selectedRadioBtn) {
            	feedback = selectedRadioBtn.getText();
            }
        }
        
        if(null == feedback || feedback.isEmpty()) {
        	is_empty_answers_found = true;
        }
        return feedback;
    }

    /**
     * @param mouseEvent : MouseEvent
     * redirect back to customer portal GUI with passing the init data
     */
    @FXML
    public void backButtonOnClick(MouseEvent mouseEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/CustomerPortal.fxml"));
            Scene scene = new Scene(loader.load());
            Stage stage = (Stage) this.rootPane.getScene().getWindow();
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.setResizable(false);
            
            CustomerPortalController controller = loader.getController();
            controller.initData(cBoxLogOut.getItems().get(0));
            stage.show();

            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
            translateTransition.setFromX(-scene.getWidth());
            translateTransition.setToX(0);
            translateTransition.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @param actionEvent : ActionEvent
     * log out the customer from the system by removing the cookie from the memory 
     * and redirects back to the user login
     */
    @FXML
    public void cBoxLogOutOnAction(ActionEvent actionEvent) {
    	String selectedText = cBoxLogOut.getSelectionModel().getSelectedItem();
    	if("LogOut".equals(selectedText)) {
    		try {
    			CookieManager.getInstance().logoutUser();
    			
                FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/Login.fxml"));
                Scene scene = new Scene(loader.load());
                Stage stage = (Stage) this.rootPane.getScene().getWindow();
                stage.setScene(scene);
                stage.centerOnScreen();
                stage.setResizable(false);
                stage.show();

                TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
                translateTransition.setFromX(-scene.getWidth());
                translateTransition.setToX(0);
                translateTransition.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
    	}
    }
}
