package lk.edu.cis.sliit.beds.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.FeedbackDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory.ServiceTypes;
import lk.edu.cis.sliit.beds.service.custom.AdminDashboardService;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in AdminDashboard GUI
 * This Class Consists of Following Main Features:
 *  1) Displays CustomerFeedback and CustomerRecomendation against time bar chart
 *  2) Displays New Customers Interaction with the business against time line chart
 *  3) Displays Categories/Types of customers by their attitude against the business pie chart
 *  4) Displays Total No of Customer Feedbacks occurred in the current day and the positive, neutral and
 *     negative feedbacks found for the current day
 * 
 * @author Punsara Prathibha - 2010866
 */
public class AdminDashboardController implements Initializable {
	/**
	 * javafx.scene.image.ImageView
	 * Creates an object reference for Customer Feedback Chart ImageView
	 */
	@FXML
	public ImageView imgCustomerFeedbackChart;
	/**
	 * javafx.scene.image.ImageView
	 * Creates an object reference for Quality of Service Chart ImageView
	 */
	@FXML
	public ImageView imgQualityOfServiceChart;
	/**
	 * javafx.scene.image.ImageView
	 * Creates an object reference for Customer Attitude Chart ImageView
	 */
	@FXML
	public ImageView imgCustomerAttitudeChart;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for Total feedback Label
	 */
	@FXML
	public Label lblTotalFeedbacks;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for Positive feedback Label
	 */
	@FXML
	public Label lblPositiveFeedbacks;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for Neutral feedback Label
	 */
	@FXML
	public Label lblNeutralFeedbacks;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for Negative feedback Label
	 */
	@FXML
	public Label lblNegetiveFeedbacks;
	
	/**
     * lk.edu.cis.sliit.beds.service.custom.AdminDashboardService
     * Creates an object reference for AdminDashboardService
     */
	private AdminDashboardService adminDashboardService;

	/**
     *  @param location : java.net.URL
     *  @param resources : java.util.ResourceBundle
     *  initializes lk.edu.cis.sliit.beds.controller.AdminDashboardController
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			adminDashboardService = (AdminDashboardService) ProxyHandler.getInstance().getService(ServiceTypes.ADMIN_DASHBOARD);
			if(CookieManager.isValidCookie()) {
				loadCustomerRecomendationAgaintTimeChart();
				loadQualityOfServiceAgaintTimeLineChart();
				loadCustomerAgainstAttitudePieChart();
				viewTodayNoOfCustomerFeedbacks();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws Exception
	 * loads the customer recommendation against time chart by retrieving data from the server side as
	 * an byte array and decode the byte array to javafx.scene.image.Image 
	 */
	private void loadCustomerRecomendationAgaintTimeChart() throws Exception  {
		byte[] encodedImage = adminDashboardService.getCustomerRecomendationAgaintTimeBarChart();
		if(null != encodedImage) {
			imgCustomerFeedbackChart.setImage(getDecodedImage(encodedImage));
		}
	}
	
	
	/**
	 * @throws Exception
	 * loads the quality of service against time chart by retrieving data from the server side as
	 * an byte array and decode the byte array to javafx.scene.image.Image 
	 */
	private void loadQualityOfServiceAgaintTimeLineChart() throws Exception {
		byte[] encodedImage = adminDashboardService.getQualityOfServiceAgaintTimeLineChart();
		if(null != encodedImage) {
			imgQualityOfServiceChart.setImage(getDecodedImage(encodedImage));
		}
	}
	
	/**
	 * @throws Exception
	 * loads the customer against attitude chart by retrieving data from the server side as
	 * an byte array and decode the byte array to javafx.scene.image.Image 
	 */
	private void loadCustomerAgainstAttitudePieChart() throws Exception {
		byte[] encodedImage = adminDashboardService.getCustomerAgainstAttitudePieChart();
		if(null != encodedImage) {
			imgCustomerAttitudeChart.setImage(getDecodedImage(encodedImage));
		}
	}
	
	/**
	 * @param encodedImage : byte[]
	 * @return javafx.scene.image.Image
	 * @throws IOException
	 * returns javafx.scene.image.Image with the process of decoding the provided encoded byte array
	 */
	private Image getDecodedImage(byte[] encodedImage) throws IOException {
		byte[] decodedImage = Base64.getDecoder().decode(encodedImage);
		BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedImage));
		return SwingFXUtils.toFXImage(bufferedImage, null);
	}
	
	/**
	 * @throws Exception
	 * sets the current day no of feedbacks happens in the questioner 
	 */
	private void viewTodayNoOfCustomerFeedbacks() throws Exception {
		FeedbackDto feedbackDto = adminDashboardService.getTodayCustomerFeedback();
		
		lblTotalFeedbacks.setText(feedbackDto.getTotalNoOfFeedbacks().toString());
		lblPositiveFeedbacks.setText(feedbackDto.getNoOfPositiveFeedbacks() + " %");
		lblNeutralFeedbacks.setText(feedbackDto.getNoOfNeutralFeedbacks() + " %");
		lblNegetiveFeedbacks.setText(feedbackDto.getNoOfNegetiveFeedbacks() + " %");
	}
}
