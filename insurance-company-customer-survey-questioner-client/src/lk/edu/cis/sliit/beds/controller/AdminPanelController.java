package lk.edu.cis.sliit.beds.controller;


import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.edu.cis.sliit.beds.cookie.CookieManager;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in AdminPanel GUI
 * This Class Consists of Following Main Features:
 *  1) Allows to Navigate between AdminDashboard GUI, ManageCustomer GUI, ManageQuestioner GUI 
 *     using the Side Bar in left side of the Application
 *  2) Displays the Current System Running Digital Time
 * 
 * @author Punsara Prathibha - 2010866
 */
public class AdminPanelController implements Initializable {
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Root Pane for the Admin Panel 
	 */
	@FXML
	public AnchorPane rootPane;
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Sub Pane for the Admin Panel 
	 * which displays other forms when navigate using the side bar 
	 */
	@FXML
    public AnchorPane subPanel;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for Current Time Displaying Label
	 */
    @FXML
    public Label lblTime;

    /**
     *  @param location : java.net.URL
     *  @param resources : java.util.ResourceBundle
     *  initializes lk.edu.cis.sliit.beds.controller.AdminPanelController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
        	if(CookieManager.isValidCookie()) {
        		loadAdminDashboard();
        		loadTime();
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * views the current digital time by updating for every second
     */
    private void loadTime() {
    	Timeline newTimeLine=new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblTime.setText(new SimpleDateFormat("hh:mm:ss a" ).format(new Date()));
                lblTime.setFocusTraversable(false);
                lblTime.setDisable(true);
                lblTime.setAlignment(Pos.CENTER);
            }

        }),new KeyFrame(Duration.seconds(1)));
        newTimeLine.setCycleCount(Animation.INDEFINITE);
        newTimeLine.play();
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * performs javafx.animation.ScaleTransition animations to increase scale to 1.1 (zoom)
     * on side bar HBoxes on mouse entered MouseEvent
     */
    @FXML
    public void hBoxMouseEntered(MouseEvent mouseEvent) {
        HBox source = (HBox) mouseEvent.getSource();
        source.setStyle("-fx-background-color:   #9c9c9c;");
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(1200), source);
        scaleTransition.setToX(1.1);
        scaleTransition.setToY(1.1);
        scaleTransition.play();
        source.setEffect(null);
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * performs javafx.animation.ScaleTransition animations to decrease scale back to 1.0
     * on side bar HBoxes on mouse exited MouseEvent
     */
    @FXML
    public void hBoxMouseExited(MouseEvent mouseEvent) {
        HBox source = (HBox) mouseEvent.getSource();
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(1200), source);
        source.setStyle("-fx-background-color:  #000000;");
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        source.setEffect(null);
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * loads the admin dashboard screen based on the current cookie validation process on 
     * javafx.scene.layout.HBox Mouse click event
     */
    @FXML
    public void hBoxDashboardOnClick(MouseEvent mouseEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
        		loadAdminDashboard();
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * loads the manage customer screen based on the current cookie validation process on 
     * javafx.scene.layout.HBox Mouse click event
     */
    @FXML
    public void hBoxManageCustomerOnClick(MouseEvent mouseEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
        		loadManageCustomerForm();
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * loads the manage question screen based on the current cookie validation process on 
     * javafx.scene.layout.HBox Mouse click event
     */
    @FXML
    public void hBoxManageQuestionerOnClick(MouseEvent mouseEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
        		loadManageQuestionerForm();
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * log out the admin user from the system by removing the cookie from the memory 
     * and redirects back to the user login on javafx.scene.layout.HBox Mouse click event
     */
    @FXML
    public void hBoxLogOutOnClick(MouseEvent mouseEvent) {
		try {
			CookieManager.getInstance().logoutUser();
			
			Parent root = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/Login.fxml"));
			Scene scene = new Scene(root);
	        Stage stage = (Stage) this.rootPane.getScene().getWindow();
	        stage.setScene(scene);
	        stage.centerOnScreen();
	        stage.setResizable(false);
	        stage.show();

	        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
	        translateTransition.setFromX(-scene.getWidth());
	        translateTransition.setToX(0);
	        translateTransition.play();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    /**
     * @throws IOException
     * clears the previous dashboard screen and loads the new dashboard screen 
     */
    private void loadAdminDashboard() throws IOException {
        this.subPanel.getChildren().clear();
        AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/AdminDashboard.fxml"));
        this.subPanel.getChildren().add(pane);
    }

    /**
     * @throws IOException
     * clears the previous manage customer screen and loads the new manage customer screen 
     */
    private void loadManageCustomerForm() throws IOException {
        this.subPanel.getChildren().clear();
        AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/ManageCustomer.fxml"));
        this.subPanel.getChildren().add(pane);
    }

    /**
     * @throws IOException
     * clears the previous manage questioner screen and loads the new manage questioner screen 
     */
    private void loadManageQuestionerForm() throws IOException {
        this.subPanel.getChildren().clear();
        AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/ManageQuestioner.fxml"));
        this.subPanel.getChildren().add(pane);
    }
}
