package lk.edu.cis.sliit.beds.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.dto.ValueTextDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.ManageQuestionerService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in AdminManageQuestioner GUI
 * This Class Consists of Following Main Features:
 *  1) Allows to Save New Question
 *  2) Allows to Save New Answer 
 *  3) Allows to Map Particular Question with a Particular Answer for the QUestioner
 *  4) Displays All Questions Currently Available in the System
 *  5) Displays All Answers Currently Available in the System
 *  6) Displays All Question Answer Mappings Currently Available in the System
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ManageQuestionerController implements Initializable {

	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for answer id hidden field
	 */
	@FXML
    public TextField txtAnswerId;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for answer input field
	 */
	@FXML
    public TextField txtAnswer;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for question id hidden field
	 */
	@FXML
    public TextField txtQuestionId;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for question input field
	 */
	@FXML
    public TextField txtQuestion;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for mapping id hidden field
	 */
	@FXML
    public TextField txtMappingId;
	/**
	 * javafx.scene.control.ComboBox<java.lang.String>
	 * Creates an object reference for question combo box
	 */
	@FXML
    public ComboBox<String> cmbQuestion;
	/**
	 * javafx.scene.control.ComboBox<java.lang.String>
	 * Creates an object reference for answer combo box
	 */
	@FXML
    public ComboBox<String> cmbAnswer;
	/**
	 * javafx.scene.control.TableView<lk.edu.cis.sliit.beds.dto.QuestionerDto>
	 * Creates an object reference for mapping table
	 */
	@FXML
    public TableView<QuestionerDto> tblMapping;
	/**
	 * javafx.scene.control.TableView<lk.edu.cis.sliit.beds.dto.ValueTextDto>
	 * Creates an object reference for questions table
	 */
	@FXML
    public TableView<ValueTextDto> tblQuestion;
	/**
	 * javafx.scene.control.TableView<lk.edu.cis.sliit.beds.dto.ValueTextDto>
	 * Creates an object reference for answers table
	 */
	@FXML
    public TableView<ValueTextDto> tblAnswer;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for save question Button
	 */
	@FXML
    public Button btnSaveQuestion;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for save answer Button
	 */
	@FXML
    public Button btnSaveAnswer;
	/**
	 * javafx.scene.control.Button
	 * Creates an object reference for save mapping Button
	 */
	@FXML
    public Button btnSaveMapping;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for question validation message
	 */
	@FXML
	public Label lblQuestionValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for answer validation message
	 */
	@FXML
	public Label lblAnswerValidationMsg;
	/**
	 * javafx.scene.control.Label
	 * Creates an object reference for mapping validation message
	 */
	@FXML
	public Label lblMappingValidationMsg;

    /**
     * lk.edu.cis.sliit.beds.service.custom.ManageQuestionerService
     * Creates an object reference for ManageQuestionerService
     */
    private ManageQuestionerService manageQuestionerService;

    /**
     *  @param location : java.net.URL
     *  @param resources : java.util.ResourceBundle
     *  initializes lk.edu.cis.sliit.beds.controller.ManageQuestionerController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
        	if(CookieManager.isValidCookie()) {
	            this.manageQuestionerService = (ManageQuestionerService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.MANAGE_QUESTIONER);
	            loadAllQuestions();
	            loadAllAnswers();
	            loadAllQuestionAnswerMapping();
	
	            loadNextQuestionId();
	            loadNextAnswerId();
	            loadNextMappingId();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws Exception
     * performs operations to retrieve and load all question details in the table
     */
    private void loadAllQuestions() throws Exception {
        List<ValueTextDto> allQuestions = manageQuestionerService.findAllQuestions();

        tblQuestion.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("value"));
        tblQuestion.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("text"));
        tblQuestion.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("lastUpdatedDate"));

        tblQuestion.getItems().clear();
        tblQuestion.setItems(FXCollections.observableArrayList(allQuestions));

        for (ValueTextDto valueTextDto : allQuestions) {
            cmbQuestion.getItems().add(valueTextDto.getText());
        }
    }

    /**
     * @throws Exception
     * performs operations to retrieve and load all answers details in the table
     */
    private void loadAllAnswers() throws Exception {
        List<ValueTextDto> allAnswers = manageQuestionerService.findAllAnswers();

        tblAnswer.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("value"));
        tblAnswer.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("text"));
        tblAnswer.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("lastUpdatedDate"));

        tblAnswer.getItems().clear();
        tblAnswer.setItems(FXCollections.observableArrayList(allAnswers));
        
        for (ValueTextDto valueTextDto : allAnswers) {
            cmbAnswer.getItems().add(valueTextDto.getText());
        }
    }

    /**
     * @throws Exception
     * performs operations to retrieve and load all question answer mapping details in the table
     */
    private void loadAllQuestionAnswerMapping() throws Exception {
        List<QuestionerDto> allQuestionAnswerMappings = manageQuestionerService.findAllQuestionAnswerMappings();
        
        tblMapping.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("questionNo"));
        tblMapping.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("question"));
        tblMapping.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("answer"));
        tblMapping.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("lastUpdatedTime"));

        tblMapping.getItems().clear();
        tblMapping.setItems(FXCollections.observableArrayList(allQuestionAnswerMappings));
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs cookie validation, data validation and finally save answer process 
     * by communicating with the server side
     */
    @FXML
    public void btnSaveAnswerOnAction(ActionEvent actionEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
	        	String answer = txtAnswer.getText();
	        	answer = answer.isEmpty() ? "TEXT" : answer;
	            ValueTextDto valueTextDto = new ValueTextDto(Integer.parseInt(txtAnswerId.getText()), answer);
	            boolean isAnswerSaved = manageQuestionerService.saveAnswer(valueTextDto);
	            Alert alert;
	            if (isAnswerSaved) {
	                alert = new Alert(Alert.AlertType.INFORMATION);
	                alert.setContentText("Answer Saved Successfully!");
	                loadNextAnswerId();
	                loadAllAnswers();
	                resetAnswer();
	            } else {
	                alert = new Alert(Alert.AlertType.ERROR);
	                alert.setContentText("OOPS.. Something Went Wrong!");
	            }
	            alert.show();
	            lblAnswerValidationMsg.setVisible(false);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws Exception
     * retrieve next incremented answerId and set as answer id 
     */
    private void loadNextAnswerId() throws Exception {
        txtAnswerId.setText(manageQuestionerService.findNextAnswerId().toString());
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs cookie validation, data validation and finally save question process 
     * by communicating with the server side
     */
    @FXML
    public void btnSaveQuestionOnAction(ActionEvent actionEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
        		if(isQuestionValidate(txtQuestion.getText())) {
        			ValueTextDto valueTextDto = new ValueTextDto(Integer.parseInt(txtQuestionId.getText()), txtQuestion.getText());
    	            boolean isQuestionSaved = manageQuestionerService.saveQuestion(valueTextDto);
    	            Alert alert;
    	            if (isQuestionSaved) {
    	                alert = new Alert(Alert.AlertType.INFORMATION);
    	                alert.setContentText("Question Saved Successfully!");
    	                loadNextQuestionId();
    	                loadAllQuestions();
    	                resetQuestion();
    	            } else {
    	                alert = new Alert(Alert.AlertType.ERROR);
    	                alert.setContentText("OOPS.. Something Went Wrong!");
    	            }
    	            alert.show();
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * performs resetting question text
     */
    private void resetQuestion() {
    	txtQuestion.setText("");
    }
    
    /**
     * performs resetting answer text
     */
    private void resetAnswer() {
    	txtAnswer.setText("");
    }

    /**
     * @throws Exception
     * retrieve next incremented questionId and set as question id 
     */
    private void loadNextQuestionId() throws Exception {
        txtQuestionId.setText(manageQuestionerService.findNextQuestionId().toString());
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs cookie validation, data validation and finally save question answer mapping process 
     * by communicating with the server side
     */
    @FXML
    public void btnSaveMappingOnAction(ActionEvent actionEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
	            QuestionerDto questionerDto = new QuestionerDto();
	            questionerDto.setQuestionNo(Integer.parseInt(txtMappingId.getText()));
	            questionerDto.setQuestion(cmbQuestion.getSelectionModel().getSelectedItem());
	            questionerDto.setAnswer(cmbAnswer.getSelectionModel().getSelectedItem());
	            boolean isMappingSaved = manageQuestionerService.saveQuestionAnswerMapping(questionerDto);
	            Alert alert;
	            if (isMappingSaved) {
	                alert = new Alert(Alert.AlertType.INFORMATION);
	                alert.setContentText("Question-Answer Mapping Saved Successfully!");
	                loadNextMappingId();
	                loadAllQuestionAnswerMapping();
	            } else {
	                alert = new Alert(Alert.AlertType.ERROR);
	                alert.setContentText("OOPS.. Something Went Wrong!");
	            }
	            alert.show();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws Exception
     * retrieve data and load next incremented question answer mapping id
     */
    private void loadNextMappingId() throws Exception {
        txtMappingId.setText(manageQuestionerService.findNextMappingId().toString());
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for reset question data for ActionEvent on question reset button
     */
    @FXML
    public void btnResetQuestionOnAction(ActionEvent actionEvent) {
    	resetQuestion();
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for reset answer data for ActionEvent on answer reset button
     */
    @FXML
    public void btnResetAnswerOnAction(ActionEvent actionEvent) {
    	resetAnswer();
    }
    
    @FXML
    public void btnResetMappingOnAction(ActionEvent actionEvent) {}

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * requests for question validation and performs question save process if question validates
     */
    @FXML
    public void txtQuestionOnAction(ActionEvent actionEvent) {
    	String question = this.txtQuestion.getText();
    	if(isQuestionValidate(question)) {
    		btnSaveQuestionOnAction(actionEvent);
    	}
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * returns boolean value based on if question is validates
     */
    private boolean isQuestionValidate(String question) {
    	boolean isNotNullAndNotEmpty = null != question && !question.isEmpty();
    	if(isNotNullAndNotEmpty) {
    		lblQuestionValidationMsg.setVisible(false);
    	} else {
    		lblQuestionValidationMsg.setText("Please Enter a Question!");
    		lblQuestionValidationMsg.setAlignment(Pos.CENTER);
    		lblQuestionValidationMsg.setVisible(true);
    	}
    	return isNotNullAndNotEmpty;
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * performs answer validation and performs answer save process if answer validates
     */
    @FXML
    public void txtAnswerOnAction(ActionEvent actionEvent) {
    	String answer = this.txtAnswer.getText();
    	if(null != answer && !answer.isEmpty()) {
    		lblAnswerValidationMsg.setVisible(false);
    		btnSaveAnswerOnAction(actionEvent);
    	} else {
    		lblAnswerValidationMsg.setText("Provided Answer is Empty!\n It will save as \"TEXT\"");
    		lblAnswerValidationMsg.setAlignment(Pos.CENTER);
    		lblAnswerValidationMsg.setVisible(true);
    		btnSaveAnswerOnAction(actionEvent);
    	}
    }

    /**
     * @param actionEvent : javafx.event.ActionEvent
     * request focus to the answer mapping combo box when an ActionEvent triggered on question combo box
     */
    @FXML
    public void cmbQuestionOnAction(ActionEvent actionEvent) {
    	cmbAnswer.requestFocus();
    }
    
    /**
     * @param actionEvent : javafx.event.ActionEvent
     * request focus to the answer mapping save button when an ActionEvent triggered on answer combo box
     */
    @FXML
    public void cmbAnswerOnAction(ActionEvent actionEvent) {
    	btnSaveMapping.requestFocus();
    }
}
