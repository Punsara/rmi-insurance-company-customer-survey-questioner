package lk.edu.cis.sliit.beds.controller;

import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.edu.cis.sliit.beds.cookie.CookieManager;
import lk.edu.cis.sliit.beds.dto.CustomerServiceDto;
import lk.edu.cis.sliit.beds.proxy.ProxyHandler;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller which Manage and Responsible for all the Functionalities and Events occurs in CustomerPortal GUI
 * This Class Consists of Following Main Features:
 *  1) Displays all the customer services provided in the insurance company business
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerPortalController implements Initializable {
	/**
	 * javafx.scene.layout.AnchorPane
	 * Creates an object reference for Root Pane for the Customer Portal
	 */
	@FXML
    public AnchorPane rootPane;
	/**
	 * javafx.scene.layout.TilePane
	 * Creates an object reference for Tile Pane 
	 * which contains and displays all the services in the business
	 */
	@FXML
    public TilePane serviceTilePane;
	/**
	 * javafx.scene.control.TextField
	 * Creates an object reference for Text Field to keep user name as hidden field
	 */
	@FXML
    public TextField txtUserName;

    /**
     * lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService
     * Creates an object reference for CustomerSurveyService
     */
    private CustomerSurveyService customerSurveyService;

    /**
     * @param location : java.net.URL
     * @param resources : java.util.ResourceBundle
     * initializes lk.edu.cis.sliit.beds.controller.CustomerPortalController
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
        	if(CookieManager.isValidCookie()) {
        		customerSurveyService = (CustomerSurveyService) ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.CUSTOMER_SURVEY);
                loadCustomerServices();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param userName : java.lang.String
     */
    public void initData(String userName) {
        txtUserName.setText(userName);
    }

    /**
     * @throws Exception
     * load and show all services currently in the business to the GUI
     */
    private void loadCustomerServices() throws Exception {
        List<CustomerServiceDto> allCustomerServices = customerSurveyService.getAllCustomerServices();
        for (CustomerServiceDto serviceDto : allCustomerServices) {
            Label serviceLabel = new Label(serviceDto.getServiceName());
            serviceLabel.setId(serviceDto.getServiceId().toString());
            serviceLabel.setStyle("-fx-font-family: Cambria; -fx-font-style: Italic; -fx-font-size: 20.0; " +
                    "-fx-background-color: #d7ccc8; -fx-pref-height: 91.0; " +
                    "-fx-pref-width: 361.0; -fx-text-alignment: center; " +
                    "-fx-padding: 50px; -fx-border-color: black; -fx-border-width: 1;");
            serviceLabel.setOnMouseClicked(e -> onServiceSelect(e));
            serviceLabel.setOnMouseEntered(e -> lblOnMouseEntered(e));
            serviceLabel.setOnMouseExited(e -> lblOnMouseExited(e));
            serviceTilePane.getChildren().add(serviceLabel);
        }
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * perform ScaleTransition animation to 1.1 scale (zoom) when mouse Entered event triggered
     */
    @FXML
    public void lblOnMouseEntered(MouseEvent mouseEvent) {
        Label source = (Label) mouseEvent.getSource();
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(1200), source);
        scaleTransition.setToX(1.1);
        scaleTransition.setToY(1.1);
        scaleTransition.play();
        source.setEffect(null);
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * perform ScaleTransition animation back to 1.0 scale (normal) when mouse Exited event triggered
     */
    @FXML
    public void lblOnMouseExited(MouseEvent mouseEvent) {
        Label source = (Label) mouseEvent.getSource();
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(1200), source);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        source.setEffect(null);
    }

    /**
     * @param mouseEvent : javafx.scene.input.MouseEvent
     * redirects to the CustomerQuestionerPanel when successfully selected a given service 
     * which the customer is referring for the questioner
     */
    public void onServiceSelect(MouseEvent mouseEvent) {
        try {
        	if(CookieManager.isValidCookie()) {
	            String serviceId = ((Control) mouseEvent.getSource()).getId();
	            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/lk/edu/cis/sliit/beds/views/CustomerQuestionerPanel.fxml"));
	            Scene scene = new Scene(loader.load());
	            Stage stage = (Stage) this.rootPane.getScene().getWindow();
	            stage.setScene(scene);
	            stage.centerOnScreen();
	            stage.setResizable(false);
	
	            QuestionerController controller = loader.getController();
	            controller.initData(serviceId, txtUserName.getText());
	            stage.show();
	
	            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(800), scene.getRoot());
	            translateTransition.setFromX(-scene.getWidth());
	            translateTransition.setToX(0);
	            translateTransition.play();
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
