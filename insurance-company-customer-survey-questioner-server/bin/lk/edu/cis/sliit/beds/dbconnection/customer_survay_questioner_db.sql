DROP DATABASE IF EXISTS insurance_customer_feedback_survey;
CREATE DATABASE insurance_customer_feedback_survey;
USE insurance_customer_feedback_survey;

-- -- Creates UserRole Table
CREATE TABLE UserRole
(
    user_role_id INT           NOT NULL,
    user_role    VARCHAR(50)   NOT NULL,
    CONSTRAINT user_role_id_PK PRIMARY KEY (user_role_id)
) ENGINE = INNODB;

-- Creates User Table
CREATE TABLE User
(
    user_id           INT       NOT NULL,
    user_role_id      INT       NOT NULL,
    user_name         BLOB(50)  NOT NULL,
    user_password     BLOB(50)  NOT NULL,
    last_updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT user_id_PK PRIMARY KEY (user_id),
    CONSTRAINT user_role_id_FK FOREIGN KEY (user_role_id) REFERENCES UserRole (user_role_id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

-- Creates Customer Table
CREATE TABLE Customer
(
    customer_id        INT          NOT NULL,
    user_id            INT          NOT NULL,
    first_name         VARCHAR(10)  NOT NULL,
    last_name          VARCHAR(10)  NOT NULL,
    customer_address   VARCHAR(200) NOT NULL,
    customer_age       INT          NOT NULL,
    customer_mobile_no INT          NOT NULL,
    customer_salary    DOUBLE       NOT NULL,
    customer_gender    VARCHAR(10)  NOT NULL,
    nic_no             VARCHAR(15)  NOT NULL,
    last_updated_date  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT customer_id_PK PRIMARY KEY (customer_id),
    CONSTRAINT user_id_FK FOREIGN KEY (user_id) REFERENCES User (user_id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

-- Creates Customer Service Table
CREATE TABLE CustomerService
(
    service_id        INT          NOT NULL,
    service_name      VARCHAR(500) NOT NULL,
    last_updated_date TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT service_id_PK PRIMARY KEY (service_id)
) ENGINE = INNODB;

-- Creates Question Table
CREATE TABLE Question
(
    question_id       INT          NOT NULL,
    question_text     VARCHAR(500) NOT NULL,
    last_updated_date TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT question_id_PK PRIMARY KEY (question_id)
) ENGINE = INNODB;

-- Creates CustomerFeedback Table
CREATE TABLE CustomerFeedback
(
    customer_feedback_id INT          NOT NULL,
    customer_id          INT          NOT NULL,
    service_id           INT          NOT NULL,
    question_id          INT          NOT NULL,
    customer_feedback    VARCHAR(200) NOT NULL,
    feedback_date_time   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT customer_feedback_id_PK PRIMARY KEY (customer_feedback_id),
    CONSTRAINT customer_feedback_customer_id_FK FOREIGN KEY (customer_id) REFERENCES Customer (customer_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT customer_feedback_question_id_FK FOREIGN KEY (question_id) REFERENCES Question (question_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT customer_feedback_service_id_FK FOREIGN KEY (service_id) REFERENCES CustomerService (service_id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

-- Creates Answer Table
CREATE TABLE Answer
(
    answer_id         INT          NOT NULL,
    answer_text       VARCHAR(500) NOT NULL,
    last_updated_date TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT answer_id_PK PRIMARY KEY (answer_id)
) ENGINE = INNODB;

-- Creates QuestionAnswerMapping Table
CREATE TABLE QuestionAnswerMapping
(
    question_answer_id INT       NOT NULL,
    question_id        INT       NOT NULL,
    answer_id          INT       NOT NULL,
    last_updated_date  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT question_answer_id_PK PRIMARY KEY (question_answer_id),
    CONSTRAINT question_answer_mapping_question_id_FK FOREIGN KEY (question_id) REFERENCES Question (question_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT question_answer_mapping_answer_id_FK FOREIGN KEY (answer_id) REFERENCES Answer (answer_id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

-- Insert User Role Types interact with the System
INSERT INTO UserRole(user_role_id, user_role) VALUES (1, 'ADMIN');
INSERT INTO UserRole(user_role_id, user_role) VALUES (2, 'CUSTOMER');

-- Insert Admin User to Manage the System
INSERT INTO User(user_id, user_role_id, user_name, user_password, last_updated_date) VALUES (1, 1, AES_ENCRYPT('admin', 'admin'), AES_ENCRYPT('1234', '1234'), CURDATE());

-- Insert all Services Provided in the Business
INSERT INTO CustomerService(service_id, service_name, last_updated_date) VALUES(1, 'Short Term Investment', CURDATE());
INSERT INTO CustomerService(service_id, service_name, last_updated_date) VALUES(2, 'Make a Loan', CURDATE());
INSERT INTO CustomerService(service_id, service_name, last_updated_date) VALUES(3, 'Long Term Investment', CURDATE());
INSERT INTO CustomerService(service_id, service_name, last_updated_date) VALUES(4, 'Check Balance', CURDATE());