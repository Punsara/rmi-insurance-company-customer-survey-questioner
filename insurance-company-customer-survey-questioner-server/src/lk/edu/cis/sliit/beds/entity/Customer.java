package lk.edu.cis.sliit.beds.entity;

import javafx.scene.control.Alert.AlertType;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.service.custom.ManageCustomerService;
import lk.edu.cis.sliit.beds.service.custom.impl.ManageCustomerServiceImpl;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines Customer Entity to interact with the Database
 * This Entity is Currently using in this Application For :
 *  1) Save Customer
 *  2) Update Customer
 *  3) Search Customer
 * 
 * Includes all the attributes which contains in the database table
 * 
 * @author Punsara Prathibha - 2010866
 */
public class Customer implements Serializable {
    /**
	 * serialVersionUID of Customer
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for customer id
	 */
	private Integer customerId;
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for customer user id
	 */
    private Integer userId;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer first name
	 */
    private String firstName;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer last name
	 */
    private String lastName;
    /**
	 * java.lang.Integer
	 * Creates a private Object reference for customer mobile number
	 */
    private Integer mobileNo;
    /**
	 * java.lang.Double
	 * Creates a private Object reference for customer salary
	 */
    private Double salary;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer gender
	 */
    private String gender;
    /**
	 * java.lang.Integer
	 * Creates a private Object reference for customer age
	 */
    private Integer age;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer NIC number
	 */
    private String nicNo;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer address
	 */
    private String address;
    /**
	 * java.sql.Timestamp
	 * Creates a private Object reference for customer last updated date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public Customer() {
    }

    /**
     * @param customerId : java.lang.Integer
     * @param userId : java.lang.Integer
     * @param firstName : java.lang.String
     * @param lastName : java.lang.String
     * @param mobileNo : java.lang.Integer
     * @param salary : java.lang.Double
     * @param gender : java.lang.String
     * @param age : java.lang.Integer
     * @param nicNo : java.lang.String
     * @param address : java.lang.String
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public Customer(Integer customerId, Integer userId, String firstName, String lastName,
                    Integer mobileNo, Double salary, String gender, Integer age, String nicNo,
                    String address, Timestamp lastUpdatedDate) {
        this.customerId = customerId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNo = mobileNo;
        this.salary = salary;
        this.gender = gender;
        this.age = age;
        this.nicNo = nicNo;
        this.address = address;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId : java.lang.Integer
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId : java.lang.Integer
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return java.lang.String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName : java.lang.String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return java.lang.String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName : java.lang.String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo : java.lang.Integer
     */
    public void setMobileNo(Integer mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return java.lang.Double
     */
    public Double getSalary() {
        return salary;
    }

    /**
     * @param salary : java.lang.Double
     */
    public void setSalary(Double salary) {
        this.salary = salary;
    }

    /**
     * @return java.lang.String
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender : java.lang.String
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age : java.lang.Integer
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return java.lang.String
     */
    public String getNicNo() {
        return nicNo;
    }

    /**
     * @param nicNo : java.lang.String
     */
    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    /**
     * @return java.lang.String
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address : java.lang.String
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return lk.edu.cis.sliit.beds.dto.CustomerDto
     * converts Customer entity type to CustomerDto type
     */
    public CustomerDto toDto() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerId(customerId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setMobileNo(mobileNo);
        customerDto.setSalary(salary);
        customerDto.setGender(gender);
        customerDto.setAge(age);
        customerDto.setNicNo(nicNo);
        customerDto.setAddress(address);
        customerDto.setUpdateButton(getButton("Update", "#3486eb", customerId, customerDto));
        customerDto.setDeleteButton(getButton("Delete", " #eb3434", customerId, customerDto));
        customerDto.setLastUpdatedDate(lastUpdatedDate);
        return customerDto;
    }

    /**
     * @param text : java.lang.String
     * @param background : java.lang.String
     * @param customerId : java.lang.Integer
     * @param customerDto : lk.edu.cis.sliit.beds.dto.CustomerDto
     * @return javafx.scene.control.Button
     * 
     * creates a javafx.scene.control.Button according to the provided data
     */
    private Button getButton(String text, String background, Integer customerId, CustomerDto customerDto) {
    	Button button = new Button(text);
        button.setId(customerId.toString());
        button.setStyle("-fx-background-color: " + background + ";");
    	if("Delete".equals(text)) {
    		button.setOnAction(e -> {
    			try {
    				ManageCustomerService customerService = new ManageCustomerServiceImpl();
    				boolean isDeleted = customerService.deleteCustomer(customerId);
    				if(isDeleted) {
    					Alert alert = new Alert(AlertType.INFORMATION);
    					alert.setContentText("Customer Deleted Successfully!");
    					alert.show();
    				} else {
    					Alert alert = new Alert(AlertType.ERROR);
    					alert.setContentText("OOPS.. Something Went Wrong!");
    					alert.show();
    				}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
    		});
    	} else if("Update".equals(text)) {
    		button.setOnAction(e -> {
    			String style = "-fx-font-family: Cambria; -fx-font-style: Italic; -fx-font-size: 15.0;";
    		
    			Stage stage = new Stage();
    			
    			GridPane updateForm =  new GridPane();
    			
    			TextField txtFirstName = new TextField(customerDto.getFirstName());
    			txtFirstName.setId("txtUpdateFirstName");
    			txtFirstName.setStyle(style);
    			txtFirstName.setPromptText("First Name");
    			
    			TextField txtLastName = new TextField(customerDto.getLastName());
    			txtLastName.setId("txtUpdateLastName");
    			txtLastName.setStyle(style);
    			txtLastName.setPromptText("Last Name");
    			
    			
    			TextField txtMobileNo = new TextField(customerDto.getMobileNo().toString());
    			txtMobileNo.setId("txtUpdateMobileNo");
    			txtMobileNo.setStyle(style);
    			txtMobileNo.setPromptText("Mobile Number");
    			
    			TextField txtSalary = new TextField(customerDto.getSalary().toString());
    			txtSalary.setId("txtUpdateSalary");
    			txtSalary.setStyle(style);
    			txtSalary.setPromptText("Salary");
    			
    			
    			ComboBox<String> cmbGender = new ComboBox<String>();
    			cmbGender.getItems().addAll("Male", "Female", "Other");
    			cmbGender.getSelectionModel().select(customerDto.getGender());
    			cmbGender.setId("txtUpdateGender");
    			cmbGender.setStyle(style);
    			
    			
    			TextField txtAge = new TextField(customerDto.getAge().toString());
    			txtAge.setId("txtUpdateAge");
    			txtAge.setStyle(style);
    			txtAge.setPromptText("Age");
    			
    			
    			TextField txtNic = new TextField(customerDto.getNicNo());
    			txtNic.setId("txtUpdateNic");
    			txtNic.setStyle(style);
    			txtNic.setPromptText("NIC");
    			
    			TextArea txtAddress = new TextArea(customerDto.getAddress());
    			txtAddress.setId("txtUpdateAddress");
    			txtAddress.setStyle(style);
    			txtAddress.setPromptText("Address");
    			
    			
    			Button btnUpdate = new Button("Update Customer");
    			btnUpdate.setStyle(style.concat("-fx-background-color: #fcfafa;"));
    			btnUpdate.setOnAction(ev -> {
    				try {
        				ManageCustomerService customerService = new ManageCustomerServiceImpl();
        				CustomerDto customer = new CustomerDto(txtFirstName.getText(), txtLastName.getText(), 
        						Integer.parseInt(txtMobileNo.getText()), Double.parseDouble(txtSalary.getText()), 
        						cmbGender.getSelectionModel().getSelectedItem(), Integer.parseInt(txtAge.getText()), 
        						txtNic.getText(), txtAddress.getText());
        				boolean isUpdated = customerService.updateCustomer(customerId, customer);
        				if(isUpdated) {
        					Alert alert = new Alert(AlertType.INFORMATION);
        					alert.setContentText("Customer Updated Successfully!");
        					alert.show();
        				} else {
        					Alert alert = new Alert(AlertType.ERROR);
        					alert.setContentText("OOPS.. Something Went Wrong!");
        					alert.show();
        				}
    				} catch (Exception ex) {
    					ex.printStackTrace();
    				}
    			});
    			
    			updateForm.add(txtFirstName, 0, 0);
    			updateForm.add(txtLastName, 0, 1);
    			updateForm.add(txtMobileNo, 0, 2);
    			updateForm.add(txtSalary, 0, 3);
    			updateForm.add(cmbGender, 0, 4);
    			updateForm.add(txtAge, 0, 5);
    			updateForm.add(txtNic, 0, 6);
    			updateForm.add(txtAddress, 0, 7);
    			updateForm.add(btnUpdate, 0, 8);
    			
    			stage.initModality(Modality.APPLICATION_MODAL);
    			stage.setTitle("Update Customer Form");
    			stage.setScene(new Scene(updateForm));
    			stage.showAndWait();
    		});
    	}
    	return button;
    }
}
