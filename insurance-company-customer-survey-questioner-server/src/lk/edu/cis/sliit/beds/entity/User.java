package lk.edu.cis.sliit.beds.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines User Entity to interact with the Database
 * This Entity is Currently using in this Application For :
 *  1) Save User Details such as user name and password
 *  2) Update User Details such as user name and password
 * 
 * Includes all the attributes which contains in the database table
 * 
 * @author Punsara Prathibha - 2010866
 */
public class User implements Serializable {
	/**
	 * serialVersionUID of User
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for user id
	 */
	private Integer userId;
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for user role id
	 */
    private Integer userRoleId;
    /**
	 * java.lang.String
	 * Creates a private Object reference for user's user name
	 */
    private String userName;
    /**
	 * java.lang.String
	 * Creates a private Object reference for user's password
	 */
    private String userPassword;
    /**
	 * java.sql.Timestamp
	 * Creates a private Object reference for last updated date time for user data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public User() {
    }

    /**
     * @param userId : java.lang.Integer
     */
    public User(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId : java.lang.Integer
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getUserRoleFlag() {
        return userRoleId;
    }

    /**
     * @param userRoleFlag : java.lang.Integer
     */
    public void setUserRoleFlag(Integer userRoleFlag) {
        this.userRoleId = userRoleFlag;
    }

    /**
     * @return java.lang.String
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName : java.lang.String
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return java.lang.String
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword : java.lang.String
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
