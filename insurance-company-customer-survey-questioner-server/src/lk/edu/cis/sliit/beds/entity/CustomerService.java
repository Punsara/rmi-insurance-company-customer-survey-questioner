package lk.edu.cis.sliit.beds.entity;

import lk.edu.cis.sliit.beds.dto.CustomerServiceDto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines CustomerService Entity to interact with the Database
 * This Entity is Currently using in this Application For :
 *  1) Search CustomerService
 * 
 * Includes all the attributes which contains in the database table
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerService implements Serializable {
	/**
	 * serialVersionUID of CustomerService
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for service id
	 */
	private Integer serviceId;
	/**
	 * java.lang.String
	 * Creates a private Object reference for service name
	 */
    private String serviceName;
    /**
	 * java.sql.Timestamp
	 * Creates a private Object reference for last updated date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public CustomerService() {
    }

    /**
     * @param serviceId : java.lang.Integer
     * @param serviceName : java.lang.String
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public CustomerService(Integer serviceId, String serviceName, Timestamp lastUpdatedDate) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId : java.lang.Integer
     */
    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return java.lang.String
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName : java.lang.String
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return lk.edu.cis.sliit.beds.dto.CustomerServiceDto
     */
    public CustomerServiceDto toDto() {
        CustomerServiceDto customerServiceDto = new CustomerServiceDto();
        customerServiceDto.setServiceId(serviceId);
        customerServiceDto.setServiceName(serviceName);
        customerServiceDto.setLastUpdatedDate(lastUpdatedDate);
        return customerServiceDto;
    }
}