package lk.edu.cis.sliit.beds.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines QuestionAnswerMapping Entity to interact with the Database
 * This Entity is Currently using in this Application For :
 *  1) Search CustomerService
 * 
 * Includes all the attributes which contains in the database table
 * 
 * @author Punsara Prathibha - 2010866
 */
public class QuestionAnswerMapping implements Serializable {
	/**
	 * serialVersionUID of QuestionAnswerMapping
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for question answer mapping id
	 */
	private Integer questionAnswerId;
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for question id
	 */
    private Integer questionId;
    /**
	 * java.lang.Integer
	 * Creates a private Object reference for answer id
	 */
    private Integer answerId;
    /**
	 * java.sql.Timestamp
	 * Creates a private Object reference for last updated date time of these data
	 */
    private Timestamp lastUpdatedDate;

    /**
     * Default Constructor
     */
    public QuestionAnswerMapping() {
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getQuestionAnswerId() {
        return questionAnswerId;
    }

    /**
     * @param questionAnswerId : java.lang.Integer
     */
    public void setQuestionAnswerId(Integer questionAnswerId) {
        this.questionAnswerId = questionAnswerId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId : java.lang.Integer
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getAnswerId() {
        return answerId;
    }

    /**
     * @param answerId : java.lang.Integer
     */
    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate : java.sql.Timestamp
     */
    public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
