package lk.edu.cis.sliit.beds.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Defines CustomerFeedback Entity to interact with the Database
 * This Entity is Currently using in this Application For :
 *  1) Save CustomerFeedback
 *  2) Search CustomerFeedback
 * 
 * Includes all the attributes which contains in the database table
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerFeedback implements Serializable {
	/**
	 * serialVersionUID of CustomerFeedback
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for customer feedback id
	 */
	private Integer customerFeedbackId;
	/**
	 * java.lang.Integer
	 * Creates a private Object reference for customer id
	 */
    private Integer customerId;
    /**
	 * java.lang.Integer
	 * Creates a private Object reference for service id
	 */
    private Integer serviceId;
    /**
	 * java.lang.Integer
	 * Creates a private Object reference for question id
	 */
    private Integer questionId;
    /**
	 * java.lang.String
	 * Creates a private Object reference for customer feedback
	 */
    private String customerFeedback;
    /**
	 * java.sql.Timestamp
	 * Creates a private Object reference for customer feedback last updated 
	 * date time of these data
	 */
    private Timestamp feedbackDateTime;

    /**
     * Default Constructor
     */
    public CustomerFeedback() {
    }

    /**
     * @param customerFeedbackId : java.lang.Integer
     * @param customerId : java.lang.Integer
     * @param serviceId : java.lang.Integer
     * @param questionId : java.lang.Integer
     * @param customerFeedback : java.lang.String
     * @param feedbackDateTime : java.sql.Timestamp
     */
    public CustomerFeedback(Integer customerFeedbackId, Integer customerId, Integer serviceId,
                            Integer questionId, String customerFeedback, Timestamp feedbackDateTime) {
        this.customerFeedbackId = customerFeedbackId;
        this.customerId = customerId;
        this.serviceId = serviceId;
        this.questionId = questionId;
        this.customerFeedback = customerFeedback;
        this.feedbackDateTime = feedbackDateTime;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerFeedbackId() {
        return customerFeedbackId;
    }

    /**
     * @param customerFeedbackId : java.lang.Integer
     */
    public void setCustomerFeedbackId(Integer customerFeedbackId) {
        this.customerFeedbackId = customerFeedbackId;
    }

    /**
     * @return java.lang.Integer
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId : java.lang.Integer
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * @return : java.lang.Integer
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId : java.lang.Integer
     */
    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return : java.lang.Integer
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId : java.lang.Integer
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    /**
     * @return java.lang.String
     */
    public String getCustomerFeedback() {
        return customerFeedback;
    }

    /** 
     * @param customerFeedback : java.lang.String
     */
    public void setCustomerFeedback(String customerFeedback) {
        this.customerFeedback = customerFeedback;
    }

    /**
     * @return java.sql.Timestamp
     */
    public Timestamp getFeedbackDateTime() {
        return feedbackDateTime;
    }

    /**
     * @param feedbackDateTime : java.sql.Timestamp
     */
    public void setFeedbackDateTime(Timestamp feedbackDateTime) {
        this.feedbackDateTime = feedbackDateTime;
    }
}
