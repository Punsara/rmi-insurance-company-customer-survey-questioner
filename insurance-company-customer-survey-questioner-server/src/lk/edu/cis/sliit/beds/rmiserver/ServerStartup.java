package lk.edu.cis.sliit.beds.rmiserver;

import lk.edu.cis.sliit.beds.service.impl.ServiceFactoryImpl;
import lk.edu.cis.sliit.beds.service.ServiceFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Server StartUp for the Customer Survey Questioner Application
 * This class starts the Server Side Application and binds and Register the 
 * lk.edu.cis.sliit.beds.service.ServiceFactory as the Remote Object in the RMI Registry
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ServerStartup {

	/**
	 * Creates a private static reference variable as a constant for declaring the RMI
	 * default registry port
	 */
    private static final int REGISTRY_PORT = 1099;
    /**
	 * Creates a private static reference variable as a constant for declaring the RMI
	 * registry name which binds with the remote object
	 */
    private static final String REGISTRY_NAME = "InsuranceCustomerSurveyRegistry";

    /**
     * @param args : java.lang.String[]
     * 
     * Main method for the Server Application which starts the Server Side Application
     * for the Customer Survey Questioner Application by binding/registering the 
     * remote Object in the RMI Registry
     */
    public static void main(String[] args) {
        System.out.println("Attempting to start the RMI-Server...");
        try {
            ServiceFactory serviceFactory = ServiceFactoryImpl.getInstance();
            Registry registry = LocateRegistry.createRegistry(REGISTRY_PORT);
            registry.rebind(REGISTRY_NAME, serviceFactory);
            System.out.println("Server Started Successfully... Welcome to RMI InsuranceCustomerSurveyQuestioneer");
        } catch (RemoteException e) {
            System.out.println("oops! already another application is running on this port(" + REGISTRY_PORT + ") : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
