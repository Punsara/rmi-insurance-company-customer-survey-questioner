package lk.edu.cis.sliit.beds.repository.custom;

import lk.edu.cis.sliit.beds.dto.FeedbackDto;
import lk.edu.cis.sliit.beds.dto.QualityOfServiceDto;
import lk.edu.cis.sliit.beds.dto.RecomendationFeedbackDto;
import lk.edu.cis.sliit.beds.entity.CustomerFeedback;
import lk.edu.cis.sliit.beds.repository.SuperRepository;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular CustomerFeedback Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface CustomerFeedbackRepository extends SuperRepository<CustomerFeedback, Integer> {

	/**
	 * @return lk.edu.cis.sliit.beds.dto.RecomendationFeedbackDto
	 * @throws Exception
	 * 
	 * returns the customer recommendations and customer feedbacks with the relevant months
	 * by limiting back to 10 previous months from the database. 
	 */
	RecomendationFeedbackDto findRecomondatinAndFeedbackAgainstMonth() throws Exception;
	
	/**
	 * @return lk.edu.cis.sliit.beds.dto.QualityOfServiceDto
	 * @throws Exception
	 * 
	 * returns the customer recommendations and customer feedbacks with the quality of the service
	 * by limiting back to 10 previous months from the database. 
	 */
	QualityOfServiceDto findQualityOfServiceAgainstMonth() throws Exception;
	
	/**
	 * @return java.lang.String
	 * @throws Exception
	 * 
	 * returns all customer negative feedback count from the database
	 */
	String findNegetiveAttitudeCustomersByFeedback() throws Exception;
	
	/**
	 * @return java.lang.String
	 * @throws Exception
	 * 
	 * returns all customer neutral feedback count from the database
	 */
	String findNeutralAttitudeCustomersByFeedback() throws Exception;
	
	/**
	 * @return java.lang.String
	 * @throws Exception
	 * 
	 * returns all customer positive feedback count from the database
	 */
	String findPositiveAttitudeCustomersByFeedback() throws Exception;
	
	/**
	 * @return lk.edu.cis.sliit.beds.dto.FeedbackDto
	 * @throws Exception
	 * 
	 * returns all the negative feedbacks, neutral feedbacks, positive feedbacks 
	 * with the total no of feedback for current day
	 */
	FeedbackDto findTodayCustomerFeedback() throws Exception;
}
