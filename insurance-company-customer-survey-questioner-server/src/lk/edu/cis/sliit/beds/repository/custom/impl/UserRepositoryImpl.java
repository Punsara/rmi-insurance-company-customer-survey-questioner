package lk.edu.cis.sliit.beds.repository.custom.impl;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular User Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
import lk.edu.cis.sliit.beds.dto.LoginDto;
import lk.edu.cis.sliit.beds.entity.User;
import lk.edu.cis.sliit.beds.repository.custom.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
	
	/**
	 * java.sql.Connection
	 * Creates a private Object reference of SQL Connection
	 */
    private Connection connection;

    /**
     * sets the database connection before a executing a query in the database 
     */
    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * returns a boolean value based on the status of user details updating process
     * in the database
     */
    @Override
    public boolean update(User user, Integer userId) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE User SET " +
                "user_role_id = ?, user_name = AES_ENCRYPT(?, ?), user_password = AES_ENCRYPT(?, ?), last_updated_date = ? WHERE user_id = ?");

        preparedStatement.setInt(1, user.getUserRoleFlag());
        preparedStatement.setString(2, user.getUserName());
        preparedStatement.setString(3, user.getUserName());
        preparedStatement.setString(4, user.getUserPassword());
        preparedStatement.setString(5, user.getUserPassword());
        preparedStatement.setTimestamp(6, user.getLastUpdatedDate());
        preparedStatement.setInt(7, userId);
        return preparedStatement.executeUpdate() > 0;
    }

    /**
     * returns a boolean value based on the status of whether a particular customer
     * exists in the system by comparing the provided user name and the password
     * of the customer 
     */
    @Override
    public boolean findIsExistsByUserNameAndPassword(LoginDto loginDto) throws Exception {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM User U LEFT JOIN UserRole UR ON U.user_role_id = UR.user_role_id \n" +
                "WHERE AES_DECRYPT(U.user_name, ?) = ? AND AES_DECRYPT(U.user_password, ?) = ? AND UR.user_role = ?");
        statement.setString(1, loginDto.getUserName());
        statement.setString(2, loginDto.getUserName());
        statement.setString(3, loginDto.getPassword());
        statement.setString(4, loginDto.getPassword());
        statement.setString(5, loginDto.getUserRole());
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    /**
     * returns the next incremented user id querying the database 
     */
	@Override
	public Integer findNextIncrementUserId() throws Exception {
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT (user_id + 1) FROM User ORDER BY user_id DESC LIMIT 1");
        ResultSet resultSet = preparedStatement.executeQuery();
        int nextUserId = 1;
        if (resultSet.next()) {
        	nextUserId = resultSet.getInt(1);
        }
        return nextUserId;
	}
	
	/**
     * returns a boolean value based on the status of user details deletion process
     * in the database
     */
    @Override
    public boolean delete(Integer integer) throws Exception {
        return false;
    }
	
	/**
	 * returns all user details currently available in the system
	 */
	@Override
    public List<User> findAll() throws Exception {
        return null;
    }

	/**
	 * returns a boolean value based on the status of user details saving process
	 * in the database
	 */
    @Override
    public boolean save(User user) throws Exception {
        return false;
    }
}
