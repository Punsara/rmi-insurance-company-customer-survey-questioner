package lk.edu.cis.sliit.beds.repository.custom;

import lk.edu.cis.sliit.beds.dto.LoginDto;
import lk.edu.cis.sliit.beds.entity.User;
import lk.edu.cis.sliit.beds.repository.SuperRepository;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular User Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface UserRepository extends SuperRepository<User, Integer> {
	
    /**
     * @param loginDto : lk.edu.cis.sliit.beds.dto.LoginDto
     * @return boolean
     * @throws Exception
     * 
     * returns a boolean values based on whether the provided userName 
     * and the Password is currently exists
     */
    boolean findIsExistsByUserNameAndPassword(LoginDto loginDto) throws Exception;
    
    /**
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the next incremented userId from the database
     */
    Integer findNextIncrementUserId() throws Exception;
}
