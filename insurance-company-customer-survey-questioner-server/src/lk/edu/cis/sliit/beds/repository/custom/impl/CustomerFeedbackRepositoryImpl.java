package lk.edu.cis.sliit.beds.repository.custom.impl;

import lk.edu.cis.sliit.beds.dto.FeedbackDto;
import lk.edu.cis.sliit.beds.dto.QualityOfServiceDto;
import lk.edu.cis.sliit.beds.dto.RecomendationFeedbackDto;
import lk.edu.cis.sliit.beds.entity.CustomerFeedback;
import lk.edu.cis.sliit.beds.repository.custom.CustomerFeedbackRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular CustomerFeedback Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerFeedbackRepositoryImpl implements CustomerFeedbackRepository {

	/**
	 * java.sql.Connection
	 * Creates a private Object reference of SQL Connection
	 */
    private Connection connection;

    /**
     * sets the database connection before a executing a query in the database 
     */
    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * finds all customer feedbacks and provides a List of CustomerFeedback entity types
     */
    @Override
    public List<CustomerFeedback> findAll() throws Exception {
        return null;
    }

    /**
     * saves the provided customer feedback in the database and returns a boolean value
     * whether the executeUpdate method of the prepared statement returns an integer value
     * greater than zero 
     */
    @Override
    public boolean save(CustomerFeedback customerFeedback) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO CustomerFeedback(" +
                "    customer_feedback_id,\n" +
                "    customer_id,\n" +
                "    service_id,\n" +
                "    question_id,\n" +
                "    customer_feedback,\n" +
                "    feedback_date_time) VALUES(?,?,?,?,?,?)");
        preparedStatement.setInt(1, customerFeedback.getCustomerFeedbackId());
        preparedStatement.setInt(2, customerFeedback.getCustomerId());
        preparedStatement.setInt(3, customerFeedback.getServiceId());
        preparedStatement.setInt(4, customerFeedback.getQuestionId());
        preparedStatement.setString(5, customerFeedback.getCustomerFeedback());
        preparedStatement.setTimestamp(6, customerFeedback.getFeedbackDateTime());
        return preparedStatement.executeUpdate() > 0;
    }
    
	/**
	 * returns ten previous months list mapped with the progress of recommendations and feedbacks
	 * by querying from the database
	 */
	@Override
	public RecomendationFeedbackDto findRecomondatinAndFeedbackAgainstMonth() throws Exception {
		RecomendationFeedbackDto recomendationFeedbackDto = new RecomendationFeedbackDto();
		
		ResultSet monthsResultSet = getMonthListFromDB();
		while(monthsResultSet.next()) {
			recomendationFeedbackDto.getMonth().add(monthsResultSet.getString(1));
		}
		
		PreparedStatement recomendationPrepareStatement = connection.prepareStatement("SELECT ((COUNT(customer_feedback) / 10) * 100) AS recomendation\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE Q.question_text like '%recommend our service to the others%' AND CF.customer_feedback = 'Yes'\r\n"
				+ "GROUP BY CF.customer_id, MONTH(feedback_date_time)\r\n"
				+ "LIMIT 10");
		ResultSet recomendationResultSet = recomendationPrepareStatement.executeQuery();
		while(recomendationResultSet.next()) {
			recomendationFeedbackDto.getRecomendation().add(recomendationResultSet.getString(1));
		}
		
		
		PreparedStatement feedbackPrepareStatement = connection.prepareStatement("SELECT ((COUNT(customer_feedback) /10) * 100) AS feedback\r\n"
				+ "       FROM CustomerFeedback\r\n"
				+ "       WHERE customer_feedback = 'Yes'\r\n"
				+ "       GROUP BY MONTH(feedback_date_time)\r\n"
				+ "       LIMIT 10");
		ResultSet feedbackResultSet = feedbackPrepareStatement.executeQuery();
		while(feedbackResultSet.next()) {
			recomendationFeedbackDto.getFeedback().add(feedbackResultSet.getString(1));
		}
		return recomendationFeedbackDto;
	}

	/**
	 * returns ten previous months list mapped with the progress of new customers
	 * who interact with the system for each given month by querying from the database
	 */
	@Override
	public QualityOfServiceDto findQualityOfServiceAgainstMonth() throws Exception {
		QualityOfServiceDto customerAttitudeDto = new QualityOfServiceDto();
		
		PreparedStatement monthsPrepareStatement = connection.prepareStatement("SELECT "
				+ "MONTHNAME(last_updated_date) AS month\r\n"
				+ "FROM Customer GROUP BY MONTH(last_updated_date) LIMIT 10");
		ResultSet monthsResultSet =  monthsPrepareStatement.executeQuery();
		while(monthsResultSet.next()) {
			customerAttitudeDto.getMonth().add(monthsResultSet.getString(1));
		}
		
		PreparedStatement attitudePrepareStatement = connection.prepareStatement("SELECT "
				+ "ROUND(COUNT(customer_id) / 10 * 100) AS recomendation\r\n"
				+ "FROM Customer GROUP BY MONTH(last_updated_date) LIMIT 10");
		ResultSet attitudeResultSet = attitudePrepareStatement.executeQuery();
		while(attitudeResultSet.next()) {
			customerAttitudeDto.getQualityOfService().add(attitudeResultSet.getString(1));
		}
		return customerAttitudeDto;
	}
	
	
	/**
	 * @return java.sql.ResultSet
	 * @throws Exception
	 * returns 10 previous Months from the current month by querying the customer feedback table
	 */
	private ResultSet getMonthListFromDB() throws Exception {
		PreparedStatement monthsPrepareStatement = connection.prepareStatement("SELECT MONTHNAME(feedback_date_time) AS month\r\n"
				+ "       FROM CustomerFeedback\r\n"
				+ "       GROUP BY MONTH(feedback_date_time)\r\n"
				+ "       LIMIT 10");
		return monthsPrepareStatement.executeQuery();
	}

	/**
	 * returns all customers with negative attitudes according to their provided feedbacks
	 * by querying the database for each customer
	 */
	@Override
	public String findNegetiveAttitudeCustomersByFeedback() throws Exception {
		PreparedStatement attitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10) * 100) AS negetiveFeedback\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE Q.question_text like '%scale of one to nine, how likely would you recommend our service%'\r\n"
				+ "  AND customer_feedback < 5");
		ResultSet attitudeResultSet = attitudePrepareStatement.executeQuery();
		String noOfNegetiveAttitudes = null;
		if(attitudeResultSet.next()) {
			noOfNegetiveAttitudes = attitudeResultSet.getString(1);
		}
		return noOfNegetiveAttitudes;
	}
	
	/**
	 * returns all customers with neutral attitudes according to their provided feedbacks 
	 * by querying the database for each customer
	 */
	@Override
	public String findNeutralAttitudeCustomersByFeedback() throws Exception {
		PreparedStatement attitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10 ) * 100) AS neutralFeedback\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE Q.question_text like '%scale of one to nine, how likely would you recommend our service%'\r\n"
				+ "  AND customer_feedback = 5");
		ResultSet attitudeResultSet = attitudePrepareStatement.executeQuery();
		String noOfNeutralAttitudes = null;
		if(attitudeResultSet.next()) {
			noOfNeutralAttitudes = attitudeResultSet.getString(1);
		}
		return noOfNeutralAttitudes;
	}
	
	/**
	 * returns all customers with positive attitudes according to their provided feedbacks
	 * by querying the database for each customer
	 */
	@Override
	public String findPositiveAttitudeCustomersByFeedback() throws Exception {
		PreparedStatement attitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10) * 100) AS positiveFeedback\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE Q.question_text like '%scale of one to nine, how likely would you recommend our service%'\r\n"
				+ "AND customer_feedback > 5");
		ResultSet attitudeResultSet = attitudePrepareStatement.executeQuery();
		String noOfPositiveAttitudes = null;
		if(attitudeResultSet.next()) {
			noOfPositiveAttitudes = attitudeResultSet.getString(1);
		}
		return noOfPositiveAttitudes;
	}

	/**
	 * returns the today total number of customer surveys launched and the categorization of 
	 * those feedbacks submitted in the current day by querying the database 
	 */
	@Override
	public FeedbackDto findTodayCustomerFeedback() throws Exception {
		FeedbackDto feedbackDto = new FeedbackDto();
		
		PreparedStatement totalFeedbackPrepareStatement = connection.prepareStatement("SELECT ROUND(COUNT(*) / 10)\r\n"
				+ "FROM CustomerFeedback CF WHERE DATE(feedback_date_time) = curdate()");
		ResultSet totalFeedbackResultSet = totalFeedbackPrepareStatement.executeQuery();
		if(totalFeedbackResultSet.next()) {
			feedbackDto.setTotalNoOfFeedbacks(totalFeedbackResultSet.getInt(1));
		}
		
		PreparedStatement negetiveAttitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10 ) * 100 ) AS negetiveFeedbackPercentage\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE ((Q.question_text LIKE '%scale of one to nine, how likely would you recommend our service%' AND TRIM(customer_feedback) < 5)\r\n"
				+ "    OR (TRIM(customer_feedback) IN ('No', 'Worse')))\r\n"
				+ "  AND DATE(feedback_date_time) = curdate()");
		ResultSet negetiveAttitudeResultSet = negetiveAttitudePrepareStatement.executeQuery();
		if(negetiveAttitudeResultSet.next()) {
			feedbackDto.setNoOfNegetiveFeedbacks(negetiveAttitudeResultSet.getInt(1));
		}
		
		PreparedStatement neutralAttitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10 ) * 100 ) AS neutralFeedbackPercentage\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE ((Q.question_text LIKE '%scale of one to nine, how likely would you recommend our service%' AND TRIM(customer_feedback) = 5)\r\n"
				+ "    OR (TRIM(customer_feedback) IN ('Medium')))\r\n"
				+ "  AND DATE(feedback_date_time) = curdate()");
		ResultSet neutralAttitudeResultSet = neutralAttitudePrepareStatement.executeQuery();
		if(neutralAttitudeResultSet.next()) {
			feedbackDto.setNoOfNeutralFeedbacks(neutralAttitudeResultSet.getInt(1));
		}
		
		PreparedStatement positiveAttitudePrepareStatement = connection.prepareStatement("SELECT ((COUNT(*) / 10 ) * 100 ) AS positiveFeedbackPercentage\r\n"
				+ "FROM CustomerFeedback CF\r\n"
				+ "         INNER JOIN Question Q ON CF.question_id = Q.question_id\r\n"
				+ "WHERE ((Q.question_text LIKE '%scale of one to nine, how likely would you recommend our service%' AND TRIM(customer_feedback) > 5)\r\n"
				+ "    OR (TRIM(customer_feedback) IN ('Yes', 'Excellent', 'Good')))\r\n"
				+ "  AND DATE(feedback_date_time) = curdate()");
		ResultSet positiveAttitudeResultSet = positiveAttitudePrepareStatement.executeQuery();
		if(positiveAttitudeResultSet.next()) {
			feedbackDto.setNoOfPositiveFeedbacks(positiveAttitudeResultSet.getInt(1));
		}
		
		return feedbackDto;
	}
	
	/**
     * returns a boolean value based on whether the provided customer feedback is updated
     */
    @Override
    public boolean update(CustomerFeedback customerFeedback, Integer integer) throws Exception {
        return false;
    }

    /**
     * returns a boolean value based on whether the provided customer feedback is deleted
     */
    @Override
    public boolean delete(Integer integer) throws Exception {
        return false;
    }
}
