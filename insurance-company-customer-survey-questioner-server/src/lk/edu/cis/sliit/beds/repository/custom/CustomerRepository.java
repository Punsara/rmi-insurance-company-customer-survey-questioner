package lk.edu.cis.sliit.beds.repository.custom;

import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.entity.Customer;
import lk.edu.cis.sliit.beds.repository.SuperRepository;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular Customer Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface CustomerRepository extends SuperRepository<Customer, Integer> {

    /**
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns next incremented customer id from the database
     */
    Integer findNextIncrementCustomerId() throws Exception;

    /**
     * @param nicNo : java.lang.String
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the related userId based on the provided NIC number of the customer
     */
    Integer findUserIdByNicNo(String nicNo) throws Exception;

    /**
     * @param userName : java.lang.String
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the related customerId based on the provided user name of the customer
     */
    Integer findCustomerIdByUserName(String userName) throws Exception;
    
    /**
     * @param customerDto : lk.edu.cis.sliit.beds.dto.CustomerDto
     * @param customerId : java.lang.Integer
     * @return boolean
     * @throws Exception
     * 
     * returns a boolean value based on the status of the customer update process in the database
     */
    boolean updateCustomerById(CustomerDto customerDto, Integer customerId) throws Exception;
}
