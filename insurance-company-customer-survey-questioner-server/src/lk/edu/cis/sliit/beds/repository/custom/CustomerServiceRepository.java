package lk.edu.cis.sliit.beds.repository.custom;

import lk.edu.cis.sliit.beds.entity.CustomerService;
import lk.edu.cis.sliit.beds.repository.SuperRepository;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular CustomerService Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface CustomerServiceRepository extends SuperRepository<CustomerService, Integer> {
	
}
