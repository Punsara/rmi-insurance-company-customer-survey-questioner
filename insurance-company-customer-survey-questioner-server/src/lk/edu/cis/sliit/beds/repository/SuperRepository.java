package lk.edu.cis.sliit.beds.repository;

import java.sql.Connection;
import java.util.List;

/**
 * Defines a SuperRepository to Maintain the Class Hierarchy and with the intention of 
 * Defining and Inherit all the Create, Read, Update, Delete (CRUD) operations from one place 
 * as they are common to each Entity Type. 
 * 
 * These features will implicitly inherit to all Classes of which implements from this Interface
 * 
 * @author Punsara Prathibha - 2010866
 *
 * @param <T> : Any Entity Type
 * @param <ID> : ID type of the given entity
 */
public interface SuperRepository<T, ID> {

    /**
     * @param connection : java.sql.Connection
     */
    void setConnection(Connection connection);

    /**
     * @return java.util.List<T>
     * @throws Exception
     * returns all the results as a list of given type of entities
     */
    List<T> findAll() throws Exception;

    /**
     * @param t : T
     * @return boolean
     * @throws Exception
     * returns boolean value based on the status of the database save operation
     */
    boolean save(T t) throws Exception;

    /**
     * @param t : T
     * @param id : ID
     * @return boolean
     * @throws Exception
     * returns boolean value based on the status of the database update operation by id
     */
    boolean update(T t, ID id) throws Exception;

    /**
     * @param id : ID
     * @return boolean
     * @throws Exception
     * returns boolean value based on the status of the database delete operation by id
     */
    boolean delete(ID id) throws Exception;
}
