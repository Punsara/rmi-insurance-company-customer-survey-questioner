package lk.edu.cis.sliit.beds.repository.custom.impl;

import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.entity.Customer;
import lk.edu.cis.sliit.beds.repository.custom.CustomerRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static lk.edu.cis.sliit.beds.util.Utility.CUSTOMER_USER_ROLE_FLAG;
import static lk.edu.cis.sliit.beds.util.Utility.EMPTY_STRING;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular Customer Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerRepositoryImpl implements CustomerRepository {
	
	/**
	 * java.sql.Connection
	 * Creates a private Object reference of SQL Connection
	 */
    private Connection connection;

    /**
     * sets the database connection before a executing a query in the database 
     */
    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * returns all active customers currently available in the business
     * by querying the database
     */
    @Override
    public List<Customer> findAll() throws Exception {
        List<Customer> customerList = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Customer");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            customerList.add(new Customer(resultSet.getInt("customer_id"),
                    resultSet.getInt("user_id"), resultSet.getString("first_name"),
                    resultSet.getString("last_name"), resultSet.getInt("customer_mobile_no"),
                    resultSet.getDouble("customer_salary"), resultSet.getString("customer_gender"),
                    resultSet.getInt("customer_age"), resultSet.getString("nic_no"),
                    resultSet.getString("customer_address"), resultSet.getTimestamp("last_updated_date")));
        }
        return customerList;
    }

    /**
     * returns a boolean value based on process of saving a particular new customer
     * in the database
     */
    @Override
    public boolean save(Customer customer) throws Exception {
        boolean isSuccessfullySaved = false;
        String defaultValue = EMPTY_STRING;
        PreparedStatement userPreStatement = connection.prepareStatement("INSERT INTO User(" +
                        "                    user_id,\n" +
                        "                    user_role_id,\n" +
                        "                    user_name,\n" +
                        "                    user_password,\n" +
                        "                    last_updated_date) VALUES(?, ?, AES_ENCRYPT(?, ?), AES_ENCRYPT(?, ?), ?)");
        userPreStatement.setInt(1, customer.getUserId());
        userPreStatement.setInt(2, CUSTOMER_USER_ROLE_FLAG);
        userPreStatement.setString(3, defaultValue);
        userPreStatement.setString(4, defaultValue);
        userPreStatement.setString(5, defaultValue);
        userPreStatement.setString(6, defaultValue);
        userPreStatement.setTimestamp(7, Timestamp.valueOf(LocalDateTime.now()));
        isSuccessfullySaved = userPreStatement.executeUpdate() > 0;
        
        
        PreparedStatement customerPreStatement = connection.prepareStatement("INSERT INTO Customer(" +
                    "customer_id, user_id, first_name, last_name, customer_address, " +
                    "customer_age, customer_mobile_no, customer_salary, " +
                    "customer_gender, nic_no, last_updated_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        customerPreStatement.setInt(1, customer.getCustomerId());
        customerPreStatement.setInt(2, customer.getUserId());
        customerPreStatement.setString(3, customer.getFirstName());
        customerPreStatement.setString(4, customer.getLastName());
        customerPreStatement.setString(5, customer.getAddress());
        customerPreStatement.setInt(6, customer.getAge());
        customerPreStatement.setInt(7, customer.getMobileNo());
        customerPreStatement.setDouble(8, customer.getSalary());
        customerPreStatement.setString(9, customer.getGender());
        customerPreStatement.setString(10, customer.getNicNo());
        customerPreStatement.setTimestamp(11, Timestamp.valueOf(LocalDateTime.now()));
        isSuccessfullySaved = customerPreStatement.executeUpdate() > 0;
        return isSuccessfullySaved;
    }

    /**
     * returns a boolean value based on process of updating a particular customer
     * in the database
     */
    @Override
    public boolean update(Customer customer, Integer integer) throws Exception {
        return false;
    }

    /**
     * returns a boolean value based on process of deleting a particular customer
     * in the database
     */
    @Override
    public boolean delete(Integer customerId) throws Exception {
    	boolean isDeleted = false;
    	PreparedStatement preparedStatement = connection.prepareStatement("SELECT U.user_id FROM User U INNER JOIN Customer C on U.user_id = C.user_id WHERE C.customer_id = ?");
    	preparedStatement.setInt(1, customerId);
    	ResultSet resultSet = preparedStatement.executeQuery();
    	if(resultSet.next()) {
    		PreparedStatement userPreparedStatement = connection.prepareStatement("DELETE FROM User WHERE user_id = ?");
    		userPreparedStatement.setInt(1, resultSet.getInt(1));
    		isDeleted = userPreparedStatement.executeUpdate() > 0;
    	}
    	return isDeleted;
    }

    
    /**
     * returns the next incremented customer id by querying the database
     */
    @Override
    public Integer findNextIncrementCustomerId() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT (customer_id + 1) FROM Customer ORDER BY customer_id DESC LIMIT 1");
        ResultSet resultSet = preparedStatement.executeQuery();
        int nextCustomerId = 1;
        if (resultSet.next()) {
            nextCustomerId = resultSet.getInt(1);
        }
        return nextCustomerId;
    }

    /**
     * find and returns user id for a given NIC number of a particular customer by querying the database
     */
    @Override
    public Integer findUserIdByNicNo(String nicNo) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT user_id FROM Customer WHERE nic_no = ?");
        preparedStatement.setString(1, nicNo);
        ResultSet resultSet = preparedStatement.executeQuery();
        int userId = 0;
        if (resultSet.next()) {
            userId = resultSet.getInt(1);
        }
        return userId;
    }

    /**
     * find and returns user id for a given user name of a particular customer by querying the database
     */
    @Override
    public Integer findCustomerIdByUserName(String userName) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT customer_id FROM Customer C " +
                "LEFT JOIN User U ON U.user_id = C.user_id WHERE AES_DECRYPT(U.user_name, ?) = ?");
        preparedStatement.setString(1, userName);
        preparedStatement.setString(2, userName);
        ResultSet resultSet = preparedStatement.executeQuery();
        int customerId = 0;
        if (resultSet.next()) {
            customerId = resultSet.getInt(1);
        }
        return customerId;
    }

    /**
     * returns a boolean value based on process of updating a particular customer
     * in the database
     */
	@Override
	public boolean updateCustomerById(CustomerDto customerDto, Integer customerId) throws Exception {
		PreparedStatement customerPreStatement = connection.prepareStatement("UPDATE Customer\r\n"
				+ "SET first_name         = ?,\r\n"
				+ "    last_name          = ?,\r\n"
				+ "    customer_address   = ?,\r\n"
				+ "    customer_age       = ?,\r\n"
				+ "    customer_mobile_no = ?,\r\n"
				+ "    customer_salary    = ?,\r\n"
				+ "    customer_gender    = ?,\r\n"
				+ "    nic_no             = ?,\r\n"
				+ "    last_updated_date  = CURDATE()\r\n"
				+ "WHERE customer_id = ?");
	    customerPreStatement.setString(1, customerDto.getFirstName());
	    customerPreStatement.setString(2, customerDto.getLastName());
	    customerPreStatement.setString(3, customerDto.getAddress());
	    customerPreStatement.setInt(4, customerDto.getAge());
	    customerPreStatement.setInt(5, customerDto.getMobileNo());
	    customerPreStatement.setDouble(6, customerDto.getSalary());
	    customerPreStatement.setString(7, customerDto.getGender());
	    customerPreStatement.setString(8, customerDto.getNicNo());
	    customerPreStatement.setInt(9, customerId);
	    return customerPreStatement.executeUpdate() > 0;
	}
}
