package lk.edu.cis.sliit.beds.repository.custom.impl;

import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.dto.ValueTextDto;
import lk.edu.cis.sliit.beds.entity.QuestionAnswerMapping;
import lk.edu.cis.sliit.beds.repository.custom.QuestionerRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular Questioner Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public class QuestionerRepositoryImpl implements QuestionerRepository {
	
	/**
	 * java.sql.Connection
	 * Creates a private Object reference of SQL Connection
	 */
    private Connection connection;

    /**
     * sets the database connection before a executing a query in the database 
     */
    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * returns a boolean value based on the status of Question Answer mapping saving
     * process 
     */
    @Override
    public boolean save(QuestionAnswerMapping questionAnswerMapping) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO QuestionAnswerMapping(" +
                "    question_answer_id,\n" +
                "    question_id,\n" +
                "    answer_id,\n" +
                "    last_updated_date) VALUES(?, ?, ?, ?)");
        preparedStatement.setInt(1, questionAnswerMapping.getQuestionAnswerId());
        preparedStatement.setInt(2, questionAnswerMapping.getQuestionId());
        preparedStatement.setInt(3, questionAnswerMapping.getAnswerId());
        preparedStatement.setTimestamp(4, questionAnswerMapping.getLastUpdatedDate());
        return preparedStatement.executeUpdate() > 0;
    }

    /**
     * returns a boolean value based on the status of Question saving process 
     */
    @Override
    public boolean saveQuestion(ValueTextDto valueTextDto) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Question(question_id,\n" +
                "    question_text,\n" +
                "    last_updated_date) VALUES(?, ?, ?)");
        preparedStatement.setInt(1, valueTextDto.getValue());
        preparedStatement.setString(2, valueTextDto.getText());
        preparedStatement.setTimestamp(3, valueTextDto.getLastUpdatedDate());
        return preparedStatement.executeUpdate() > 0;
    }

    /**
     * returns a boolean value based on the status of Answer saving process 
     */
    @Override
    public boolean saveAnswer(ValueTextDto valueTextDto) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Answer(answer_id,\n" +
                "    answer_text,\n" +
                "    last_updated_date) VALUES(?, ?, ?)");
        preparedStatement.setInt(1, valueTextDto.getValue());
        preparedStatement.setString(2, valueTextDto.getText());
        preparedStatement.setTimestamp(3, valueTextDto.getLastUpdatedDate());
        return preparedStatement.executeUpdate() > 0;
    }

    /**
     * returns next incremented answer id querying from the database
     */
    @Override
    public Integer findNextAnswerId() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT (answer_id + 1) FROM Answer ORDER BY answer_id DESC LIMIT 1");
        ResultSet resultSet = preparedStatement.executeQuery();
        int nextAnswerId = 1;
        if (resultSet.next()) {
            nextAnswerId = resultSet.getInt(1);
        }
        return nextAnswerId;
    }

    /**
     * returns next incremented question id querying from the database
     */
    @Override
    public Integer findNextQuestionId() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT (question_id + 1) FROM Question ORDER BY question_id DESC LIMIT 1");
        ResultSet resultSet = preparedStatement.executeQuery();
        int nextQuestionId = 1;
        if (resultSet.next()) {
            nextQuestionId = resultSet.getInt(1);
        }
        return nextQuestionId;
    }

    /**
     * returns next incremented mapping id querying from the database
     */
    @Override
    public Integer findNextMappingId() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT (question_answer_id + 1) FROM Questionanswermapping ORDER BY question_answer_id DESC LIMIT 1");
        ResultSet resultSet = preparedStatement.executeQuery();
        int nextMappingId = 1;
        if (resultSet.next()) {
            nextMappingId = resultSet.getInt(1);
        }
        return nextMappingId;
    }

    /**
     * returns all the questions currently available in the questioner by retrieving from the database
     */
    @Override
    public List<ValueTextDto> findAllQuestions() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Question");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ValueTextDto> valueTextDtoList = new ArrayList<>();
        while (resultSet.next()) {
            valueTextDtoList.add(new ValueTextDto(resultSet.getInt(1), resultSet.getString(2), resultSet.getTimestamp(3)));
        }
        return valueTextDtoList;
    }

    /**
     * returns all the answers currently available in the questioner by retrieving from the database
     */
    @Override
    public List<ValueTextDto> findAllAnswers() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Answer");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ValueTextDto> valueTextDtoList = new ArrayList<>();
        while (resultSet.next()) {
            valueTextDtoList.add(new ValueTextDto(resultSet.getInt(1), resultSet.getString(2), resultSet.getTimestamp(3)));
        }
        return valueTextDtoList;
    }
    
    /**
     * returns all questions which are mapped with a particular set of answers 
     * currently available in the business by retrieving the database
     */
    @Override
    public List<QuestionerDto> findAllQuestionAnswerMapping() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT customer_feedback_id FROM "
        		+ "customerfeedback ORDER BY customer_feedback_id DESC LIMIT 1");
        ResultSet resultSet1 = preparedStatement.executeQuery();
        int customerFeedbackId = 0;
        if (resultSet1.next()) {
            customerFeedbackId = resultSet1.getInt(1);
        }

        PreparedStatement statement = connection.prepareStatement("SELECT Q.question_id, Q.question_text, A.answer_text, QAM.last_updated_date\r\n"
        		+ "FROM QuestionAnswerMapping QAM\r\n"
        		+ "         LEFT JOIN Question Q ON QAM.question_id = Q.question_id\r\n"
        		+ "         LEFT JOIN Answer A ON QAM.answer_id = A.answer_id\r\n"
        		+ "ORDER BY last_updated_date\r\n"
        		+ "LIMIT 10");
        ResultSet resultSet = statement.executeQuery();
        List<QuestionerDto> questionAnswerMapping = new ArrayList<QuestionerDto>();
        int i = 0 ;
        while (resultSet.next()) {
            questionAnswerMapping.add(new QuestionerDto(++i, ++customerFeedbackId ,resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3), resultSet.getTimestamp(4)));
        }
        return questionAnswerMapping;
    }

    /**
     * returns question id for a particular question in the questioner
     */
    @Override
    public Integer findQuestionIdByName(String question) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT question_id FROM Question WHERE question_text = ?");
        preparedStatement.setString(1, question);
        ResultSet resultSet = preparedStatement.executeQuery();
        int questionId = 0;
        while (resultSet.next()) {
            questionId = resultSet.getInt(1);
        }
        return questionId;
    }

    /**
     * returns answer id for a particular answer in the questioner
     */
    @Override
    public Integer findAnswerIdByName(String answer) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT answer_id FROM Answer WHERE answer_text = ?");
        preparedStatement.setString(1, answer);
        ResultSet resultSet = preparedStatement.executeQuery();
        int answerId = 0;
        while (resultSet.next()) {
            answerId = resultSet.getInt(1);
        }
        return answerId;
    }
    
    /**
     * returns all questions which are mapped with a particular set of answers 
     * currently available in the business by retrieving the database
     */
    @Override
    public List<QuestionAnswerMapping> findAll() throws Exception {
        return null;
    }
    
    /**
     * returns a boolean value based on the status of Question Answer Mapping updating process 
     */
    @Override
    public boolean update(QuestionAnswerMapping questionAnswerMapping, Integer integer) throws Exception {
        return false;
    }

    /**
     * returns a boolean value based on the status of Question Answer Mapping deletion process 
     */
    @Override
    public boolean delete(Integer integer) throws Exception {
        return false;
    }
}
