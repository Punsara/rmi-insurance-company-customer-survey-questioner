package lk.edu.cis.sliit.beds.repository.custom.impl;

import lk.edu.cis.sliit.beds.entity.CustomerService;
import lk.edu.cis.sliit.beds.repository.custom.CustomerServiceRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular CustomerService Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerServiceRepositoryImpl implements CustomerServiceRepository {
	
	/**
	 * java.sql.Connection
	 * Creates a private Object reference of SQL Connection
	 */
    private Connection connection;

    /**
     * sets the database connection before a executing a query in the database 
     */
    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * returns all customer services currently available in the business
     * by retrieving the database
     */
    @Override
    public List<CustomerService> findAll() throws Exception {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM CustomerService");
        ResultSet resultSet = statement.executeQuery();
        List<CustomerService> customerServices = new ArrayList<CustomerService>();
        while (resultSet.next()) {
            customerServices.add(new CustomerService(resultSet.getInt(1), resultSet.getString(2), resultSet.getTimestamp(3)));
        }
        return customerServices;
    }
    
    /**
     * returns boolean value based on the saving process of a particular customer service
     */
    @Override
    public boolean save(CustomerService customerService) throws Exception {
        return false;
    }

    /**
     * returns boolean value based on the updating process of a particular customer service
     */
    @Override
    public boolean update(CustomerService customerService, Integer integer) throws Exception {
        return false;
    }

    /**
     * returns boolean value based on the deleting process of a particular customer service
     */
    @Override
    public boolean delete(Integer integer) throws Exception {
        return false;
    }
}
