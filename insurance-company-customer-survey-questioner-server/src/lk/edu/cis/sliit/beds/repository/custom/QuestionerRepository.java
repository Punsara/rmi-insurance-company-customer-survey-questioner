package lk.edu.cis.sliit.beds.repository.custom;

import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.dto.ValueTextDto;
import lk.edu.cis.sliit.beds.entity.QuestionAnswerMapping;
import lk.edu.cis.sliit.beds.repository.SuperRepository;

import java.util.List;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular Questioner Entity Related Database Operation
 * 
 * @author Punsara Prathibha - 2010866
 */
public interface QuestionerRepository extends SuperRepository<QuestionAnswerMapping, Integer> {
	
    /**
     * @param valueTextDto : lk.edu.cis.sliit.beds.dto.ValueTextDto
     * @return boolean
     * @throws Exception
     * 
     * saves a particular question in the database and returns a boolean value based 
     * on the question saving process
     */
    boolean saveQuestion(ValueTextDto valueTextDto) throws Exception;

    /**
     * @param valueTextDto : lk.edu.cis.sliit.beds.dto.ValueTextDto
     * @return boolean
     * @throws Exception
     * 
     * saves a particular answer in the database and returns a boolean value based 
     * on the answer saving process
     */
    boolean saveAnswer(ValueTextDto valueTextDto) throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the next incremented answer id retrieving from the database
     */
    Integer findNextAnswerId() throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the next incremented question id retrieving from the database
     */
    Integer findNextQuestionId() throws Exception;

    /**
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the next incremented question-answer mapping id retrieving from the database
     */
    Integer findNextMappingId() throws Exception;

    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.ValueTextDto>
     * @throws Exception
     * 
     * returns all the questions in the questioner retrieving from the database
     */
    List<ValueTextDto> findAllQuestions() throws Exception;

    
    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.ValueTextDto>
     * @throws Exception
     * 
     * returns all the answers in the questioner retrieving from the database
     */
    List<ValueTextDto> findAllAnswers() throws Exception;
    
    
    /**
     * @return java.util.List<lk.edu.cis.sliit.beds.dto.QuestionerDto>
     * @throws Exception
     * 
     * returns all question-answer mapping records 
     */
    List<QuestionerDto> findAllQuestionAnswerMapping() throws Exception;

    /**
     * @param question : java.lang.String
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the relevant questionId for the given question from the database
     */
    Integer findQuestionIdByName(String question) throws Exception;

    /**
     * @param answerId : java.lang.String
     * @return java.lang.Integer
     * @throws Exception
     * 
     * returns the relevant answerId for the given question from the database
     */
    Integer findAnswerIdByName(String answerId) throws Exception;
}
