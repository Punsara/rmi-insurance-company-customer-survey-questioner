package lk.edu.cis.sliit.beds.repository;

import lk.edu.cis.sliit.beds.repository.custom.impl.*;

/**
 * Provides a Common Interface for Services to Interact and Request a 
 * Particular Repository Implementation Type by Using the Defined enum Repository Types
 * 
 * Used Singleton Design Pattern by declaring a private default constructor of the class to restrict and
 * prevent creating new objects in the heap again and again of the class type from outside of this class. 
 * The public getRepository method returns a particular requested Repository Implementation class by referring to 
 * one time generated object of class type.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class RepositoryFactory {
	
	/**
	 * lk.edu.cis.sliit.beds.repository.RepositoryFactory
	 * Creates a private static Object reference of this class (RepositoryFactory) 
	 * to apply singleton design pattern
	 */
    private static RepositoryFactory repositoryFactory;

    /**
     * provides enum types for each repository type which may refer
     *
     */
    public static enum RepositoryTypes {
        USER, CUSTOMER, QUESTION, QUESTION_ANSWER_MAPPING, ANSWER, CUSTOMER_FEEDBACK, CUSTOMER_SERVICE;
    }

    /**
     * defines private constructor of RepositoryFactory class for disabling creating 
	 * new Objects from out side of RepositoryFactory
     */
    private RepositoryFactory() {
    	
    }

    /**
     * @return lk.edu.cis.sliit.beds.repository.RepositoryFactory
     * disables creating new objects of RepositoryFactory type again and again, instead provides the existing 
	 * RepositoryFactory object and creates a new Object if currently not any Object exists
     */
    public static RepositoryFactory getInstance() {
        if(null == repositoryFactory) {
            repositoryFactory = new RepositoryFactory();
        }
        return repositoryFactory;
    }

    /**
     * @param type : lk.edu.cis.sliit.beds.repository.RepositoryTypes
     * @return lk.edu.cis.sliit.beds.repository.SuperRepository
     * returns the requested service type based on the RepositoryTypes enums class
     */
    public SuperRepository getRepository(RepositoryTypes type) {
        switch (type) {
            case USER:
                return new UserRepositoryImpl();
            case ANSWER:
            case QUESTION:
            case QUESTION_ANSWER_MAPPING:
                return new QuestionerRepositoryImpl();
            case CUSTOMER_SERVICE:
                return new CustomerServiceRepositoryImpl();
            case CUSTOMER:
                return new CustomerRepositoryImpl();
            case CUSTOMER_FEEDBACK:
                return new CustomerFeedbackRepositoryImpl();
            default:
                return null;
        }
    }
}
