package lk.edu.cis.sliit.beds.cookie;

import java.util.HashMap;
import java.util.Map;

import lk.edu.cis.sliit.beds.dto.CookieDto;

/**
 * Stores And Maintains all Currently Logged In User's Unique User Specific Generated Cookies
 * with their Relevant Mapped Unique Keys
 * 
 * Used Singleton Design Pattern by declaring a private default constructor of the class to restrict and
 * prevent creating new objects in the heap again and again of the class type from outside of this class. 
 * The public methods allows to manipulate or modify the attributes of the class by 
 * referring to one time generated object of class type. By using Singleton Design Pattern, the data can be stored 
 * in those attributes by preventing resetting those attributes values as again and again object creation is 
 * prevented using this design pattern and it consists all currently stored cookies.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CookieServer {

	/**
	 * lk.edu.cis.sliit.beds.cookie.CookieServer
	 * Creates a private static Object reference for CookieServer
	 */
	private static CookieServer cookieManagementHandler;
	/**
	 * java.util.Map<String, String>
	 * Creates a private static Object reference for cookie map
	 */
	private static Map<String, String> cookieMap = new HashMap<>();
	
	/**
	 * defines private constructor of CookieServer class for disabling creating 
	 * new Objects from out side of CookieServer for keep preventing resetting cookieMap 
	 * when call CookieServer each time
	 */
	private CookieServer() {
		
	}
	
	
	/**
	 * @return lk.edu.cis.sliit.beds.cookie.CookieServer
	 * disabling creating new objects of CookieServer again and again, instead provides the existing 
	 * CookieServer object and creates a new Object if currently not any Object exists
	 */
	public static CookieServer getInstance() {
		if(null == cookieManagementHandler) {
			cookieManagementHandler = new CookieServer();
		}
		return cookieManagementHandler;
	}
	
	/**
	 * @param key : java.lang.String
	 * @return lk.edu.cis.sliit.beds.dto.CookieDto
	 * returns the cookie mapped with the given key filter by provided key
	 */
	public static CookieDto generateCookie(String key) {
		String cookie = key + Math.random();
		cookieMap.put(key, cookie);
		return new CookieDto(key, cookie);
	}
	
	/**
	 * @param cookieDto : lk.edu.cis.sliit.beds.dto.CookieDto
	 * @return boolean
	 * returns boolean value based on whether given cookie is a valid and currently registered in the server side
	 * by comparing both key and cookie
	 */
	public static boolean isValidCookie(CookieDto cookieDto) {
		if(cookieMap.containsKey(cookieDto.getKey()) && cookieMap.containsValue(cookieDto.getValue())) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param key : java.lang.String
	 * @return boolean
	 * returns the boolean status of destroying the mapped cookie using the provided key
	 */
	public static boolean destroyCookie(String key) {
		cookieMap.remove(key);
		return true;
	}
}
