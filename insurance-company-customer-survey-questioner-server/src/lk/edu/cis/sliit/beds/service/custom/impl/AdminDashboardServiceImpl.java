package lk.edu.cis.sliit.beds.service.custom.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;

import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.dto.FeedbackDto;
import lk.edu.cis.sliit.beds.dto.QualityOfServiceDto;
import lk.edu.cis.sliit.beds.dto.RecomendationFeedbackDto;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory.RepositoryTypes;
import lk.edu.cis.sliit.beds.repository.custom.CustomerFeedbackRepository;
import lk.edu.cis.sliit.beds.service.custom.AdminDashboardService;

/**
 * Provides Particular Admin Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class AdminDashboardServiceImpl implements AdminDashboardService, Serializable {
	/**
	 * serialVersionUID of AdminDashboardServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 2906642554793891381L;
	
	/**
	 * creates a private static reference variable as a constant which refers 
	 * POST method URL End Point for quickchart.io
	 */
	private static final String QUICK_CHART_API_POST_METHOD_URL = "https://quickchart.io/chart/create";
	
	
	/**
	 * returns customer recommendation against time bar chart image output stream as a 
	 * base64 encoded byte array
	 */
	@Override
	public byte[] getCustomerRecomendationAgaintTimeBarChart() throws Exception {
		CustomerFeedbackRepository customerFeedbackRepository = (CustomerFeedbackRepository) RepositoryFactory.getInstance().getRepository(RepositoryTypes.CUSTOMER_FEEDBACK);
		customerFeedbackRepository.setConnection(DBConnection.getInstance().getConnection());
		RecomendationFeedbackDto recomendationFeedbackDto = customerFeedbackRepository.findRecomondatinAndFeedbackAgainstMonth();

		return getGraphImage(getCustomerRecomendationBarChartParams(recomendationFeedbackDto));
	}
	
	/**
	 * returns quality of the service against time line chart image output stream as a 
	 * base64 encoded byte array
	 */
	@Override
	public byte[] getQualityOfServiceAgaintTimeLineChart() throws Exception {
		CustomerFeedbackRepository customerFeedbackRepository = (CustomerFeedbackRepository) RepositoryFactory.getInstance().getRepository(RepositoryTypes.CUSTOMER_FEEDBACK);
		customerFeedbackRepository.setConnection(DBConnection.getInstance().getConnection());
		QualityOfServiceDto qualityOfServiceDto = customerFeedbackRepository.findQualityOfServiceAgainstMonth();
		
		return getGraphImage(getQualityOfServiceParams(qualityOfServiceDto));
	}

	/**
	 * returns customer against their attitude pie chart image output stream as a 
	 * base64 encoded byte array
	 */
	@Override
	public byte[] getCustomerAgainstAttitudePieChart() throws Exception {
		CustomerFeedbackRepository customerFeedbackRepository = (CustomerFeedbackRepository) RepositoryFactory.getInstance().getRepository(RepositoryTypes.CUSTOMER_FEEDBACK);
		customerFeedbackRepository.setConnection(DBConnection.getInstance().getConnection());
		String negetiveFeedback = customerFeedbackRepository.findNegetiveAttitudeCustomersByFeedback();
		String neutralFeedback = customerFeedbackRepository.findNeutralAttitudeCustomersByFeedback();
		String positiveFeedback = customerFeedbackRepository.findPositiveAttitudeCustomersByFeedback();
		
		return getGraphImage(getCustomerAttitudePieChartParams(positiveFeedback, neutralFeedback, negetiveFeedback));
	}

	
	/**
	 * @param urlParameters : java.lang.String
	 * @return byte[]
	 * returns base64 encoded byte array of chart image based on the provided URL Parameters
	 */
	private byte[] getGraphImage(String urlParameters) {
        try {
            URL url = new URL(QUICK_CHART_API_POST_METHOD_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "CustomerSurveyQuestioner@2020");
            connection.setRequestProperty("Content-Type", "application/json; utf-8");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            connection.setDoOutput(true);
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(urlParameters);
            outputStream.flush();
            outputStream.close();

            StringBuilder responseContent = new StringBuilder();
            BufferedReader reader;
            String line;
            int status = connection.getResponseCode();
            // 200 to 299 are successful status range
            if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while (null != (line = reader.readLine())) {
                    responseContent.append(line);
                }
                reader.close();
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while (null != (line = reader.readLine())) {
                    responseContent.append(line);
                }
                reader.close();
            }
            String jsonObject = responseContent.toString();
            connection.disconnect();

            String reportUrl = jsonObject.split("\\}")[0].split("url")[1].split("\"")[2];
            
            URL url1 = new URL(reportUrl);
            ByteArrayOutputStream byteArrayOutStream = new ByteArrayOutputStream();
            ImageIO.write(ImageIO.read(url1), "jpg", byteArrayOutStream);
            byteArrayOutStream.flush();
            
            return Base64.getEncoder().encode(byteArrayOutStream.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
	}
	
	/**
	 * @param recomendationFeedbackDto : lk.edu.cis.sliit.beds.dto.RecomendationFeedbackDto
	 * @return java.lang.String
	 * returns the JSON Object by binding the data should be sent with the POST request of 
	 * Quick chart API to generate the customer recommendation bar chart
	 */
	private String getCustomerRecomendationBarChartParams(RecomendationFeedbackDto recomendationFeedbackDto) {
		List<String> months = recomendationFeedbackDto.getMonth();
		List<String> recomendations = recomendationFeedbackDto.getRecomendation();
		List<String> feedbacks = recomendationFeedbackDto.getFeedback();
		
		String json = "{\"chart\": " +
        "{\"type\": \"bar\", " +
            "\"data\": " +
                "{\"labels\": [";
					for(int index = 0; index < months.size(); index++) {
						if(index > 0) {json += ", "; }
						json += "\"" +months.get(index) + "\"";
					}
                json += "], \"datasets\": " +
                "[{\"label\": " +
                        "\"Recommendations\", " +
                            "\"data\": [";
                for(int index = 0; index < recomendations.size(); index++) {
					if(index > 0) {json += ", "; }
					json += recomendations.get(index);
				}
                json += "]}, " +
                "{\"label\": " +
                        "\"Positive Feedbacks\", " +
                            "\"data\": [";
                for(int index = 0; index < feedbacks.size(); index++) {
					if(index > 0) {json += ", "; }
					json += feedbacks.get(index);
				}
                json += "]}" +
              "]" +
            "}" +
          "}" +
       "}";
       return json;
	}
	
	/**
	 * @param qualityOfServiceDto : lk.edu.cis.sliit.beds.dto.QualityOfServiceDto
	 * @return java.lang.String
	 * returns the JSON Object by binding the data should be sent with the POST request of 
	 * Quick chart API to generate the quality of service line chart
	 */
	private String getQualityOfServiceParams(QualityOfServiceDto qualityOfServiceDto) {
		List<String> months = qualityOfServiceDto.getMonth();
		List<String> qualityOfService = qualityOfServiceDto.getQualityOfService();
		
		String json = "{\"chart\": " +
                "{\"type\": " + "\"line\", " +
                    "\"data\": " +
                        "{\"labels\": [";
                        for(int index = 0; index < months.size(); index++) {
    						if(index > 0) {json += ", "; }
    						json += "\"" +months.get(index) + "\"";
    					}
                        json += "], \"datasets\": " +
                            "[" +
                                "{\"label\": \"Customers\", " +
                                    "\"data\": [";
                        for(int index = 0; index < qualityOfService.size(); index++) {
    						if(index > 0) {json += ", "; }
    						json += qualityOfService.get(index);
    					}
                        json +=  "]}" +
                            "]" +
                        "}" +
                 "}" +
            "}";
       return json;
	}
	
	
	/**
	 * @param positiveFeedback : java.lang.String
	 * @param neutralFeedback : java.lang.String
	 * @param negetiveFeedback : java.lang.String
	 * @return : java.lang.String
	 * returns the JSON Object by binding the data should be sent with the POST request of 
	 * Quick chart API to generate the customer attitude pie chart
	 */
	private String getCustomerAttitudePieChartParams(String positiveFeedback, String neutralFeedback, String negetiveFeedback) {
		return "{\"chart\": " +
                "{\"type\": \"pie\", " +
                    "\"data\": " +
                        "{\"labels\": " +
                            "[" +
                                "\"Positive\", \"Neutral\", \"Negetive\"" +
                            "], " +
    "                   \"datasets\": " +
                            "[" +
                                "{\"data\": " +
                                    "[" +
                                    positiveFeedback + ", "+ neutralFeedback +", "+ negetiveFeedback +
                                    "]" +
                                "}" +
                            "]" +
                        "}" +
                "}" +
            "}";
	}

	/**
	 * returns the total Number of feedbacks and the progress (Positive, Neutral or Negative) 
	 * of the customer feedbacks launched in a particular day.
	 */
	@Override
	public FeedbackDto getTodayCustomerFeedback() throws Exception {
		CustomerFeedbackRepository customerFeedbackRepository = (CustomerFeedbackRepository) RepositoryFactory.getInstance().getRepository(RepositoryTypes.CUSTOMER_FEEDBACK);
		customerFeedbackRepository.setConnection(DBConnection.getInstance().getConnection());
		return customerFeedbackRepository.findTodayCustomerFeedback();
	}
}
