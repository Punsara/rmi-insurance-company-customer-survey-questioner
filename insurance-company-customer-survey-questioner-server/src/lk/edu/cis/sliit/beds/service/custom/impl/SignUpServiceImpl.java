package lk.edu.cis.sliit.beds.service.custom.impl;

import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.dto.SignupDto;
import lk.edu.cis.sliit.beds.entity.User;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.custom.CustomerRepository;
import lk.edu.cis.sliit.beds.repository.custom.UserRepository;
import lk.edu.cis.sliit.beds.service.custom.SignUpService;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static lk.edu.cis.sliit.beds.util.Utility.CUSTOMER_USER_ROLE_FLAG;

/**
 * Provides Particular User Sign up Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class SignUpServiceImpl implements SignUpService, Serializable {
    /**
	 * serialVersionUID for SignUpServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * sign up a particular customer by creating an account which contains a user name and password
	 * for the customer referred with the provided NIC no
	 */
	@Override
    public boolean signUpCustomer(SignupDto signupDto) throws Exception {
        CustomerRepository repository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        repository.setConnection(DBConnection.getInstance().getConnection());
        Integer userId = repository.findUserIdByNicNo(signupDto.getNicNo());


        User user = new User();
        user.setUserId(userId);
        user.setUserName(signupDto.getUserName());
        user.setUserPassword(signupDto.getPassword());
        user.setUserRoleFlag(CUSTOMER_USER_ROLE_FLAG);
        user.setLastUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
        UserRepository userRepository = (UserRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.USER);
        userRepository.setConnection(DBConnection.getInstance().getConnection());
        return userRepository.update(user, user.getUserId());
    }
}
