package lk.edu.cis.sliit.beds.service.custom.impl;

import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.dto.ValueTextDto;
import lk.edu.cis.sliit.beds.entity.QuestionAnswerMapping;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.custom.QuestionerRepository;
import lk.edu.cis.sliit.beds.service.custom.ManageQuestionerService;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Provides Particular Manage Questioner Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ManageQuestionerServiceImpl implements ManageQuestionerService, Serializable {

    /**
	 * serialVersionUID for ManageQuestionerServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * returns all questions currently available in the questioner
	 */
	@Override
    public List<ValueTextDto> findAllQuestions() throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.ANSWER);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findAllQuestions();
    }

    /**
     * returns all answers currently available in the questioner
     */
    @Override
    public List<ValueTextDto> findAllAnswers() throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.ANSWER);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findAllAnswers();
    }

    /**
     * returns all currently available question - answer mapping records 
     */
    @Override
    public List<QuestionerDto> findAllQuestionAnswerMappings() throws Exception {
    	QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.QUESTION_ANSWER_MAPPING);
    	questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findAllQuestionAnswerMapping();
    }

    /**
     * returns the next incremented answer id
     */
    @Override
    public Integer findNextAnswerId() throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.ANSWER);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findNextAnswerId();
    }

    /**
     * returns the next incremented question id
     */
    @Override
    public Integer findNextQuestionId() throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.ANSWER);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findNextQuestionId();
    }

    /**
     * returns the next incremented question - answer mapping id
     */
    @Override
    public Integer findNextMappingId() throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.ANSWER);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findNextMappingId();
    }

    /**
     * saves a particular question and returns a boolean value based on 
     * the status of the question saving process
     */
    @Override
    public boolean saveQuestion(ValueTextDto valueTextDto) throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.QUESTION);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        valueTextDto.setLastUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
        return questionerRepository.saveQuestion(valueTextDto);
    }

    /**
     * saves a particular answer and returns a boolean value based on 
     * the status of the answer saving process
     */
    @Override
    public boolean saveAnswer(ValueTextDto valueTextDto) throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.QUESTION);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        valueTextDto.setLastUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
        return questionerRepository.saveAnswer(valueTextDto);
    }

    /**
     * saves a particular question - answer mapping and returns a boolean value based on 
     * the status of the question - answer mapping saving process
     */
    @Override
    public boolean saveQuestionAnswerMapping(QuestionerDto questionerDto) throws Exception {
        QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.QUESTION);
        questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        Integer questionId = questionerRepository.findQuestionIdByName(questionerDto.getQuestion());
        Integer answerId = questionerRepository.findAnswerIdByName(questionerDto.getAnswer());

        QuestionAnswerMapping questionAnswerMapping = new QuestionAnswerMapping();
        questionAnswerMapping.setQuestionAnswerId(questionerDto.getQuestionNo());
        questionAnswerMapping.setQuestionId(questionId);
        questionAnswerMapping.setAnswerId(answerId);
        questionAnswerMapping.setLastUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
        return questionerRepository.save(questionAnswerMapping);
    }
}
