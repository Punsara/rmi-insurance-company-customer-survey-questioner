package lk.edu.cis.sliit.beds.service.custom.impl;

import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.dto.LoginDto;
import lk.edu.cis.sliit.beds.cookie.CookieServer;
import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.custom.UserRepository;
import lk.edu.cis.sliit.beds.service.custom.LoginService;

import java.io.Serializable;

/**
 * Provides Particular User Login Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class LoginServiceImpl implements LoginService, Serializable {
    /**
     * serialVersionUID of LoginServiceImpl
     * 
     * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
     */
    private static final long serialVersionUID = 2906642554793891381L;

    /**
     * validates the credentials provided by the user and generates a cookie 
     * based on the status of the validity of the input credentials
     */
    @Override
    public CookieDto authenticateUser(LoginDto loginDto) throws Exception {
        UserRepository userRepository = (UserRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.USER);
        userRepository.setConnection(DBConnection.getInstance().getConnection());
        boolean isAuthenticateUser = userRepository.findIsExistsByUserNameAndPassword(loginDto);
        if(isAuthenticateUser) {
        	return CookieServer.getInstance().generateCookie(loginDto.getUserName());
        }
        return null;
    }
}

