package lk.edu.cis.sliit.beds.service.custom.impl;

import java.io.Serializable;

import lk.edu.cis.sliit.beds.cookie.CookieServer;
import lk.edu.cis.sliit.beds.dto.CookieDto;
import lk.edu.cis.sliit.beds.service.custom.UserManagementService;

/**
 * Provides Particular User Management Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class UserManagementServiceImpl implements UserManagementService, Serializable {

	/**
	 * serialVersionUID of UserManagementServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * returns a boolean value based on the validation process for a given cookie
	 * by checking whether both the provided cookie and the key binded with that cookie are valid 
	 */
	@Override
	public boolean isValidCookie(CookieDto cookieDto) {
		return CookieServer.isValidCookie(cookieDto);
	}

	/**
	 * destroys the cookie from the server end when a particular user request
	 * for logout with the reference to a given key 
	 */
	@Override
	public boolean logOutUser(String key) {
		return CookieServer.destroyCookie(key);
	}

}
