package lk.edu.cis.sliit.beds.service.custom.impl;

import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto;
import lk.edu.cis.sliit.beds.dto.CustomerServiceDto;
import lk.edu.cis.sliit.beds.dto.QuestionerDto;
import lk.edu.cis.sliit.beds.entity.CustomerFeedback;
import lk.edu.cis.sliit.beds.entity.CustomerService;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.custom.CustomerFeedbackRepository;
import lk.edu.cis.sliit.beds.repository.custom.CustomerRepository;
import lk.edu.cis.sliit.beds.repository.custom.CustomerServiceRepository;
import lk.edu.cis.sliit.beds.repository.custom.QuestionerRepository;
import lk.edu.cis.sliit.beds.service.custom.CustomerSurveyService;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides Particular CustomerSurvey Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class CustomerSurveyServiceImpl implements CustomerSurveyService, Serializable {
	/**
	 * serialVersionUID of CustomerSurveyServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
    private static final long serialVersionUID = 2906642554793891381L;


    /**
     * returns all the customer services currently available in the business
     */
    @Override
    public List<CustomerServiceDto> getAllCustomerServices() throws Exception {
        CustomerServiceRepository customerServiceRepository = (CustomerServiceRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER_SERVICE);
        customerServiceRepository.setConnection(DBConnection.getInstance().getConnection());
        List<CustomerServiceDto> customerServiceDtoList = new ArrayList<CustomerServiceDto>();
        for (CustomerService customerService : customerServiceRepository.findAll()) {
            customerServiceDtoList.add(customerService.toDto());
        }
        return customerServiceDtoList;
    }

    /**
     * returns all the questions mapped with the answers currently available in the database 
     * by limiting to maximum of 10 questions per each questioner
     */
    @Override
    public List<QuestionerDto> getQuestionsMappedWithAnswers() throws Exception {
    	QuestionerRepository questionerRepository = (QuestionerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.QUESTION_ANSWER_MAPPING);
    	questionerRepository.setConnection(DBConnection.getInstance().getConnection());
        return questionerRepository.findAllQuestionAnswerMapping();
    }

    /**
     * saves all the customer provided feedbacks in the database for the given questioner
     */
    @Override
    public boolean saveCustomerFeedback(List<CustomerFeedbackDto> feedbackDtoList) throws Exception {
        CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        int customerId = customerRepository.findCustomerIdByUserName(feedbackDtoList.get(0).getCustomerUserName());

        CustomerFeedbackRepository customerFeedbackRepository = (CustomerFeedbackRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER_FEEDBACK);
        customerFeedbackRepository.setConnection(DBConnection.getInstance().getConnection());

        boolean isFeedbackSaved = false;
        for (CustomerFeedbackDto feedbackDto : feedbackDtoList) {
            feedbackDto.setCustomerId(customerId);
            feedbackDto.setFeedbackDateTime(Timestamp.valueOf(LocalDateTime.now()));
            
            isFeedbackSaved = customerFeedbackRepository.save(getFeedback(feedbackDto));
        }
        return isFeedbackSaved;
    }
    
    /**
     * @param feedbackDto : lk.edu.cis.sliit.beds.dto.CustomerFeedbackDto
     * @return lk.edu.cis.sliit.beds.entity.CustomerFeedback
     * returns a CustomerFeedback entity type by converting CustomerFeedbackDto in to it's entity type
     */
    private CustomerFeedback getFeedback(CustomerFeedbackDto feedbackDto) {
    	CustomerFeedback customerFeedback = new CustomerFeedback();
        customerFeedback.setCustomerFeedbackId(feedbackDto.getCustomerFeedbackId());
        customerFeedback.setCustomerId(feedbackDto.getCustomerId());
        customerFeedback.setQuestionId(feedbackDto.getQuestionId());
        customerFeedback.setServiceId(feedbackDto.getServiceId());
        customerFeedback.setCustomerFeedback(feedbackDto.getCustomerFeedback());
        customerFeedback.setFeedbackDateTime(feedbackDto.getFeedbackDateTime());
        return customerFeedback;
    }
}
