package lk.edu.cis.sliit.beds.service.custom.impl;

import lk.edu.cis.sliit.beds.dbconnection.DBConnection;
import lk.edu.cis.sliit.beds.dto.CustomerDto;
import lk.edu.cis.sliit.beds.entity.Customer;
import lk.edu.cis.sliit.beds.repository.RepositoryFactory;
import lk.edu.cis.sliit.beds.repository.custom.CustomerRepository;
import lk.edu.cis.sliit.beds.repository.custom.UserRepository;
import lk.edu.cis.sliit.beds.service.custom.ManageCustomerService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides Particular Manage Customer Related Services which are requested from the Client Side
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ManageCustomerServiceImpl implements ManageCustomerService, Serializable {

    /**
	 * serialVersionUID for ManageCustomerServiceImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * returns all the customers currently available in the system
	 */
	@Override
    public List<CustomerDto> findAllCustomers() throws Exception {
        CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        List<CustomerDto> allCustomers = new ArrayList<>();
        for (Customer customer : customerRepository.findAll()) {
            allCustomers.add(customer.toDto());
        }
        return allCustomers;
    }

    /**
     * saves the provided details of a particular customer and returns a boolean value 
     * based on the status of the saving the customer
     */
    @Override
    public boolean saveCustomer(CustomerDto customerDto) throws Exception {
        CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        return customerRepository.save(getCustomer(customerDto));
    }
    
    /**
     * @param customerDto : lk.edu.cis.sliit.beds.dto.CustomerDto
     * @return lk.edu.cis.sliit.beds.entity.Customer
     */
    private Customer getCustomer(CustomerDto customerDto) {
    	Customer customer = new Customer();
        customer.setCustomerId(customerDto.getCustomerId());
        customer.setUserId(customerDto.getUserId());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setAddress(customerDto.getAddress());
        customer.setAge(customerDto.getAge());
        customer.setGender(customerDto.getGender());
        customer.setMobileNo(customerDto.getMobileNo());
        customer.setNicNo(customerDto.getNicNo());
        customer.setSalary(customerDto.getSalary());
        customer.setLastUpdatedDate(customerDto.getLastUpdatedDate());
        return customer;
    }

    /**
     * returns the next Incremented customer id
     */
    @Override
    public Integer findNextCustomerId() throws Exception {
        CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        return customerRepository.findNextIncrementCustomerId();
    }

	/**
	 * Deletes a particular customer details from the database by referring the customer id 
	 * of a particular customer and returns a boolean value based on the status of the 
	 * customer deletion process
	 */
	@Override
	public boolean deleteCustomer(Integer customerId) throws Exception {
		CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        return customerRepository.delete(customerId);
	}

	/**
	 * returns the next incremented user id
	 */
	@Override
	public Integer findNextIncrementUserId() throws Exception {
		UserRepository userRepository = (UserRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.USER);
		userRepository.setConnection(DBConnection.getInstance().getConnection());
        return userRepository.findNextIncrementUserId();
	}

	/**
	 * Updates a particular customer from the database by referring the customer id 
	 * of a particular customer and returns a boolean value based on the status of the 
	 * customer update process
	 */
	@Override
	public boolean updateCustomer(Integer customerId, CustomerDto customerDto) throws Exception {
		CustomerRepository customerRepository = (CustomerRepository) RepositoryFactory.getInstance().getRepository(RepositoryFactory.RepositoryTypes.CUSTOMER);
        customerRepository.setConnection(DBConnection.getInstance().getConnection());
        return customerRepository.updateCustomerById(customerDto, customerId);
	}
}
