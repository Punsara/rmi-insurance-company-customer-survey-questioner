package lk.edu.cis.sliit.beds.service.impl;

import lk.edu.cis.sliit.beds.service.custom.impl.*;
import lk.edu.cis.sliit.beds.service.ServiceFactory;
import lk.edu.cis.sliit.beds.service.SuperService;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Provides a Particular Service Implementation Type which is requested from the Client Side 
 * Using the Defined enum Service Types
 * 
 * This class is extends from java.rmi.server.UnicastRemoteObject and implements
 * all the abstract methods Defined in lk.edu.cis.sliit.beds.service.ServiceFactory
 * interface
 * 
 * Used Singleton Design Pattern by declaring a private default constructor of the class to restrict and
 * prevent creating new objects in the heap again and again of the class type from outside of this class. 
 * The public getService method returns a particular service Implementation class by referring to 
 * one time generated object of class type.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class ServiceFactoryImpl extends UnicastRemoteObject implements ServiceFactory, Serializable {
	/**
	 * serialVersionUID of ServiceFactoryImpl
	 * 
	 * Works as a Unique Identifier for Serializable classes which helps to ensure 
     * that the loaded class is compatible with the serialized object during the deserialization process.
	 */
    private static final long serialVersionUID = 2906642554793891381L;
    
    /**
	 * lk.edu.cis.sliit.beds.service.ServiceFactory
	 * Creates a private static Object reference of ServiceFactory
	 */
    private static ServiceFactory serviceFactory;

    
    /**
     * @throws RemoteException
     * 
     * defines private constructor of ServiceFactoryImpl class for disabling creating 
	 * new Objects from out side of ServiceFactoryImpl for keep preventing resetting the serviceFactory
	 * when call ServiceFactoryImpl each time by creating new Object
     */
    private ServiceFactoryImpl() throws RemoteException {
    }

    /**
     * @return lk.edu.cis.sliit.beds.service.ServiceFactory
     * @throws RemoteException
     * 
     * disabling creating new objects of ServiceFactory type again and again, instead provides the existing 
	 * ServiceFactory object and creates a new Object if currently not any Object exists
     */
    public static ServiceFactory getInstance() throws RemoteException {
        if (null == serviceFactory) {
            serviceFactory = new ServiceFactoryImpl();
        }
        return serviceFactory;
    }

    
    /**
     * returns the service implementation class type of a requested service type
     * 
     * Java switch is used to filter the service types and returns the service implementation
     * class which is requested
     */
    @Override
    public SuperService getService(ServiceTypes serviceType) {
        switch (serviceType) {
            case LOGIN_SERVICE:
                return new LoginServiceImpl();
            case SIGNUP_SERVICE:
                return new SignUpServiceImpl();
            case UMM_SERVICE: 
            	return new UserManagementServiceImpl();
            case CUSTOMER_SURVEY:
                return new CustomerSurveyServiceImpl();
            case MANAGE_CUSTOMER:
                return new ManageCustomerServiceImpl();
            case MANAGE_QUESTIONER:
                return new ManageQuestionerServiceImpl();
            case ADMIN_DASHBOARD:
            	return new AdminDashboardServiceImpl();
            default:
                return null;
        }
    }
}
