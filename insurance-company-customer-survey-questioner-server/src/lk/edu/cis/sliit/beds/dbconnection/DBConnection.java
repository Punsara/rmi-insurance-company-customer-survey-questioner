package lk.edu.cis.sliit.beds.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Manages Application's Database Connection and Provides the Database Connection 
 * whenever it is Requested
 * 
 * Singleton Design Pattern is used in this Class by declaring a private default constructor of the class 
 * to restrict and prevent creating new connection objects in the heap again and again of the class type 
 * from outside of this class each time when a request occurs, instead provides a single currently existing 
 * connection for every time Requests for the Connection otherwise provides a new Connection Object 
 * which creates only one time during this class.
 * 
 * @author Punsara Prathibha - 2010866
 */
public class DBConnection {
	
	/**
	 * lk.edu.cis.sliit.beds.dbconnection.DBConnection
	 * Creates a private static Object reference for DBConnection
	 */
    private static DBConnection dbConnection;
    /**
	 * java.sql.Connection
	 * Creates a private Object reference for SQL connection
	 */
    private Connection connection = null;
    
    /**
	 * defines private constructor of DBConnection class for disabling creating 
	 * new Objects from out side of DBConnection for keep preventing resetting the connection
	 * when call DBConnection each time by creating new Object
	 */
    private DBConnection() {
    	
    }

    /**
	 * @return lk.edu.cis.sliit.beds.dbconnection.DBConnection
	 * disabling/restricting creating new objects of DBConnection again and again, instead provides the existing 
	 * DBConnection object and creates a new Object if currently not any Object exists
	 */
    public static DBConnection getInstance() {
        if (null == dbConnection) {
            dbConnection = new DBConnection();
        }
        return dbConnection;
    }

    /**
     * @return java.sql.Connection
     * creates a jdbc connection using mysql connector and returns the existing connection object 
     * if already an Object exists, else create a new object and returns it
     */
    public Connection getConnection() {
        try {
            if (null == connection) {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/insurance_customer_feedback_survey", "root", "123456");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
